<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Client_Property;
use App\Models\Client;
use App\Models\Propertylist;
use App\Models\Property;
use App\Models\Payment;
use Illuminate\Support\Facades;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ClientPropertyController extends Controller
{
    //

    Public function getpropList(Request $request)
    {
      $propID = $request->prop_id;
   $data = DB::table('propertylists')

      ->where('propertylists.propId',$propID)
      ->get();
// dd($data);
    }
    Public function forfeit($cp_id,$propertylistid)
    {

        $data=Client_Property::find($cp_id);
        $data->isActive=0;
        $data->save();
        $data2=Propertylist::find($propertylistid);
        $data2->client_id=NULL;
        $data2->status=1;
        $data2->save();
 return redirect()->back()->with('message', 'Property Forfeited Successfully');
    }

    Public function getActiveListPay($client_id)
    {


      // $data = DB::table('propertylists')->paginate(10)->where('propId','=',$propId)->get();

      $clientProperties = DB::table('client__properties')
      ->join('clients','clients.client_id', '=', 'client__properties.client_id')
      ->join('propertylists','propertylists.propertylistid', '=', 'client__properties.propertylistid')
      ->join('properties', 'properties.propId', '=', 'propertylists.propId')
      ->select('client__properties.cp_id','client__properties.counter','client__properties.directCommission','client__properties.unitManagerCommission','client__properties.GC1','client__properties.GC2','client__properties.GC4','client__properties.GC3','client__properties.GC5','client__properties.managerCommission','client__properties.DC','client__properties.UMC',
      'client__properties.MC','client__properties.grant1','client__properties.grant2','client__properties.grant3','client__properties.comnum','client__properties.grant4','client__properties.grant5','client__properties.comRelease','client__properties.PlanTerms','client__properties.dueDate','client__properties.totalPaid','client__properties.monthlyAmortization','propertylists.contractPrice','propertylists.propertylistid','properties.propertyName','propertylists.lot','propertylists.block')
      ->where('clients.client_id', '=', $client_id)
      ->where('client__properties.isActive', '=', 1)
      ->get();

// dd($clientProperties);
echo json_encode($clientProperties);

      // $data = DB::table('client__properties')->where('client_id', $client_id)->paginate(10);
    //  dd($clientProperties);

       // return view('property.newproperty',['data'=>$data])->with('count',1);
      // return $propId;
    }
    Public function Fetch($cp_id)
    {

      $clientProperties = DB::table('client__properties')
      ->join('propertylists','propertylists.propertylistid', '=', 'client__properties.propertylistid')
      ->join('properties', 'properties.propId', '=', 'propertylists.propId')
      ->select('client__properties.cp_id','client__properties.counter','client__properties.directCommission','client__properties.unitManagerCommission','client__properties.GC1','client__properties.GC2','client__properties.GC4','client__properties.GC3','client__properties.GC5','client__properties.managerCommission','client__properties.DC','client__properties.UMC',
      'client__properties.MC','client__properties.grant1','client__properties.grant2','client__properties.grant3','client__properties.comnum','client__properties.grant4','client__properties.grant5','client__properties.comRelease','client__properties.PlanTerms','client__properties.dueDate','client__properties.totalPaid','client__properties.monthlyAmortization','propertylists.contractPrice','propertylists.propertylistid','properties.propertyName','propertylists.lot','propertylists.block')
      ->where('client__properties.cp_id', '=', $cp_id)
      ->where('client__properties.isActive', '=', 1)
      ->get();

$propertyNameList = DB::table('properties')->get();

echo json_encode ($clientProperties);

    }
    Public function clientProperties($client_id)
    {
      $ClientProperty = Client::find($client_id);

      // $data = DB::table('propertylists')->paginate(10)->where('propId','=',$propId)->get();

$propertyNameList = DB::table('properties')->get();

$clientProperties = DB::table('client__properties')
->join('propertylists','propertylists.propertylistid', '=', 'client__properties.propertylistid')
->join('properties', 'properties.propId', '=', 'propertylists.propId')
->join('agents as dc', 'client__properties.DC', '=' , 'dc.agent_id', 'left outer')
->join('agents as mc', 'client__properties.MC', '=' , 'mc.agent_id', 'left outer')
->join('agents as umc', 'client__properties.UMC', '=' , 'umc.agent_id', 'left outer')
->select('client__properties.cp_id','client__properties.comnum', 'dc.AgentFname as DirectFname','dc.AgentLname as DirectLname','mc.AgentFname as ManagerFname','mc.AgentLname as ManagerLname',
'umc.AgentFname as UnitManagerFname','umc.AgentLname as UnitManagerLname','client__properties.directCommission','client__properties.isSign','client__properties.comRelease',
'client__properties.totalPaid','client__properties.monthlyAmortization','propertylists.contractPrice','propertylists.propertylistid','properties.propertyName',
'propertylists.lot','propertylists.block','client__properties.isFullyPaid','client__properties.unitManagerCommission','client__properties.managerCommission')
->where('client__properties.client_id', '=', $client_id)
->where('client__properties.isActive', '=', 1)
->get();



      // $data = DB::table('client__properties')->where('client_id', $client_id)->paginate(10);
    //  dd($clientProperties);
      return view('clientProperties.clientPropertyList', compact('ClientProperty', 'clientProperties','propertyNameList'))->with('count',1);
       // return view('property.newproperty',['data'=>$data])->with('count',1);
      // return $propId;
    }
    public function store(Request $request)
    {
 
      $dateNow = date('Y-m-d');
      if (str_contains($request->input('date'), '31')) {
        $str2 = date('Y-m-d', strtotime('-3 days', strtotime($request->input('date')))); 

    }
    else if(str_contains($request->input('date'), '30'))
    {
      $str2 = date('Y-m-d', strtotime('-2 days', strtotime($request->input('date')))); 
    }
    else if(str_contains($request->input('date'), '29'))
    {
      $str2 = date('Y-m-d', strtotime('-1 days', strtotime($request->input('date')))); 
    }
    else if(!$request->input('date'))
    {
      $str2 =  date('Y-m-d');
    }
    else
    {
      $str2 = $request->input('date');
    }

      if(!$request->input('date'))
      {
        $dateRecord = date('Y-m-d');
      }
      else{
        $dateRecord = $request->input('date');
      }
      if ($request->input('res')>0) {
    $finalCom = ($request->input('comRel'));
      }
      else{
        $finalCom = ($request->input('comRel'));
      }

     $client = new Client_Property();
     $client->PlanTerms = $request->input('terms');
     $client->dueDate = $request->input('due');
     $client->client_id = $request->input('cid');
     //for grant commission
    $client->GC1 = $request->input('grant1');
    $client->GC2 = $request->input('grant2');
    $client->GC3 = $request->input('grant3');
    $client->GC4 = $request->input('grant4');
    $client->GC5 = $request->input('grant5');
    $client->grant1 = $request->input('grant1Com');
    $client->grant2 = $request->input('grant2Com');
    $client->grant3 = $request->input('grant3Com');
    $client->grant4 = $request->input('grant4Com');
    $client->grant5 = $request->input('grant5Com');
     $client->monthlyAmortization = $request->input('Monthly');
     $client->propertylistid = $request->input('lotList');
     $client->directCommission = $request->input('dcom');
     $client->unitManagerCommission = $request->input('umcom');
     $client->managerCommission = $request->input('mcom');
     $client->totalPaid = $request->input('res');
      $client->comRelease = $finalCom;
      $client->created_at = $dateRecord;
      $client->dateApplied = $str2;
      $client->DC = $request->input('directAgent');
      $client->UMC = $request->input('unitmanager');
      $client->MC = $request->input('manager');
      $client->comnum = $request->input('comRel');

     $client->save();

    $cp_id = $client->cp_id;
     if ($request->input('res')>0) {
     $this->storePayment($request->input('res'),$request->input('OR'),$request->input('date'),$request->input('dcom'), $request->input('umcom'), $request->input('mcom'), $request->input('directAgent'), $request->input('unitmanager'), $request->input('unitmanager'),
     $request->input('grant1'),$request->input('grant2'),$request->input('grant3'),$request->input('grant4'),$request->input('grant5'),
     $request->input('grant1Com'),$request->input('grant2Com'),$request->input('grant3Com'),$request->input('grant4Com'),$request->input('grant5Com'),$cp_id);
     }
      $this->updatePropertyStatus($request->input('lotList'),$request->input('cid'));
     return redirect()->back()->with('message', 'Property Added Successfully');


      // return view('property.propertylist');
    }
    private function updatePropertyStatus($propertylistid,$client_id)
    {
      $data=Propertylist::find($propertylistid);
      $data->status='0';
      $data->client_id = $client_id;
      $data->save();
    }
    public function assume(Request $request)
    {
      
      $access =  Auth::user()->isSuperAdmin;
      if($access =="0")
      {
        return Response::json(['error' => 'Error msg'], 404);
        //return redirect()->back()->withErrors(['msg' => 'Action DENIED']);
      }
      else{
        $cpid = $request->input('cpid');
        $client = $request->input('client');
        $directAgent1 = $request->input('directAgent1');
        $unitmanager1 = $request->input('unitmanager1');
        $propid = $request->input('propid');
  
        $manager1 = $request->input('manager1');
        $dcom1 = $request->input('dcom1');
        $umcom1 = $request->input('umcom1');
        $mcom1 = $request->input('mcom1');
     
        
     
        
    
          $data1=Client_Property::find($cpid);
          $data1->client_id=$client;
          $data1->DC=$directAgent1;
          $data1->UMC= $unitmanager1;
          $data1->MC=$manager1;
          $data1->directCommission=$dcom1;
          $data1->unitManagerCommission=$umcom1;
          $data1->managerCommission=$mcom1;
          $data1->save();
          $data2=Propertylist::find($propid);
          $data2->client_id=$client;
          $data2->save();
    
        return redirect()->back()->with('message', 'Payment Edited Successfully');
      }
      
    }
    Public function nowSigned($cp_id)
    {

      $data=Client_Property::find($cp_id);
      $data->isSign='1';
      $data->save();
      return redirect()->back()->with('message', 'Property Edited Successfully');
    }
    private function storePayment($res,$OR,$date,$commission,$sd,$sh,$cp_id)
    {

      $id = Auth::id();
     $branch = DB::table('users')
     ->select('branch')
     ->where('id',$id)
     ->get();
     $finalBranch = ($branch[0]->branch);
      $pay = new Payment();
  $pay->user_id =  $id;
  $pay->branch = $finalBranch;
  $pay->Commission = $commission;
  $pay->or_num = $OR;
  $pay->cp_id = $cp_id;
  $pay->HM = $sh;
  $pay->SD = $sd;
  $pay->payment =$res;
  $pay->otherpayment = 0;
  $pay->penalty = 0;
  $pay->paymentName = 'Reservation';
  $pay->paymentMethod = 'Cash';
  $pay->paymentDesc = 'Payment for Reservation';
  $pay->created_at =$date;
  $pay->save();
    }
}
