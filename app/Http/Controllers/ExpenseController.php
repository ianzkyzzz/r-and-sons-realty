<?php

namespace App\Http\Controllers;

use App\Models\Expense;
use Illuminate\Http\Request;
use Illuminate\Support\Facades;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ExpenseController extends Controller
{
  public function index()
  {
    $data = DB::table('expenses')->get();
    // dd($data);
     return view('expense.expenselist',['data'=>$data])->with('count',1);

  }
  public function store(Request $request)
  {
    $finalBranch =  Auth::user()->branch;
    $id = Auth::id();

    $request->validate([
      'expenseName' => 'required',
      'Eamount' => 'required',
      'details' => 'required',

    ]);
   $pay = new Expense();
   $pay->user_id =  $id;
   $pay->branch = $finalBranch;
   $pay->ExpenseName = $request->input('expenseName');
   $pay->Amount = $request->input('Eamount');
   $pay->details = $request->input('details');
   $pay->save();
   return redirect()->back()->with('message', 'Expense Added Successfully');


  }
}
