<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\partner;
use Illuminate\Support\Facades;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class PartnerController extends Controller
{
  public function partnerslist(Request $request)
  {
    $role=  Auth::user()->role;
    if($role=="Partner"){
      $ids=  Auth::user()->busid;
      $data = DB::table('partners')->where('partnerId',$ids )->paginate(100);
       return view('partners.partnerslist',['data'=>$data])->with('count',1);
    }
    else{
      $data = DB::table('partners')->paginate(100);
      return view('partners.partnerslist',['data'=>$data])->with('count',1);
    }

  }
  public function store(Request $request)
  {
    $request->validate([
      'PartnerName' => 'required',
      'PartnerAddress' => 'required',
      'PartnerContact' => 'required',
    ]);
      Partner::create($request->all());
    return redirect()->back()->with('message', 'Partner Added Successfully');

  }

  public function GeneratePartnerReport(Request $request)
  {
    $request->validate([
      'from' => 'required',
      'To' => 'required',
    ]);
    $propId = $request->propId;
    $partnerId = $request->partnerId;
    $percentage = $request->percentage;
    $partnerName = $request->partnerName;
    $propertyName = $request->propertyName;
    $To = $request->To;
    $from = $request->from;
    $user=  Auth::user()->name;

header("Location: /fpdf/partnerShare.php/?propId=$propId&partnerId=$partnerId&percentage=$percentage&To=$To&from=$from&user=$user&partnerName=$partnerName&propertyName=$propertyName");
die();

  }
  public function getPartners(Request $request)
  {
    $search = $request->search;
    if ($search=='') {
      $Partner = partner::orderBy('PartnerName', 'asc')
      ->select('partnerId', 'PartnerName')
      ->limit(5)
      ->get();
    }
    else {
      $Partner = partner::orderBy('PartnerName', 'asc')
      ->select('partnerId', 'PartnerName')
      ->where('PartnerName', 'like','%'.$search.'%' )
      ->limit(5)
      ->get();
}

    $response = array();
    foreach ($Partner as $partner) {
      $response[] = array(
        "id"=>$partner->partnerId,
        "text"=>$partner->PartnerName
      );
    }

    echo json_encode($response);
    exit;
  }
  public function partnerPaymentsShare($propId,$partnerId,$percentage)
  {
    $data = DB::table('properties')

    ->where('properties.propId', '=', $propId)
    ->get();
    $data1 = DB::table('partners')

    ->where('partners.partnerId', '=', $partnerId)
    ->get();
// dd($partners);
$propertyName = $data[0]->propertyName;
$PartnerName = $data1[0]->PartnerName;

      return view('partners.partnerSharesList')->with('PartnerName',$PartnerName)->with('propertyName',$propertyName)->with('propId',$propId)->with('partnerId',$partnerId)->with('percentage',$percentage);
  }
  public function partnersproperties($partnerId)
  {
    $partners = DB::table('partners')
    ->where('partners.partnerId', '=', $partnerId)
    ->get();
    $data = DB::table('shares')
    ->join('partners','shares.partnerId', '=', 'partners.partnerId')
    ->join('properties', 'properties.propId', '=', 'shares.propId')
    ->select('partners.partnerId','partners.PartnerName','shares.propId','properties.propertyName','shares.percentage')
    ->where('partners.partnerId', '=', $partnerId)
    ->get();
// dd($partners);

      return view('partners.partnerproperties',['data'=>$data,'partners'=>$partners])->with('count',1);
  }
  public function edit(Request $request)
  {

    $id = $request->input('id');
    $name = $request->input('PartnerNameEdit');
    $add = $request->input('PartnerAddressEdit');
    $contact = $request->input('PartnerContactEdit');


      $data1=partner::find($id);
      $data1->PartnerName=$name;
      $data1->PartnerAddress=$add;
      $data1->PartnerContact=$contact;
      $data1->save();


    return redirect()->back()->with('message', 'Partner Edited Successfully');
  }

}
