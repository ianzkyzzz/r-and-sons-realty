<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Payment;
use App\Models\Commission;
use App\Models\Agent;
use App\Models\Bill;
use App\Models\Client;
use App\Models\Property;
use App\Models\Propertylist;
use App\Models\Client_Property;
use Illuminate\Support\Facades;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class PaymentController extends Controller
{
  public function index()
  {

    $userType=  Auth::user()->role;
  
    if($userType=="Partner")
    {
      return redirect()->back()->with('message', 'RESTRICTED');
    }
    if($userType=="Admin" || $userType=="Cashier" )
    {
          return view('payment.payment');
    }
    else
    {

        return view('auth.login')->withErrors(['You Are Not Login!!!']);
}

  }
  public function scheduleSMS()
  {
    $data = DB::select(DB::raw('SELECT cl.firstName,cl.lastName, cp.dueDate, pr.`propertyName`, pl.`block`, pl.`lot`,cl.`mobileNumber` FROM client__properties cp, clients cl, properties pr, propertylists pl WHERE
 (cp.dueDate = DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 3 DAY), "%d")) AND cp.client_id=cl.client_id AND cp.propertylistid=pl.propertylistid AND pl.propId=pr.propId AND cp.`isActive`="1" AND cp.`isFullyPaid`="0"'));




foreach($data as $item)
{
  $apicode="ST-DSRDO875467_BTAET";
  $passwd="8trubl&qfs";
  $number=$item->mobileNumber;
  $message = "Hi " . $item->firstName. " " .$item->lastName. ", This is a friendly reminder, Your property located at" .$item->propertyName ." on block ". $item->block.", lot ".$item->lot." dated every " . $item->dueDate." of the month. Please pay on or before due date. Thank You! Message from Diamanete Land Development Realty Services";
   $this->itexmo($number,$message,$apicode,$passwd);
}

  }
  public function paymenthistory()
  {
    $data = DB::table('payments')
    ->join('client__properties','client__properties.cp_id', '=', 'payments.cp_id')
    ->join('clients', 'clients.client_id', '=', 'client__properties.client_id')
    ->join('propertylists', 'propertylists.propertylistid', '=', 'client__properties.propertylistid')
    ->join('properties', 'properties.propId', '=', 'propertylists.propId')
    ->select('payments.otherpayment','payments.paymentMethod','payments.or_num','payments.bank', 'payments.paymentDesc', 'payments.PenaltyDescription', 'payments.penalty','payments.payment','payments.created_at','payments.id','payments.paymentName','payments.paymentMethod','payments.paymentDesc','properties.propertyName','propertylists.lot','propertylists.block', 'payments.created_at','clients.firstName','clients.lastName')
    ->where('payments.isActive', '=', 1)
    ->get();
     return view('paymenthistory.paymenthistory',['data'=>$data])->with('count',1);

  }
  public function Edit(Request $request)
  {
   
    $access =  Auth::user()->isSuperAdmin;
    if($access =="0")
    {
      return Response::json(['error' => 'Error msg'], 404);
      //return redirect()->back()->withErrors(['msg' => 'Action DENIED']);
    }
    else{
      $payid = $request->input('payid');
      $ornum = $request->input('ornum');
      $payment = $request->input('payment');
      $Method = $request->input('Method');

      $penaltyDesc = $request->input('penaltyDesc');
      $penalty = $request->input('penalty');
      $Bank = $request->input('Bank');
      $paymentDesc = $request->input('paymentDesc');
      $date = $request->input('createdDate');
      
   
      
  
        $data1=Payment::find($payid);
        $data1->or_num=$ornum;
        $data1->payment=$payment;
        $data1->paymentMethod=$Method;
        $data1->penalty=$penalty;
        $data1->PenaltyDescription=$penaltyDesc;
        $data1->bank=$Bank;
        $data1->created_at=$date;
        $data1->paymentDesc=$paymentDesc;
        $data1->save();
  
  
      return redirect()->back()->with('message', 'Payment Edited Successfully');
    }
    
  }
  public function unlist($id,$amount)
  {
    $access =  Auth::user()->isSuperAdmin;
    if($access =="0")
    {
      return redirect()->back()->withErrors(['msg' => 'Action DENIED']);
    }
      $data1=Payment::find($id);
      $data1->isActive='0';
      $data1->save();
      $this->updatePropertyDelete($id,$amount);
      $this->updateCommissionDelete($id);


    return redirect()->back()->with('message', 'Payment Edited Successfully');
  }

  public function otherindex()
    {
        return view('payment.otherpayment');
    }
public function SaveOtherPayment(Request $request)
{
  $finalBranch =  Auth::user()->branch;
  $id = Auth::id();
$comm =  0;
$cp_id  = $request->input('propList');
  $request->validate([
    'propList' => 'required',
    'Monthly' => 'required',
    'paymentfor' => 'required',
    'details' => 'required',
    'OR' => 'required',

  ]);

 $pay = new Payment();
 $pay->user_id =  $id;
 $pay->branch = $finalBranch;
 $pay->Commission = $comm;
 $pay->or_num = $request->input('OR');
 $pay->cp_id = $request->input('propList');
 $pay->payment = $request->input('Monthly');
 $pay->penalty = $request->input('Penalty');
 $pay->paymentName = $request->input('paymentfor');
 $pay->paymentMethod = 'Cash';
 $pay->paymentDesc = $request->input('details');
 $pay->save();
 return redirect()->back()->with('message', 'Payment Added Successfully');


}
private function storeBilling($id,$cp_id,$ornum)
{
  $type="payment";
  $bill = new Bill();

   $bill->id =  $id;
   $bill->cp_id = $cp_id;
   $bill->type = $type;
   $bill->orcn =$ornum;
$bill->save();
}
public function saveCommission($id,$cpid,$agent_id,$commission,$details)
{

 $comm = new Commission();
 $comm->id =  $id;
 $comm->agent_id = $agent_id;
 $comm->amount = $commission;
 $comm->cp_id =$cpid;
 $comm->comDetails = $details;
 $comm->save();
}
public function updatePropertyDelete($id,$amount)
{
  $data = DB::table('payments')
  ->join('client__properties','client__properties.cp_id', '=', 'payments.cp_id')
  ->select('client__properties.counter','client__properties.comRelease','client__properties.totalPaid','client__properties.cp_id')
  ->where('payments.id',$id)
  ->get();
  $counter=($data[0]->counter)-1;
  $comRelease=($data[0]->comRelease)+1;
  $totalPaid =($data[0]->totalPaid)-$amount;
  $data1=Client_Property::find($data[0]->cp_id);
  $data1->counter=$counter;
  $data1->comRelease=$comRelease;
  $data1->totalPaid=$totalPaid;
  $data1->save();
}
public function updateCommissionDelete($id)
{
  $updateDetails = [
      'isDelete' => '1'
  ];
  DB::table('commissions')
      ->where('id', $id)
      ->update($updateDetails);
}
  public function Save(Request $request)
  {
    $dateNow = date('Y-m-d');
    if(!$request->input('date'))
    {
      $dateRecord = date('Y-m-d');
    }
    else{
      $dateRecord = $request->input('date');
     
    }
 
     $id = Auth::id();
    $branch = DB::table('users')
    ->select('branch')
    ->where('id',$id)
    ->get();
    $directAgentID = $request->input('directAgent');
    $unitManagerID = $request->input('unitManager');
    $managerID = $request->input('manager');
    $grant1 = $request->input('gcom1');
    $grant2 = $request->input('gcom2');
    $grant3 = $request->input('gcom3');
    $grant4 = $request->input('gcom4');
    $grant5 = $request->input('gcom5');
$finalBranch = ($branch[0]->branch);

$amount = $request->input('Paid') + $request->input('Monthly');

$commRell = $request->input('comRelease')-1;
$commRellCHeck = $request->input('comRelease');
$currentRelCom = $request->input('numberRelease') - $commRell;
$comdetails = ($currentRelCom . '/' . $request->input('numberRelease'));

$balance = $request->input('Balance')-$request->input('Monthly');
$cp_id  = $request->input('propList');
if($commRellCHeck<0) {
  $dcom =0;
  $umcom = 0;
  $mcom = 0;
  $gcom1 = 0;
  $gcom2 = 0;
  $gcom3 = 0;
  $gcom4 = 0;
  $gcom5 = 0;
}
else
{
  if ($request->input('cusCommission')=="") {
  $dcom = $request->input('dcom');
  $umcom = $request->input('umcom');
  $mcom = $request->input('mcom');
  $gcom1 = $request->input('grant1');
  $gcom2 = $request->input('grant2');
  $gcom3 = $request->input('grant3');
  $gcom4 = $request->input('grant4');
  $gcom5 = $request->input('grant5');
  }


}

if ($balance<=0) {

     $this->nowFullPaid($cp_id);
}

    $request->validate([
      'propList' => 'required',
      'Monthly' => 'required',
      'PaymentMethod' => 'required',
      'details' => 'required',
      'OR' => 'required',

    ]);
   $pay = new Payment();
   $pay->user_id =  $id;
   $pay->branch = $finalBranch;

   $pay->or_num = $request->input('OR');
   $pay->cp_id = $request->input('propList');
   $pay->payment = $request->input('Monthly');
   $pay->penalty = $request->input('Penalty');
   $pay->paymentName = 'Monthly Payment';
   $pay->bank = $request->input('bank');
   $pay->PenaltyDescription = $request->input('PenaltyDescription');
   $pay->created_at = $dateRecord;
   $pay->payCounter = $request->input('counter');
   $pay->paymentMethod = $request->input('PaymentMethod');
   $pay->paymentDesc = $request->input('details');

   $pay->save();
   $id = $pay->id;
   if($id){
        $this->updatePropertyPaid($cp_id,$amount,$commRell,$request->input('counter'));
     if($commRellCHeck > 0) {
       $this->saveCommission($id,$request->input('propList'),$directAgentID,$dcom, $comdetails);
       if ($unitManagerID) {
          $this->saveCommission($id,$request->input('propList'),$unitManagerID,$umcom, $comdetails);
       }
       if ($managerID) {
          $this->saveCommission($id,$request->input('propList'),$managerID,$mcom, $comdetails);
       }
       if ($grant1) {
          $this->saveCommission($id,$request->input('propList'),$grant1,$gcom1,$comdetails);
       }
       if ($grant2) {
          $this->saveCommission($id,$request->input('propList'),$grant2,$gcom2,$comdetails);
       }
       if ($grant3) {
          $this->saveCommission($id,$request->input('propList'),$grant3,$gcom3,$comdetails);
       }
       if ($grant4) {
          $this->saveCommission($id,$request->input('propList'),$grant4,$gcom4,$comdetails);
       }
       if ($grant5) {
          $this->saveCommission($id,$request->input('propList'),$grant5,$gcom5,$comdetails);
       }
       if($dateNow !=$dateRecord)
       {
        $this->updateCommissionBackdate($id,$request->input('date'));
       }
       else if($dateNow ==$dateRecord)
       {
        $this->sendSMS($id);
       }
      $this->storeBilling($id,$request->input('propList'),$request->input('OR'));

     
   }
   

   }

   return redirect()->back()->with('message', 'Payment Successfully');


  }
  public function updateCommissionBackdate($id,$date)
{
  $updateDetails = [
      'isRelease' => '1', 'releaseDate' => $date
  ];
  DB::table('commissions')
      ->where('id', $id)
      ->update($updateDetails);
}
  public function sendSMS($id)
  {

$data = DB::table('payments')
->join('client__properties','client__properties.cp_id', '=', 'payments.cp_id')
->join('clients', 'clients.client_id', '=', 'client__properties.client_id')
->join('propertylists', 'propertylists.propertylistid', '=', 'client__properties.propertylistid')
->join('properties', 'properties.propId', '=', 'propertylists.propId')
->select((DB::raw("CONCAT(clients.firstName, ' ', clients.lastName) AS Cname")),'clients.mobileNumber','payments.id','propertylists.contractPrice','client__properties.totalPaid','client__properties.dueDate','properties.propertyName','propertylists.lot','propertylists.block', 'payments.created_at')
->where('payments.id', '=', $id)
->get();


    $commission = ($data[0]->totalPaid/$data[0]->contractPrice);
    $propertyName 	=$data[0]->propertyName;
    $name 			=$data[0]->Cname;
    $number         =$data[0]->mobileNumber;
    $lotno          =$data[0]->lot;
    $blockno        =$data[0]->block;
    $due            =$data[0]->dueDate;
    $date           =$data[0]->created_at;
    $message = "Hi " . $name . " we have just recieved your payment to your property located at " . $propertyName . " Block:" . $blockno . " lot:" . $lotno  . " Dated: ".date("F  j, Y",strtotime($date)) . ". Thank you and God Bless from R and Sons Properties Co";
    if (strlen($number)==11) {
     $this->itexmo($number,$message);
    }


  }
  public function sendNoticeSMS()
  {

$data = DB::table('clients')
->join('client__properties','client__properties.client_id', '=', 'clients.client_id')
->join('payments', 'payments.cp_id', '=', 'client__properties.cp_id')
->join('propertylists', 'propertylists.propertylistid', '=', 'client__properties.propertylistid')
->join('properties', 'properties.propId', '=', 'propertylists.propId')
->select((DB::raw("CONCAT(clients.firstName, ' ', clients.lastName) AS Cname")),'client__properties.created_at',(DB::raw("ROUND((ABS(DATEDIFF(CURRENT_TIMESTAMP,client__properties.created_at))/30)-COUNT(payments.id)) AS diff")),'properties.propertyName','client__properties.totalPaid','client__properties.dueDate','propertylists.lot','propertylists.block','clients.mobileNumber')
->where('payments.isActive', '=','1')
->where('properties.propId', '!=',21)
->where('properties.propId', '!=',25)
->where('properties.propId', '!=',27)
// ->where('properties.propId', '=',21)
// ->OrWhere('properties.propId', '=',25)
// ->OrWhere('properties.propId', '=',27)
->where('client__properties.isActive', '=','1')
->where('client__properties.isFullyPaid', '=','1')
->groupBy('payments.cp_id')
->get();

foreach($data as $items)
{
  $items->lot;
  $number = $items->mobileNumber;
  //$message = "Good day Sir/Maam " .  $items->Cname . " Please disregard the previous SMS you recieved regarding late payments, it is just a migaration error, rest assured that all your payment records is secured in our parallel database.  Message from R and Sons Properties Co. "; 
					 $message = "Good day Sir/Maam " .  $items->Cname . " a gentle reminder on your property " .  $items->propertyName . " block: " . $items->block . " lot: " . $items->lot ." has " . $items->diff ." month/s delayed payments. Please pay your remaining balance if you still want to continue your lot installment. Disregard this if already settled. message from R and Sons Properties Co.";
				if (strlen($items->mobileNumber)==11 && ($items->diff >=3) && ($items->diff <=12)) {
          
				$this->itexmo($number,$message);
         echo($message);

					}
        
}

    // $commission = ($data[0]->totalPaid/$data[0]->contractPrice);
    // $propertyName 	=$data[0]->propertyName;
    // $name 			=$data[0]->Cname;
    // $number         =$data[0]->mobileNumber;
    // $lotno          =$data[0]->lot;
    // $blockno        =$data[0]->block;
    // $due            =$data[0]->dueDate;
    // $date           =$data[0]->created_at;
    // $message = "Hi " . $name . " we have just recieved your payment to your property located at " . $propertyName . " Block:" . $blockno . " lot:" . $lotno  . " Dated: ".date("F  j, Y",strtotime($date)) . ". Thank you and God Bless from R and Sons Properties Co";
    // if (strlen($number)==11) {
    //  $this->itexmo($number,$message);
    // }


  }
  public function itexmo($number,$message){
		$url = 'https://api.m360.com.ph/v3/api/broadcast';

	$ch = curl_init($url);

$data = array("app_key" => "PFEvVnubdtB9brMj","app_secret" => "BoH97PfzCIOG0BT7nM0sZgAu7IazQ8tK","msisdn"=> $number,"content" =>$message,"shortcode_mask"=>"RandSonsPco","rcvd_transid"=>"","is_intl"=>"false");

$postdata = json_encode($data);

$ch = curl_init($url); 
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
$result = curl_exec($ch);
curl_close($ch);

	

		return (1);
}
  private function updatePropertyPaid($cp_id,$amount,$commRell,$counter)
  {
    $finalCount = $counter +1;
    $data=Client_Property::find($cp_id);
    $data->totalPaid=$amount;
    $data->counter=$finalCount;
    $data->comRelease=$commRell;
    $data->save();
  }
  private function nowFullPaid($cp_id)
  {
    $data=Client_Property::find($cp_id);
    $data->isFullyPaid=1;
    $data->save();
  }
public function bulkrelease(request $request)
{
  $user=  Auth::user()->name;
  $ids = $request->ids;
  $agent_id = $request->agent_id;
  $method = $request->method;
  // Use eager loading to reduce queries
  $commissions = Commission::whereIn('id', $ids)->get();

  // Get the current date
  $releaseDate = now()->toDateString();
 
  // Update records in batch
  Commission::whereIn('cmid', $ids)
      ->update([
          'releaseDate' => $releaseDate,
          'isRelease'   => 1,
      ]);
      return Redirect::to("/fpdf/bulkrelease.php?" . http_build_query([
        'save' => $agent_id,
        'user' => $user,
        'ids'  => $ids,
        'method' => $method,
        'date' => $releaseDate,
    ]));
  //header("Location: /fpdf/bulkrelease.php/?save=$agent_id&user=$user&ids[]=$ids");
}
  public function releaseComm($id,$cp_id,$ornum,$method)
  {
 
    $user=  Auth::user()->name;
    $data=Commission::find($id);
    $ldate = date('Y-m-d');
    $data->releaseDate=$ldate;
    $data->isRelease='1';
    // $data->systemPay2=50;
    $data->save();
    // $this->storeBillingCom($id,$cp_id,$ornum);
    // Redirect::away("/fpdf/comm.php/?save=$id&user=$user");
    header("Location: /fpdf/comm.php/?save=$id&user=$user&method=$method");
die();
  }
  private function storeBillingCom($id,$cp_id,$ornum)
  {
    $type="commission";
    $bill = new Bill();

     $bill->id =  $id;
     $bill->cp_id = $cp_id;
     $bill->type = $type;
     $bill->orcn =$ornum;
  $bill->save();
  }
  public function releaseCommDate(Request $request)
  {
    $user=  Auth::user()->name;
$id = $request->agent_id;
$date = $request->date;
$method = $request->method;
    header("Location: /fpdf/commDate.php/?save=$id&user=$user&date=$date&method=$method");
die();
  }
  public function getUnclaimed(Request $request)
  {
    $user=  Auth::user()->id;
$date = date('Y-m-d');
    $agent_id = $request->agent_id;
      $agentz = Agent::find($agent_id);
    $data = DB::table('commissions')
    ->join('client__properties','client__properties.cp_id', '=', 'commissions.cp_id')
    ->join('payments','payments.id', '=', 'commissions.id')
    ->join('clients', 'clients.client_id', '=', 'client__properties.client_id')
    ->join('agents', 'agents.agent_id', '=', 'commissions.agent_id')
    ->join('propertylists', 'propertylists.propertylistid', '=', 'client__properties.propertylistid')
    ->join('properties', 'properties.propId', '=', 'propertylists.propId')
    ->select((DB::raw("CONCAT(agents.AgentFname, ' ', agents.AgentLname) AS name")),(DB::raw("CONCAT(clients.firstName, ' ', clients.lastName) AS cname")),'client__properties.isSign','payments.payCounter','payments.user_id','commissions.amount','commissions.comDetails','client__properties.PlanTerms','commissions.cmid','commissions.cp_id','commissions.id','propertylists.contractPrice','client__properties.totalPaid', 'properties.propertyName','propertylists.lot','propertylists.block', 'commissions.created_at','agents.agent_id')
    ->where('commissions.agent_id', '=', $agent_id)
      ->where('payments.paymentMethod', '=', 'Cash')
      ->where('payments.isActive', '=','1')
      ->where('properties.propId', '!=',21)
      ->where('properties.propId', '!=',25)
      ->where('properties.propId', '!=',27)
    // ->orWhere('client__properties.shID', '=', $agent_id)
    // ->orWhere('client__properties.sdID', '=', $agent_id)
    ->where('commissions.amount', '>', 0)
    ->where('commissions.isRelease', '=', '0')
    ->where('commissions.isDelete', '=', '0')
    ->get();
    // dd($data);
      return view('Commission.agentcommission', compact('data', 'agentz'))->with('count',1)->with('initial',0)->with('initial1',0)->with('initial3',0)->with('finalCom',0)->with('user',$user)->with('date',$date);
  }
  public function getUnclaimedBank(Request $request)
  {
$date = date('Y-m-d');
    $agent_id = $request->agent_id;
      $agentz = Agent::find($agent_id);
    $data = DB::table('commissions')
    ->join('client__properties','client__properties.cp_id', '=', 'commissions.cp_id')
    ->join('payments','payments.id', '=', 'commissions.id')
    ->join('clients', 'clients.client_id', '=', 'client__properties.client_id')
    ->join('agents', 'agents.agent_id', '=', 'commissions.agent_id')
    ->join('propertylists', 'propertylists.propertylistid', '=', 'client__properties.propertylistid')
    ->join('properties', 'properties.propId', '=', 'propertylists.propId')
    ->select((DB::raw("CONCAT(agents.AgentFname, ' ', agents.AgentLname) AS name")),(DB::raw("CONCAT(clients.firstName, ' ', clients.lastName) AS cname")),'client__properties.isSign','commissions.comDetails','commissions.cmid','payments.payCounter','commissions.amount','client__properties.PlanTerms','commissions.cmid','commissions.cp_id','commissions.id','propertylists.contractPrice','client__properties.totalPaid', 'properties.propertyName','propertylists.lot','propertylists.block', 'commissions.created_at','agents.agent_id')
    ->where('commissions.agent_id', '=', $agent_id)
    ->where('payments.paymentMethod', '=', 'Bank')
    ->where('payments.isActive', '=','1')
    ->where('properties.propId', '!=',21)
    ->where('properties.propId', '!=',25)
    ->where('properties.propId', '!=',27)
    // ->orWhere('client__properties.shID', '=', $agent_id)
    // ->orWhere('client__properties.sdID', '=', $agent_id)
    ->where('commissions.created_at', '!=', $date)
    ->where('commissions.amount', '>', 0)
    ->where('commissions.isRelease', '=', '0')
    ->where('commissions.isDelete', '=', '0')
    ->get();
    // dd($data);
      return view('Commission.agentbankcommission', compact('data', 'agentz'))->with('count',1)->with('initial',0)->with('initial1',0)->with('initial3',0)->with('finalCom',0);
  }

  public function getRelease(Request $request)
  {
$selectvalue="";
    $agent_id = $request->agent_id;
    $date = date('Y-m-d');
      $agentz = Agent::find($agent_id);
      $balance ="0";
      $data = DB::table('commissions')
      ->join('client__properties','client__properties.cp_id', '=', 'commissions.cp_id')
      ->join('clients', 'clients.client_id', '=', 'client__properties.client_id')
      ->join('agents', 'agents.agent_id', '=', 'commissions.agent_id')
      ->join('propertylists', 'propertylists.propertylistid', '=', 'client__properties.propertylistid')
      ->join('properties', 'properties.propId', '=', 'propertylists.propId')
      ->join('payments', 'payments.id', '=', 'commissions.id')
      ->select((DB::raw("CONCAT(agents.AgentFname, ' ', agents.AgentLname) AS name")),(DB::raw("CONCAT(clients.firstName, ' ', clients.lastName) AS cname")),'commissions.amount','payments.paymentMethod','commissions.comDetails','client__properties.PlanTerms','commissions.cmid','propertylists.contractPrice','client__properties.totalPaid', 'properties.propertyName','propertylists.lot','propertylists.block','commissions.releaseDate', 'commissions.created_at','agents.agent_id')
      ->where('commissions.agent_id', '=', $agent_id)
      // ->orWhere('client__properties.shID', '=', $agent_id)
      // ->orWhere('client__properties.sdID', '=', $agent_id)
      ->where('commissions.amount', '>', 0)
      ->where('commissions.isRelease', '=', '1')
      ->where('commissions.isDelete', '=', '0')
      ->where('payments.paymentMethod', '=', 'cash')
      ->get();
      return view('Commission.commissionHistory', compact('data', 'agentz','selectvalue','balance'))->with('balance','')->with('count',1)->with('gselect',"")->with('agent_id',$agent_id)->with('date',$date)->with('initial',0)->with('method','cash');
  }
  public function getReleaseBank(Request $request)
  {
$selectvalue="";
    $agent_id = $request->agent_id;
    $date = date('Y-m-d');
      $agentz = Agent::find($agent_id);
      $balance ="0";
      $data = DB::table('commissions')
      ->join('client__properties','client__properties.cp_id', '=', 'commissions.cp_id')
      ->join('clients', 'clients.client_id', '=', 'client__properties.client_id')
      ->join('agents', 'agents.agent_id', '=', 'commissions.agent_id')
      ->join('propertylists', 'propertylists.propertylistid', '=', 'client__properties.propertylistid')
      ->join('properties', 'properties.propId', '=', 'propertylists.propId')
      ->join('payments', 'payments.id', '=', 'commissions.id')
      ->select((DB::raw("CONCAT(agents.AgentFname, ' ', agents.AgentLname) AS name")),(DB::raw("CONCAT(clients.firstName, ' ', clients.lastName) AS cname")),'commissions.amount','commissions.comDetails','client__properties.PlanTerms','commissions.cmid','propertylists.contractPrice','client__properties.totalPaid', 'properties.propertyName','propertylists.lot','propertylists.block','commissions.releaseDate', 'commissions.created_at','agents.agent_id')
      ->where('commissions.agent_id', '=', $agent_id)
      // ->orWhere('client__properties.shID', '=', $agent_id)
      // ->orWhere('client__properties.sdID', '=', $agent_id)
      ->where('commissions.amount', '>', 0)
      ->where('commissions.isRelease', '=', '1')
      ->where('commissions.isDelete', '=', '0')
      ->where('commissions.isDelete', '=', '0')
      ->where('payments.paymentMethod', '=', 'bank')
      ->get();



    // dd($data);
      return view('Commission.commissionHistory', compact('data', 'agentz','selectvalue','balance'))->with('balance','')->with('count',1)->with('gselect',"")->with('agent_id',$agent_id)->with('date',$date)->with('initial',0)->with('method','bank');
  }
  public function getReleaseClient(Request $request)
  {

    $agent_id = $request->agent_id;
    $gg= $request->client;
  $date = date('Y-m-d');
      $agentz = Agent::find($agent_id);

      $data = DB::table('commissions')
      ->join('client__properties','client__properties.cp_id', '=', 'commissions.cp_id')
      ->join('clients', 'clients.client_id', '=', 'client__properties.client_id')
      ->join('agents', 'agents.agent_id', '=', 'commissions.agent_id')
      ->join('propertylists', 'propertylists.propertylistid', '=', 'client__properties.propertylistid')
      ->join('properties', 'properties.propId', '=', 'propertylists.propId')
      ->select((DB::raw("CONCAT(agents.AgentFname, ' ', agents.AgentLname) AS name")),(DB::raw("CONCAT(clients.firstName, ' ', clients.lastName) AS cname")),'commissions.amount','client__properties.PlanTerms','commissions.cmid','propertylists.contractPrice','client__properties.totalPaid', 'properties.propertyName','propertylists.lot','propertylists.block','commissions.releaseDate', 'commissions.created_at','client__properties.agent_id')
      ->where('commissions.agent_id', '=', $agent_id)
      ->where('client__properties.cp_id', '=', $gg)
      ->where('commissions.amount', '>', 0)
      ->where('commissions.isRelease', '=', '1')
      ->where('commissions.isDelete', '=', '0')
      ->get();
      $balance = DB::table('client__properties')
        ->select('client__properties.commTotal')
        ->where('client__properties.cp_id' , '=', $gg)
        ->get();
        // dd($balance);
        $select = DB::table('client__properties')
        ->join('propertylists','propertylists.propertylistid', '=', 'client__properties.propertylistid')
        ->join('properties', 'properties.propId', '=', 'propertylists.propId')
        ->join('clients', 'clients.client_id', '=', 'client__properties.client_id')
        ->join('agents', 'agents.agent_id', '=','client__properties.agent_id')
        ->select((DB::raw("CONCAT(clients.firstName, ' ', clients.lastName, '--', properties.propertyName, ' Block ', propertylists.block, ' Lot ', propertylists.lot) AS name")),'client__properties.cp_id')
        ->where('client__properties.agent_id', '=', $agent_id)
        ->get();
        $selectvalue = DB::table('client__properties')
        ->join('propertylists','propertylists.propertylistid', '=', 'client__properties.propertylistid')
        ->join('properties', 'properties.propId', '=', 'propertylists.propId')
        ->join('clients', 'clients.client_id', '=', 'client__properties.client_id')
        ->join('agents', 'agents.agent_id', '=','client__properties.agent_id')
        ->select((DB::raw("CONCAT(clients.firstName, ' ', clients.lastName, '--', properties.propertyName, ' Block ', propertylists.block, ' Lot ', propertylists.lot) AS namez")))
        ->where('client__properties.agent_id', '=', $agent_id)
        ->where('client__properties.cp_id', '=', $gg)
        ->get();

    // dd($data);
      return view('Commission.commissionHistory', compact('data', 'agentz','select','selectvalue',))->with('date',$date)->with('balance',$balance[0]->commTotal)->with('gselect',$gg)->with('agent_id',$agent_id)->with('count',1)->with('initial',0);
  }

  public function getReleaseDate(Request $request)
  {

    $agent_id = $request->agent_id;
    $date = $request->relDate;
      $agentz = Agent::find($agent_id);
      $method = $request->method;
      $data = DB::table('commissions')
      ->join('client__properties','client__properties.cp_id', '=', 'commissions.cp_id')
      ->join('clients', 'clients.client_id', '=', 'client__properties.client_id')
      ->join('agents', 'agents.agent_id', '=', 'commissions.agent_id')
      ->join('propertylists', 'propertylists.propertylistid', '=', 'client__properties.propertylistid')
      ->join('properties', 'properties.propId', '=', 'propertylists.propId')
      ->join('payments', 'payments.id', '=', 'commissions.id')
      ->select((DB::raw("CONCAT(agents.AgentFname, ' ', agents.AgentLname) AS name")),(DB::raw("CONCAT(clients.firstName, ' ', clients.lastName) AS cname")),'commissions.amount','payments.paymentMethod','commissions.comDetails','client__properties.PlanTerms','commissions.cmid','propertylists.contractPrice','client__properties.totalPaid', 'properties.propertyName','propertylists.lot','propertylists.block','commissions.releaseDate', 'commissions.created_at','commissions.agent_id')
      ->where('commissions.agent_id', '=', $agent_id)
      ->where('commissions.releaseDate', '=', $date)
      ->where('payments.paymentMethod', '=', $method)
      ->where('commissions.amount', '>', 0)
      ->where('commissions.isRelease', '=', '1')
      ->where('commissions.isDelete', '=', '0')
      ->get();







    // dd($method );
      return view('Commission.commissionHistory', compact('data', 'agentz'))->with('balance','')->with('date',$date)->with('agent_id',$agent_id)->with('count',1)->with('initial',0)->with('method',$method);
  }

  public function listPaid(Request $request)
  {

    $cp_id = $request->cp_id;
    $data = DB::table('payments')
    ->join('client__properties','client__properties.cp_id', '=', 'payments.cp_id')

    ->join('propertylists', 'propertylists.propertylistid', '=', 'client__properties.propertylistid')
    ->join('properties', 'properties.propId', '=', 'propertylists.propId')
    ->select('payments.otherpayment','payments.payment','payments.paymentName', 'properties.propertyName','propertylists.lot','propertylists.block', 'payments.created_at')
    ->where('client__properties.cp_id', '=', $cp_id)
        ->where('payments.isActive', '=', 1)
    ->get();
// dd($data);
    $date = DB::table('client__properties')
    ->where('client__properties.cp_id', '=', $cp_id)

    ->get();
    $finaldate = ($date[0]->created_at);
    $paymentsDetails = DB::table('client__properties')
    ->join('propertylists','propertylists.propertylistid', '=', 'client__properties.propertylistid')
    ->join('clients','clients.client_id', '=', 'client__properties.client_id')
    ->join('properties','properties.propId', '=', 'propertylists.propId')
    ->where('client__properties.cp_id', '=', $cp_id)
    ->where('client__properties.isActive', '=', 1)
    ->select('properties.propertyName','client__properties.cp_id','client__properties.comRelease','client__properties.created_at','propertylists.lot','propertylists.block','clients.firstName', 'clients.firstName', 'clients.lastName')
    ->get();
    // dd($data);
      $date = date("m-d-y");
      return view('clientProperties.clientPaymentHistory', compact('data','paymentsDetails'))->with('count',1)->with('initial',0)->with('date',$finaldate)->with('repeat',$date);
  }
}
