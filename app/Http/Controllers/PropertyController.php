<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Property;
use App\Models\User;
use App\Models\share;
use Illuminate\Support\Facades;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
// use DataTables;

class PropertyController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function proplist(Request $request)
  {
    //  $id = Auth::id();
    // $email = DB::table('users')
    // ->select('email')
    // ->where('id',$id)
    // ->get();
    // dd($email);
   $data = DB::table('properties')->paginate(100);
    return view('property.propertylist',['data'=>$data])->with('count',1);
  }
  public function EditProp(Request $request)
  {

    $id = $request->input('id');
    $name = $request->input('propNamez');
    $add = $request->input('addressz');


      $data1=Property::find($id);
      $data1->propertyName=$name;
      $data1->address=$add;
      $data1->save();


    return redirect()->back()->with('message', 'Property Edited Successfully');
  }
  public function store(Request $request)
  {

  $request->validate([
    'propertyName' => 'required',
    'address' => 'required',
  ]);
  $property = new Property();
  $property->propertyName = $request->input('propertyName');
  $property->address = $request->input('address');
  $property->gc1 = $request->input('agent1');
  $property->gc2 = $request->input('agent2');
  $property->gc3 = $request->input('agent3');
  $property->gc4 = $request->input('agent4');
  $property->gc5 = $request->input('agent5');
  $property->gc1Per = $request->input('aper1');
  $property->gc2Per = $request->input('aper2');
  $property->gc3Per = $request->input('aper3');
  $property->gc4Per = $request->input('aper4');
  $property->gc5Per = $request->input('aper5');
  $property->p1 = $request->input('partner1');
  $property->p2 = $request->input('partner2');
  $property->p3 = $request->input('partner3');
  $property->p4 = $request->input('partner4');
  $property->p5 = $request->input('partner5');
  $property->p1Per = $request->input('pper1');
  $property->p2Per = $request->input('pper2');
  $property->p3Per = $request->input('pper3');
  $property->p4Per = $request->input('pper4');
  $property->p5Per = $request->input('pper5');
  $property->save();
  $propID= $property->propId;
  if ($request->input('pper1')>0) {
  $this->storePartner($request->input('partner1'),$propID,$request->input('pper1'));
  }
  if ($request->input('pper2')>0) {
  $this->storePartner($request->input('partner2'),$propID,$request->input('pper2'));
  }
  if ($request->input('pper3')) {
  $this->storePartner($request->input('partner3'),$propID,$request->input('pper3'));
  }
  if ($request->input('pper4')) {
  $this->storePartner($request->input('partner4'),$propID,$request->input('pper4'));
  }
  if ($request->input('pper5')) {
  $this->storePartner($request->input('partner5'),$propID,$request->input('pper5'));
  }

  return redirect()->back()->with('message', 'Parent Property Added Successfully');


    // return view('property.propertylist');
  }
  //partner imap_savebody
  public function storePartner($partnerid,$property,$percentage)
  {
  $partners = new share();
  $partners->percentage = $percentage;
  $partners->propID =$property;
  $partners->partnerId =$partnerid;
  $partners->save();

    // return view('property.propertylist');
  }
}
