<?php


namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Propertylist;
use App\Models\Property;
use Illuminate\Support\Facades;
use Illuminate\Support\Facades\DB;
use Session;
class PropertylistController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function propertyListAll($propId)
  {
    $property = Property::find($propId);
    // $data = DB::table('propertylists')->paginate(10)->where('propId','=',$propId)->get();
    $data = DB::table('propertylists')
    ->join('clients','clients.client_id', '=', 'propertylists.client_id', 'left outer')
    ->where('propId', $propId)->get();
    $selectData = DB::table('propertylists')->distinct()->select('block')->where('propId', '=', $propId)->groupBy('block')->get();

    return view('property.newproperty', compact('property', 'data', 'selectData'))->with('count',1)->with('propID',$propId)->with('block',"");
     // return view('property.newproperty',['data'=>$data])->with('count',1);
    // return $propId;
  }
  public function propertylistwithBlock($prop,$block)
  {

  $property = Property::find($prop);
    // $data = DB::table('propertylists')->paginate(10)->where('propId','=',$propId)->get();
    $data = DB::table('propertylists')
    ->join('clients','clients.client_id', '=', 'propertylists.client_id', 'left outer')
      ->where('block', $block)
    ->where('propId', $prop)->get();
  $selectData = DB::table('propertylists')->distinct()->select('block')->where('propId', '=', $prop)->groupBy('block')->get();



    return view('property.newproperty', compact('property', 'data', 'selectData'))->with('count',1)->with('propID',$prop)->with('block',$block);
    // return $propId;
  }

  public function index()
  {

    return view('payment.template');
     // return view('property.newproperty',['data'=>$data])->with('count',1);
    // return $propId;
  }

  public function getBlockList($prop_id)
  {
  //echo json_encode(DB::table('propertylists')->where('propId', $prop_id)->where('status',1)->get());
    echo json_encode (DB::table('propertylists')->distinct()->where('propId', $prop_id)->where('status',1)->get(['block']));
  }
  public function getView($propertylist)
  {
  //echo json_encode(DB::table('propertylists')->where('propId', $prop_id)->where('status',1)->get());
  echo json_encode (
    DB::table('propertylists')
    ->join('properties','properties.propID', '=', 'propertylists.propID')
    ->where('propertylistid', $propertylist)->get());
  }
  public function getActiveList($block,$prop_id)
  {
  echo json_encode(DB::table('propertylists')->where('propId', $prop_id)->where('block', $block)->where('status',1)->get());
  //  echo json_encode (DB::table('propertylists')->distinct()->get(['block']));
  }
  public function storepropertylist(Request $request)
  {

$status = 1;
if($request->input('fixTCP')=="")
{
$contractPrice = ($request->input('sqmPrice') * $request->input('lotsize'));
}
else {
  $contractPrice = $request->input('fixTCP');
}


$propertyList = new Propertylist();
$propertyList->propId = $request->input('parentID');
$propertyList->areasize = $request->input('lotsize');
$propertyList->lot = $request->input('lot');
$propertyList->block = $request->input('block');
$propertyList->priceSQM = $request->input('sqmPrice');
$propertyList->contractPrice = $contractPrice;
$propertyList->status = $status;
// assign other properties
$propertyList->save();
return redirect()->back()->with('message', 'Property Added Successfully');


    // return view('property.propertylist');
  }
  public function storepropertylistmany(Request $request)
  {
  $contractPrice = ($request->input('sqmPrice') * 100);

$status = 1;



  $x=1;
    for ($i=0; $i <$request->Numlot ; $i++) {

    $propertyList = new Propertylist();
    $propertyList->propId = $request->input('parentID2');
    $propertyList->areasize = 100;
    $propertyList->lot = $x;
    $propertyList->block = $request->input('block2');
    $propertyList->priceSQM =$request->input('sqmPrice');
    $propertyList->contractPrice = $contractPrice;
    $propertyList->status = $status;
    $x++;
    // assign other properties
    $propertyList->save();
    }

return redirect()->back()->with('message', 'Property Added Successfully');


    // return view('property.propertylist');
  }
  public function Edit(Request $request)
  {

$status = 1;

$propertylistid = $request->input('prrropid');
$parent = $request->input('parent');

if($request->input('fixTCPedit')=="")
{
  $contractPrice = ($request->input('sqmPriceedit') * $request->input('lotsize3'));
}
else {
$contractPrice = $request->input('fixTCPedit');
}

  $data1=Propertylist::find($propertylistid);
  $data1->contractPrice=$contractPrice;
  $data1->block=$request->input('block3');
  $data1->lot=$request->input('lotEdit');
  $data1->areasize=$request->input('lotsize3');
  $data1->priceSQM=$request->input('sqmPriceedit');

  $data1->save();


return redirect()->back()->with('message', 'Property Edited Successfully');


    // return view('property.propertylist');
  }
}
