<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ReportController extends Controller
{
  public function index()
  {
    $user=  Auth::user()->name;
    $userType=  Auth::user()->role;
  
    if($userType=="Partner")
    {
      return redirect()->back()->with('message', 'RESTRICTED');
    }
    // $data = DB::table('propertylists')->paginate(10)->where('propId','=',$propId)->get();

    return view('reports.generatereport')->with('name',$user);
     // return view('property.newproperty',['data'=>$data])->with('count',1);
    // return $propId;
  }

  public function agents()
  {
      $pdf = new \fpdf();
      $pdf->AddPage();
      $pdf->SetFont('Arial','B',16);
      $pdf->Cell(40, 10, 'Hello World', 1);
      $pdf->Cell(40, 10, 'Hello World', 1);
      $pdf->Cell(40, 10, 'Hello World', 1);
      $pdf->Ln();
      $users = \App\Models\User::all();
      foreach($users as $user) {
          $pdf->Cell(40, 10, $user->name, 1);
          $pdf->Ln();
      }
      $pdf->Output();
      exit;
  }
  public function BankDaily(Request $request)
  {

    $user=  Auth::user()->name;
$select = $request->input('reportSelect');
$date = $request->input('date');
$from =  $request->input('from');
$to =  $request->input('to');
// dd($to);
if($select=="Bank" && ($from == null && $to==null )  ){
    header("Location: /fpdf/dailyreport.php/?user=$user&date=$date");
die();
  }
  else if($select =="Cash" && ($from == null && $to==null ) )
  {
      header("Location: /fpdf/dailyCashReport.php/?user=$user&date=$date");
      die();
  }
  else if($select =="Bank")
  {
      header("Location: /fpdf/customBank.php/?user=$user&from=$from&to=$to");
      die();
  }
  else if($select =="Cash" )
  {
      header("Location: /fpdf/customCash.php/?user=$user&from=$from&to=$to");
      die();
  }
  else{
  header("Location: /fpdf/dailyCommissionReport.php/?user=$user&date=$date");
die();
  }
}
  public function statement($cp_id)
  {
      $pdf = new fpdf();
      $pdf->AddPage();
      $pdf->SetFont('Arial','B',16);
      $pdf->Cell(40, 10, 'Hello World', 1);
      $pdf->Cell(40, 10, 'Hello World', 1);
      $pdf->Cell(40, 10, 'Hello World', 1);
      $pdf->Ln();
      $data = DB::table('payments')
      ->join('client__properties','client__properties.cp_id', '=', 'payments.cp_id')
      ->join('agents', 'agents.agent_id', '=', 'client__properties.agent_id')
      ->join('propertylists', 'propertylists.propertylistid', '=', 'client__properties.propertylistid')
      ->join('properties', 'properties.propId', '=', 'propertylists.propId')
      ->select('payments.otherpayment','payments.payment','payments.paymentName', 'properties.propertyName','propertylists.lot','propertylists.block', 'payments.created_at','client__properties.agent_id')
      ->where('client__properties.cp_id', '=', $cp_id)
      ->get();
      foreach($data as $items) {
          $pdf->Cell(40, 10, $items->paymentName, 1);
          $pdf->Ln();
      }
      $pdf->Output();
      exit;
  }
}
