<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\subagent;
use App\Models\Agent;
use Illuminate\Support\Facades;
use Illuminate\Support\Facades\DB;
use DataTables;


class SubagentController extends Controller
{
    //
    public function fetch($id)
    {
     $agent = Agent::find($id);

     $data = DB::table('subagents')
     ->where('subagents.agent_id', '=', $id)
     ->get();
     return view('agents.agentDownline',['data'=>$data],['agent'=>$agent])->with('count',1);
    }
    Public function getSubagent($agent_id)
    {
      $data = DB::table('subagents')
      ->where('subagents.agent_id', '=', $agent_id)
      ->get();
echo json_encode($data);
    }
    public function store(Request $request)
    {

      $subagents = new subagent();
      $subagents->agent_id = $request->input('agent_id');
      $subagents->SubAgentName = $request->input('name');
      $subagents->SubAgentAddress = $request->input('address');
      $subagents->SubAgentMobile= $request->input('contact');

      // assign other properties
      $subagents->save();


  return redirect()->back()->with('message', 'Sub Agent Added Successfully');


      // return view('property.propertylist');
    }
}
