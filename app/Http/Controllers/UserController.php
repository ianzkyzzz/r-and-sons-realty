<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Client_Property;
use App\Models\Propertylist;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class UserController extends Controller
{

  public function adduser(Request $request)
  {
    $request->validate([
      'name' => 'required',
      'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
      'password' => ['required', 'string', 'min:8', 'confirmed'],
    ]);
    $user = new User();
  $user->name = $request->input('name');
  $user->email = $request->input('email');
  $user->password = bcrypt($request->input('password'));
  $user->branch = $request->input('branch');
  $user->role = $request->input('role');

  // assign other properties
  $user->save();
   return redirect()->back()->with('message', 'User Added Successfully');
    // return view('dashboard');
  }
  public function loaduserform()
  {
     return view('user.userform');
    // return view('dashboard');
  }
  public function checkpassword(Request $request)
  {
    $cp_id = $request->input('cpFormid');
    $pw = $request->input('password');
    $propID = $request->input('propp');
    $password = DB::table('users')
    ->select('password')
    ->where('role','SuperAdmin')
    ->get();
    $Finalpassword = ($password[0]->password);
    if (Hash::check($pw,$Finalpassword)) {
    $this->forfeit($cp_id,$propID);
    $this->fetchPropDetails($cp_id);
    return redirect()->back()->with('message', 'Property Forfeited Successfully');
    }
    else{
      return redirect()->back()->withErrors('Invalid Password');
    }

  }
  public function fetchPropDetails($cp_id)
  {
    $clientProperties = DB::table('client__properties')
    ->join('clients','clients.client_id', '=', 'client__properties.client_id')
    ->join('propertylists','propertylists.propertylistid', '=', 'client__properties.propertylistid')
    ->join('properties', 'properties.propId', '=', 'propertylists.propId')
    ->select('client__properties.cp_id', (DB::raw("CONCAT(clients.firstName, ' ', clients.lastName) AS Cname")), 'clients.mobileNumber','client__properties.counter','client__properties.directCommission','client__properties.unitManagerCommission','client__properties.GC1','client__properties.GC2','client__properties.GC4','client__properties.GC3','client__properties.GC5','client__properties.managerCommission','client__properties.DC','client__properties.UMC',
    'client__properties.MC','client__properties.grant1','client__properties.grant2','client__properties.grant3','client__properties.comnum','client__properties.grant4','client__properties.grant5','client__properties.comRelease','client__properties.PlanTerms','client__properties.dueDate','client__properties.totalPaid','client__properties.monthlyAmortization','propertylists.contractPrice','propertylists.propertylistid','properties.propertyName','propertylists.lot','propertylists.block')
    ->where('client__properties.cp_id', '=', $cp_id)
    ->where('client__properties.isActive', '=', 1)
    ->get();

    $message = "Good day Sir/Maam " .  $clientProperties[0]->Cname . " we regret to inform you that your property in " .  $clientProperties[0]->propertyName . " block: " . $clientProperties[0]->block . " lot: " . $clientProperties[0]->lot ." has been forfeited. You have reached the maximum allowed extension provided for you to settle the said property, message from R and Sons Properties Co.";
    $number = $clientProperties[0]->mobileNumber; 
    dd($number . " " . $message );  
    // if (strlen($items->mobileNumber)==11 && ($items->diff >=3) && ($items->diff <=12)) {
      
    $this->itexmo($number,$message);
    //  echo($message);

    //   }
  }
  public function itexmo($number,$message){
		$url = 'https://api.m360.com.ph/v3/api/broadcast';

	$ch = curl_init($url);

$data = array("app_key" => "PFEvVnubdtB9brMj","app_secret" => "BoH97PfzCIOG0BT7nM0sZgAu7IazQ8tK","msisdn"=> $number,"content" =>$message,"shortcode_mask"=>"RandSonsPco","rcvd_transid"=>"","is_intl"=>"false");

$postdata = json_encode($data);

$ch = curl_init($url); 
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
$result = curl_exec($ch);
curl_close($ch);

	

		return (1);
}
  Public function forfeit($cp_id,$propertylistid)
  {

      $data=Client_Property::find($cp_id);
      $data->isActive=0;
      $data->save();
      $data2=Propertylist::find($propertylistid);
      $data2->client_id=NULL;
      $data2->status=1;
      $data2->save();

  }
    public function userlists()
    {
      $data = DB::table('users')
      ->where('isActive', '1')
      ->get();
       return view('user.userlisting',['data'=>$data])->with('count',1);
      // return view('dashboard');
    }
    public function profile()
    {

       return view('user.userprofile');
      // return view('dashboard');
    }
    public function uploadAvatar(Request $request)
    {
      if ($request->hasfile('image')) {
        $filename = $request->image->getClientOriginalName();
        $request->image->storeAs('images', $filename, 'public');
        auth()->user()->update(['avatar'=>$filename]);

      }
      return redirect()->back()->with('message', 'User Avatar Updated Successfully');


    }
    public function editUser(Request $request)
    {
        $branch =  $request->input('branch');
        $name =  $request->input('name');
        auth()->user()->update(['name'=>$name,'branch'=>$branch ]);


      return redirect()->back()->with('message', 'User Updated Successfully');


    }

}
