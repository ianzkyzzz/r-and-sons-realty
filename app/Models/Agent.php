<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    use HasFactory;
    protected $fillable = ['AgentFname', 'AgentMname','AgentLname','AgentAddress','AgentMobile','AgentEmailAd','manager','unitmanager'];
    protected $primaryKey = 'agent_id';
}
