<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Propertylist extends Model
{
    use HasFactory;
    protected $fillable = ['block', 'propId','lot','status' ,'contractPrice','areasize','sqmPrice'];
    protected $primaryKey = 'propertylistid';
    public function getpropertylistRelation()
    {
      return $this->belongsTo('App\Models\Property', 'propId', 'propId');
    }
    public function getClientpropertyRelation()
    {
      return $this->hasOne('App\Models\Client_Property', 'propertylistid', 'propertylistid');
    }
}
