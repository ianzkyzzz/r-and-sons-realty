<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertylistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propertylists', function (Blueprint $table) {
            $table->increments('propertylistid');
            $table->integer('propId')->unsigned();
            $table->integer('client_id')->nullable();
            $table->integer('block');
            $table->integer('lot');
            $table->string('status');
            $table->decimal('priceSQM', 10,2);
            $table->decimal('contractPrice', 10,2);
            $table->decimal('areasize', 10,2);
            $table->integer('isDeleted')->default(0);
            $table->timestamps();

             $table->foreign('propId')->references('propId')->on('properties');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('propertylists');
    }
}
