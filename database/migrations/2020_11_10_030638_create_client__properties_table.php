<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client__properties', function (Blueprint $table) {
            $table->increments('cp_id');
            $table->integer('propertylistid')->unsigned();
            $table->integer('client_id')->unsigned();
            $table->integer('agent_id')->unsigned();
            $table->integer('PlanTerms');
            $table->integer('isActive')->default('1');//1 active, 0 inactive
            $table->integer('isFullyPaid')->default('0');
            $table->decimal('totalPaid', 10,2);
            $table->integer('justReserved')->default('0');
            $table->string('dueDate');

            $table->decimal('monthlyAmortization', 10,2);
            $table->decimal('comRelease', 10,2)->default('1');
            $table->decimal('Commission', 10,2)->default('0');
            $table->foreign('propertylistid')->references('propertylistid')->on('propertylists');
            $table->foreign('client_id')->references('client_id')->on('clients');
            $table->foreign('agent_id')->references('agent_id')->on('agents');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client__properties');
    }
}
