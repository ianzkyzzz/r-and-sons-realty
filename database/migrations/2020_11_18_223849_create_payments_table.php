<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->integer('cp_id')->unsigned();
            $table->integer('user_id');
            $table->string('or_num')->nullable();
            $table->decimal('payment', 10,2);
            $table->decimal('otherpayment', 10,2)->nullable();
            $table->decimal('penalty', 10,2)->nullable();
            $table->string('paymentName');
            $table->string('paymentMethod')->nullable();
            $table->string('paymentDesc')->nullable();
            $table->string('branch');
            $table->date('releaseDate')->nullable();
            $table->integer('isCOmRelease')->default('0');
            $table->integer('isBilled1')->default('0');
            $table->integer('isBilled2')->default('0');
            $table->decimal('systemPay1', 10,2)->default('50');
            $table->decimal('systemPay2', 10,2)->default('0');
            $table->decimal('Commission', 10,2);
            $table->integer('isActive')->default('1');

            $table->timestamps();
            $table->foreign('cp_id')->references('cp_id')->on('client__properties');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
