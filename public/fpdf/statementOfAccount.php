<?php
include_once("../fpdf/fpdf.php");



class PDF extends FPDF
{
// Pag
function Header()
{
    // Logo
    // $this->Image('logo.png',10,6,30);
    // Arial bold 15
    // $this->SetFont('Arial','B',15);
    // // Move to the right
    // $this->Cell(80);
    // // Title
    // $this->Cell(30,10,'Mabuhay Hardware Sales Report',4,0,'C');
    // $this->SetFont('Arial','B',12);
    //  $this->Cell(30,10,'Mabuhay Hardware Sales Report',1,0,'C');

  $this->Image('logo.jpg',80,5,-800);
    $this->Ln(20);
    $this->setFont("Arial",'B',8);
    $this->Cell(201,3,"R and Sons Properties Co.",0,1,"C");
     $this->setFont("Arial",'',8);
    $this->Cell(201,3,"2nd Floor S10 at The Paddock, J Camus St.,",0,1,"C");
    $this->Cell(201,3,"Corner General Luna St., Brgy. 4-A Poblacion District,",0,1,"C");
    $this->Cell(201,3,"Tel no. (082) 235 1146",0,1,"C");
     // $this->setFont("Arial","I",10);
    // $this->Cell(260,5,$_GET["cid"],0,1,"C");
    // Line break
    $this->Ln(6);
}

// Page fo$oter
function Footer()
{
    $date = date("m-d-y");
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Times','I',8);
    // Page number
    $this->Cell(0,5,'Page '.$this->PageNo().'/{nb}',0,1,'C');
    $this->Cell(0,5,'R and Sons Properties Co',0,1,'C');
    $this->Cell(0,-5,'Printed Date:'. $date,0,1,'R');

}
}

// Instanciation of inherited class


$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage('P','A4');
         $amortzation = 0;
         $month = 0;
         $down =0;
         $name ="";
         $lot ="";
         $block ="";
         $property = "";
         $dbhost = 'localhost';
         $dbuser = 'dbuser';
         $dbpass = '*/S3hd%/~]m~X<Zf';//*/S3hd%/~]m~X<Zf for testing live
         $dbname = 'randsons';//tech_staging_dsr for testing live
         $conn = mysqli_connect($dbhost, $dbuser, $dbpass,$dbname);
         $price = 0;
         if(! $conn ) {
            die('Could not connect: ' . mysqli_error());
         }
         $sql1 = "SELECT CONCAT(cl.`firstName`, ' ', cl.`lastName`) AS fName, prop.`propertyName`,cp.`PlanTerms`, cp.`isFullyPaid`, pl.`block`, pl.`lot`, cp.`monthlyAmortization`,prop.`address`, cp.`dueDate`, cp.`created_at`, pl.`contractPrice`
FROM client__properties AS cp, propertylists AS pl, properties AS prop, clients AS cl
WHERE cp.`propertylistid`=pl.`propertylistid` AND pl.`propId`=prop.`propId` AND cp.`client_id`=cl.`client_id`  AND cp.`cp_id`= '".$_GET['cid']."' ";
            $resultz = mysqli_query($conn, $sql1);
         if ($resultz->num_rows == 1) {

$rowz = $resultz->fetch_assoc();
$name = $rowz["fName"];
$lot =$rowz["lot"];
$block =$rowz["block"];
$property = $rowz["propertyName"];
$today =($rowz["created_at"]);
 $month = new DateTime ($rowz["created_at"]);
 $month2 = new DateTime (date("Y-m-d"));
 $vieww = $month->diff($month2);
 $full = $rowz["isFullyPaid"];
 $view = (($vieww->format('%y') * 12) + $vieww->format('%m'));
$price = $rowz["contractPrice"];
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"CLIENTS NAME: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,$rowz["fName"],0,1,"L");
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"ADDRESS: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,$rowz["propertyName"],0,1,"L");
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"BLOCK NO.: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,$rowz["block"],0,1,"L");
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"LOT NO.: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,$rowz["lot"],0,1,"L");
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"DATE APPLIED.: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,date("F  j, Y",strtotime($rowz["created_at"])),0,1,"L");

$pdf->SetFont('Times','B',10);

$pdf->Cell(50,5,"CONTRACT PRICE: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,number_format(round($rowz["contractPrice"],2),2,'.',','),0,1,"L");
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"PLAN TERMS: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,$rowz["PlanTerms"],0,1,"L");
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"MONTHY AMORTAZATION: ",0,0,"L");
$pdf->SetFont('Arial','',10);

$pdf->Cell(100,5,number_format(round($rowz["monthlyAmortization"],2),2,'.',','),0,1,"L");
$pdf->SetFont('Times','B',10);
$pdf->Cell(50,5,"DUE DATE: ",0,0,"L");
$pdf->SetFont('Times','',10);
$pdf->Cell(100,5,$rowz["dueDate"],0,1,"L");
$pdf->SetFont('Times','B',10);

        }
        $pdf->SetFont('Times','B',10);
$pdf->Cell(5,5,"#",1,0,"C");
$pdf->Cell(15,5,"OR #",1,0,"C");
$pdf->Cell(45,5,"For the month of",1,0,"C");
$pdf->Cell(40,5,"Date of Payment",1,0,"C");
$pdf->Cell(30,5,"Cash",1,0,"C");
$pdf->Cell(30,5,"Bank",1,0,"C");
$pdf->Cell(26,5,"Balance",1,1,"C");
    $pdf->SetFont('Times','',10);

         $sql = "SELECT pay.`or_num`, pay.`paymentName`,pay.`created_at`, pay.`payment`, pay.`paymentMethod`, pay.`otherpayment`
FROM payments pay, client__properties cp, propertylists plist, properties prop WHERE pay.`cp_id`=cp.`cp_id` AND cp.`propertylistid`=plist.`propertylistid`
AND plist.`propId`=prop.`propId` AND pay.`isActive`='1' AND cp.`cp_id`= '".$_GET['cid']."'
                    ORDER BY pay.`created_at` ";
            $result = mysqli_query($conn, $sql);
            $n=0;
            $bank=0;
            $cash=0;
            $total=0;
            $repeat='';
         if (mysqli_num_rows($result) > 0) {
            while($row = mysqli_fetch_assoc($result)) {
                 $repeat = strtotime("+1 Month",strtotime($today));
                 if (($row["paymentName"]=="Reservation") || ($row["paymentName"]=="Monthly Payment"))
                  {
                  $total= $total + $row["payment"];
                 }




                $n++;
                $pdf->Cell(5,5,$n,0,0,"C");
                //


                //
                $pdf->Cell(15,5,$row["or_num"],0,0,"C");
                $pdf->Cell(45,5,date("F, Y",strtotime($today)),0,0,"C");
                $pdf->Cell(40,5,date("F j, Y",strtotime($row["created_at"])),0,0,"C");
                if ($row["paymentMethod"]=="Cash") {
                    $pdf->Cell(30,5,number_format(($row["payment"]),2,'.',','),0,0,"C");
                }
                else
                {
                      $pdf->Cell(30,5,"0.00",0,0,"C");
                }
                if ($row["paymentMethod"]=="Bank") {
                    $pdf->Cell(30,5,number_format(($row["payment"]),2,'.',','),0,0,"C");
                }
                else
                {
                      $pdf->Cell(30,5,"0.00",0,0,"C");
                }


                $pdf->Cell(26,5,"",0,1,"C");


                  $today = date('Y-m-d',$repeat);
                }
                //  $pdf->Cell(5,5,$n,0,0,"C");
                // $pdf->Cell(15,5,$row["orNumber"],0,0,"C");
                // if ($row["particulars"]=="Downpayment"||($down==0&&$n==1)) {
                // $pdf->Cell(45,5,"Downpayment",0,0,"C");
                // }
                // else
                // {
                // $pdf->Cell(45,5,date("F, Y",strtotime($today)),0,0,"C");
                // }

                // $pdf->Cell(40,5,$row["dateAdded"],0,0,"C");
                // $pdf->Cell(30,5,number_format(($row["propertyDebit"]),2,'.',','),0,0,"C");
                // $pdf->Cell(30,5,number_format(($row["propertyCredit"]),2,'.',','),0,0,"C");
                // $pdf->Cell(26,5,'',0,1,"C");



         } else {
             $pdf->Cell(196,5,"NO PAYMENTS YET!!",1,1,"C");
         }
           $pdf->Ln(4);
           $pdf->SetFont('Times','B',10);
         $pdf->Cell(5,5,"",0,0,"C");
    $pdf->Cell(15,5,"",0,0,"C");
    $pdf->Cell(45,5,"",0,0,"C");
    $pdf->Cell(40,5,"",0,0,"C");
    $pdf->Cell(30,5,"",0,0,"C");
    $pdf->Cell(30,5,"",0,0,"C");
    $pdf->Cell(26,5,number_format(round(($price-$total),2),2,'.',','),1,1,"C");
    $pdf->Cell(5,5,"",0,0,"C");
    $pdf->Cell(15,5,"",0,0,"C");
    $pdf->Cell(85,5,"",0,0,"C");
    $pdf->Cell(60,5,"TOTAL PAID: ".number_format(round($total,2),2,'.',',')." "  ,1,0,"C");
    $pdf->Cell(26,5,"" ,0,1,"C");
    $delay = $view-$n;
  if($delay>0 && $full=="0")
    {
      $pdf->Cell(105,5,"Delayed Payments: ". ($delay) . " months",0,0,"L");
    }
    else
    {
      $pdf->Cell(105,5,"",0,0,"L");
    }
    $pdf->Cell(60,5,"",0,0,"C");
    $pdf->Cell(26,5,"" ,0,1,"C");

         mysqli_close($conn);

         $pdf->Output('', $name . "  " .$property . "  Block:" . $block. ", Lot:". $lot . ".pdf", false);

?>
