@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h2> {{$agentz->AgentFname . "  " .$agentz->AgentLname }}'s Commissions</h2>
                  <h5>{{$agentz->AgentAddress}}</h5>


                </div>

                <div class="card-body">
                  @if (Route::has('login'))
                    <div class="container-fluid">

@if(session()->has('message'))
<div class="alert alert-success">{{session()->get('message')}} </div>
@endif
<x-alert />
@if($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>

</div>

@endif



                      <table class="table" id="clientpropertylistTable">
  <thead class="thead-primary">
    <tr align="center">
      <th scope="col">#</th>
      <th scope="col">Client Name</th>
      <th scope="col">Property Name</th>
      <th scope="col">Installment</th>
      <th scope="col">lot</th>
      <th scope="col">block</th>
        <th scope="col">Net Commission</th>
        <th scope="col">Date</th>
          <th scope="col">Action</th>
    </tr>
  </thead>




@foreach($data as $items)
<a href="#" hidden>
{{$finalCom = ($items->amount)-($items->amount * 0.05)}}
</a>
<a href="#" hidden>
{{$initial3 = $initial3 + $finalCom }}
</a>
<a href="#" hidden>
{{$finalCom = ($items->amount)-($items->amount * 0.05)}}
</a>
<tr>
  <form action="/bulkrelease" method="POST">
    @csrf
  <th scope="row" align="center">{{$count++}}</th>
  @if($items->isSign == '0' && $items->payCounter > 1)
  <td align="center"></td>
  @else
  <td align="center"><input type="checkbox" name="ids[]" value="{{$items->cmid}}" ></td>
  @endif
  <input type="hidden" name="agent_id" value="{{$agentz->agent_id}}">
  <input type="hidden" name="method" value="cash">
 <td align="center">{{$items->cname }}</td>
  <td align="center">{{$items->propertyName }}</td>
  <td align="center">{{$items->comDetails}}</td>
  <td align="center">{{$items->lot}}</td>
  <td align="center">{{$items->block}}</td>
  <td align="center">{{number_format(round($finalCom,2),2,'.',',')}}</td>
  <td align="center">{{date("F j, Y", strtotime($items->created_at))}}</td>
  <td align="center">
@if($items->isSign == '0' && $items->payCounter > 1)
<a href="#" class="form-control">Not Available</a>
<a href="#" hidden>
{{$initial1 = $initial1 +$finalCom }}
@else
</a>
<a class="btn btn-sm btn-outline-info" href="{{'/Release/' . $items->cmid. '/'. $items->cp_id . '/' . $items->id . '/' . 'cash'}}" role="button">Release</a>
<a href="#" hidden>
{{$initial = $initial +$finalCom }}
</a>
  </td>
@endif


</tr>

@endforeach



<tr>

  <td scope="row" align="center"></td>
  <td align="center">Total Available Commission</td>
  <td align="center">---></td>
  <td align="center"></td>
  <td align="center">{{number_format(round($initial,2),2,'.',',')}}</td>
  <td align="center"></td>
</tr>
<tr>

  <td scope="row" align="center"></td>
  <td align="center">Total Unavailable</td>
  <td align="center">---></td>
  <td align="center"></td>
  <td align="center">{{number_format(round($initial1,2),2,'.',',')}}</td>
  <td align="center"></td>
</tr>
<tr>

  <td scope="row" align="center"></td>
  <td align="center">Total</td>
  <td align="center">---></td>
  <td align="center"></td>
  <td align="center">{{number_format(round($initial3,2),2,'.',',')}}</td>
  <td align="center"></td>
</tr>
</table>
<button class="btn btn-primary btn-sm" type="submit">BULK RELEASE</button>
</form>
<nav aria-label="Page navigation example">

</nav>



                    </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
</div>

@endsection
