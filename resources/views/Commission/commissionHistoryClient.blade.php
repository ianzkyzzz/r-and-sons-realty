@extends('layouts.app')

@section('content')


<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h2> {{$agentz->AgentFname . "  " .$agentz->AgentLname }}'s Commissions</h2>
                  <h5>{{$agentz->AgentAddress}}</h5>

                </div>

                <div class="card-body">
                  @if (Route::has('login'))
                    <div class="container-fluid">

@if(session()->has('message'))
<div class="alert alert-success">{{session()->get('message')}} </div>
@endif
<x-alert />
@if($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>

</div>

@endif


<form method="post" action="/client/create">
   @csrf
     <h5>Select Date</h5>
<input type="date"  id="relDate" name="relDate" value="{{$date}}"/>
<input type="hidden" id="agentId" name="agentId" value="{{$agentz->agent_id}}"/>
<select id="clientzz" name="clientzz">
  @if($selectvalue=="")
  <option value="">Select Block</option>
  @else
   <option value="{{$gselect}}">{{$selectvalue[0]->namez}}</option>
   @endif

  @foreach($select as $plist)
 <option value="{{$plist->cp_id}}">{{$plist->name}}</option>
@endforeach </select>

</form>
                      <table class="table" id="clientpropertylistTable">
  <thead class="thead-primary">
    <tr align="center">
      <th scope="col">#</th>
      <th scope="col">Client Name</th>
      <th scope="col">Property Name</th>
      <th scope="col">lot</th>
      <th scope="col">block</th>
        <th scope="col">Commission</th>
        <th scope="col">Date</th>

    </tr>
  </thead>




@foreach($data as $items)
<a href="#" hidden>
{{$initial = $initial + $items->amount }}
</a>

<tr>

  <th scope="row" align="center">{{$count++}}</th>
 <td align="center">{{$items->cname }}</td>
  <td align="center">{{$items->propertyName }}</td>
  <td align="center">{{$items->lot}}</td>
  <td align="center">{{$items->block}}</td>
  <td align="center">{{number_format(round($items->amount,2),2,'.',',')}}</td>
  <td align="center">{{date("F j, Y", strtotime($items->releaseDate))}}</td>




</tr>

@endforeach



<tr>

  <td scope="row" align="center"></td>
  <td align="center">Total Commission</td>
  <td align="center">---></td>
  <td align="center"></td>
  <td align="center">{{number_format(round($initial,2),2,'.',',')}}</td>
  <td align="center"></td>
</tr>
</table>
@if (count($data) > 0)
<a class="btn btn-sm btn-outline-info" href="{{'/ReleaseDate/' . $agent_id . '/' . $date}}" role="button">Print</a>
@endif
<nav aria-label="Page navigation example">

</nav>



                    </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
</div>

@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function(){
  $('#property').on('change', function () {
                 let prop_id = $(this).val();
                 $('#pList').empty();
                 $('#pList').append(`<option value="0" disabled selected>Processing...</option>`);
                 $.ajax({
                 type: 'GET',
                 url: '/propertyLists/' + prop_id,
                 success: function (response) {
                 var response = JSON.parse(response);
                 // console.log(response);
                 $('#pList').empty();
                 $('#pList').append(`<option value="0" disabled selected>Select Block*</option>`);
                 response.forEach(element => {
                     $('#pList').append(`<option value="${element['block']}">Block ${element['block']}</option>`);
                     });
                 }
             });
         });
         $('#pList').on('change', function () {
                        let block = $(this).val();
                        var prop_id = $('#property').val();
                        $('#lotList').empty();
                        $('#lotList').append(`<option value="0" disabled selected>Processing...</option>`);
                        $.ajax({
                        type: 'GET',
                        url: '/propertyListsActive/' + block +'/'+ prop_id ,
                        success: function (response) {
                        var response = JSON.parse(response);
                        // console.log(response);
                        $('#lotList').empty();
                        $('#lotList').append(`<option value="0" disabled selected>Select Lot*</option>`);
                        response.forEach(element => {
                            $('#lotList').append(`<option value="${element['propertylistid']}">Lot ${element['lot']}</option>`);
                            });
                        }
                    });
                });
                $('#lotList').on('change', function () {
                               let propertylist = $(this).val();
                               $.ajax({
                               type: 'GET',
                               dataType: 'json',
                               url: '/propertyListsView/' + propertylist ,
                               success: function (data) {
                               // var data = JSON.parse(data);
                               console.log(data[0]);
                                $('#contractPrice').val(data[0].contractPrice);
                               //alert(data[0].contractPrice);


                               }
                           });
                       });
                       $('#comRel').on('change', function () {
                                      let release = $(this).val();
                              let price = $('#contractPrice').val();
                              let downCom = $('#res').val()*0.10;
                              let com = ((price*0.10)-downCom)/release;
                              $('#monCOm').val(com);
                              });

                       $('#relDate').on('change', function () {
                         agentId = $('#agentId').val();
                         relDate = $('#relDate').val();
                         if ($('#clientzz').val()=="") {
                           select="0";
                           // alert(select);
                         }
                         else {
                           select = $('#clientzz').val();
                         }


                         window.location.href ="/CommissionHistoryDate/"+agentId+"/"+relDate;
                              });
                       $('#terms').on('change', function () {

                              $('#Monthly').val((($('#contractPrice').val()-$('#res').val())/$('#terms').val()).toFixed(2));
                              });


});
</script>
