@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h2>{{ __('AGENT LIST') }}</h2></div>

                <div class="card-body">
                  @if (Route::has('login'))
                    <div class="container-fluid">
<button type="button" class="btn btn-sm btn-outline-primary form-group" data-toggle="modal" data-target="#addProperty">Add Agent</button>
@if(session()->has('message'))
<div class="alert alert-success">{{session()->get('message')}} </div>
@endif
<x-alert />
@if($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif

                      <table class="table" id="agetTable">
  <thead class="thead-light">
    <tr align="center">
      <th scope="col">#</th>
      <th scope="col">Agents Name</th>
      <th scope="col">Agents Address</th>
      <th scope="col">Agents Number</th>

      <th scope="col"></th>

    </tr>
  </thead>
  <tbody>

    @foreach($data as $item)

    <tr>

      <th scope="row" align="center">{{$count++}}</th>
      <td align="center">{{$item->AgentFname . " " . $item->AgentMname . " " . $item->AgentLname }}</td>
      <td align="center">{{$item->AgentAddress}}</td>
      <td align="center">{{$item->AgentMobile}}</td>


      <td align="center">


        <button type="button" class="btn btn-sm" data-fname="{{$item->AgentFname}}" data-mname="{{$item->AgentMname}}"data-email="{{$item->AgentEmailAd}}" data-lname="{{$item->AgentLname}}" data-contact="{{$item->AgentMobile}}" data-id="{{$item->agent_id}}" data-address="{{$item->AgentAddress}}" data-toggle="modal" data-target="#editAgent">Edit</button>

          <a class="btn btn-sm btn-outline-info" href="{{'/Commission/'.$item->agent_id}}" role="button">Cash Com</a>
          
          <a class="btn btn-sm btn-outline-info" href="{{'/CommissionBank/'.$item->agent_id}}" role="button">Bank Com</a>


    <a class="btn btn-sm btn-outline-info" href="{{'/CommissionHistory/'.$item->agent_id}}" role="button">Cash History</a>
    
    <a class="btn btn-sm btn-outline-info" href="{{'/CommissionHistoryBank/'.$item->agent_id}}" role="button">Bank History</a>


  </td>

    </tr>

@endforeach
  </tbody>
</table>
<nav aria-label="Page navigation example">

</nav>

<!-- Modal -->
<div class="modal fade" id="addProperty" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Add Agents</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="/agent/create">
           @csrf
        <div class="container">

          <div class="form-row">
             <div class="form-group col-md-4">
               <label for="product_name">First Name</label>
               <input type="text" class="form-control" id="AgentFname" name="AgentFname" >
               <small id="asNameError" class="form-text text-muted"></small>

             </div>
             <div class="form-group col-md-4 mx-auto">
               <label for="Unit">Middle Name</label>
             <input type="text" class="form-control" id="AgentMname" name="AgentMname" >
               <small id="asTpypeError" class="form-text text-muted"></small>
             </div>
             <div class="form-group col-md-4 mx-auto">
               <label for="Unit">Last Name</label>
             <input type="text" class="form-control" id="AgentLname" name="AgentLname" >
               <small id="asTpypeError" class="form-text text-muted"></small>
             </div>
           </div>

           <div class="form-row">
           <div class="form-group col-md-4">
               <label for="Prod_categ">Address </label>
            <textarea id="AgentAddress" name="AgentAddress" class="form-control"> </textarea>
             </div>
             <div class="form-group col-md-4">
               <label for="product_name">Contact Number</label>
               <input type="text" class="form-control" id="AgentMobile" name="AgentMobile" >
               <small id="asNameError" class="form-text text-muted"></small>

             </div>
             <div class="form-group col-md-4">
               <label for="product_name">Email Address</label>
               <input type="text" class="form-control" id="AgentEmailAd" name="AgentEmailAd" >
               <small id="asNameError" class="form-text text-muted"></small>

             </div>
             </div>

                        <div class="form-row">
                          <div class="form-group col-md-6">
                            <label for="Agent_name">Manager</label>

                            <select class="form-control" style="width:300px;" id="manager" name="manager">
                               <option value="">Select Agent</option>

                            </select>


                          </div>
                          <div class="form-group col-md-6">
                            <label for="product_name">Unit Manager </label>
                            <select class="form-control" style="width:300px;" id="unitmanager" name="unitmanager">
                               <option value="">Select Agent</option>

                            </select>
                            <small id="asNameError" class="form-text text-muted"></small>

                          </div>
                          </div>



      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>
                    </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
</div>
<!-- modal this is for edit modal -->
<div class="modal fade" id="editAgent" tabindex="-1" aria-labelledby="exampleModalLabelss" aria-hidden="true">
  <div class="modal-dialog modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title"  id="exampleModalLabel">Edit Agent</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="/agent/Edit">
           @csrf
        <div class="container">

          <div class="form-row">
             <div class="form-group col-md-4">
               <label for="product_name">First Name</label>
               <input type="text" class="form-control" id="EditAgentFname" name="EditAgentFname" >
               <input type="hidden" id="editID" name="editID">
               <small id="asNameError" class="form-text text-muted"></small>

             </div>
             <div class="form-group col-md-4 mx-auto">
               <label for="Unit">Middle Name</label>
             <input type="text" class="form-control" id="EditAgentMname" name="EditAgentMname" >
               <small id="asTpypeError" class="form-text text-muted"></small>
             </div>
             <div class="form-group col-md-4 mx-auto">
               <label for="Unit">Last Name</label>
             <input type="text" class="form-control" id="EditAgentLname" name="EditAgentLname" >
               <small id="asTpypeError" class="form-text text-muted"></small>
             </div>
           </div>

           <div class="form-row">
           <div class="form-group col-md-4">
               <label for="Prod_categ">Address </label>
            <textarea id="EditAgentAddress" name="EditAgentAddress" class="form-control"> </textarea>
             </div>
             <div class="form-group col-md-4">
               <label for="product_name">Contact Number</label>
               <input type="text" class="form-control" id="EditAgentMobile" name="EditAgentMobile" >
               <small id="asNameError" class="form-text text-muted"></small>

             </div>
             <div class="form-group col-md-4">
               <label for="product_name">Email Address</label>
               <input type="text" class="form-control" id="EditAgentEmailAd" name="EditAgentEmailAd" >
               <small id="asNameError" class="form-text text-muted"></small>

             </div>
             </div>
             </div>



      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- modal  -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function(){
  $('#editAgent').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget)
      var fname = button.data('fname')
      var mname = button.data('mname')
      var lname = button.data('lname')
      var email = button.data('email')
      var id = button.data('id')
      var address = button.data('address')
      var mobile = button.data('contact')








      $('#editID').val(id);
      $('#EditAgentFname').val(fname);
      $('#EditAgentMname').val(mname);
      $('#EditAgentLname').val(lname);
      $('#EditAgentAddress').val(address);
      $('#EditAgentMobile').val(mobile);
      $('#EditAgentEmailAd').val(email);







      // var description = button.data('mydescription')
      // var cat_id = button.data('catid')
      // var modal = $(this)
      // modal.find('.modal-body #title').val(title);
      // modal.find('.modal-body #des').val(description);
      // modal.find('.modal-body #cat_id').val(cat_id);
  });
  $('#manager').select2({
    ajax:{
      url:"/agent/getList",
      type:"post",
      dataType:"json",
      data: function(params){
        return{
        _token:CSRF_TOKEN,
        search: params.term
      };
    },
    processResults: function(response){
      return{
        results: response
      };
    },
    cache :true
  }
});
$('#unitmanager').select2({
  ajax:{
    url:"/agent/getList",
    type:"post",
    dataType:"json",
    data: function(params){
      return{
      _token:CSRF_TOKEN,
      search: params.term
    };
  },
  processResults: function(response){
    return{
      results: response
    };
  },
  cache :true
}
});
});
</script>
@endsection
