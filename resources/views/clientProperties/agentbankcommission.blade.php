@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h2> {{$agentz->AgentFname . "  " .$agentz->AgentLname }}'s Commissions</h2>
                  <h5>{{$agentz->AgentAddress}}</h5>


                </div>

                <div class="card-body">
                  @if (Route::has('login'))
                    <div class="container-fluid">

@if(session()->has('message'))
<div class="alert alert-success">{{session()->get('message')}} </div>
@endif
<x-alert />
@if($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>

</div>

@endif



                      <table class="table" id="clientpropertylistTable">
  <thead class="thead-primary">
    <tr align="center">
      <th scope="col">#</th>
      <th scope="col">Client Name</th>
      <th scope="col">Property Name</th>
      <th scope="col">lot</th>
      <th scope="col">block</th>
        <th scope="col">Commission</th>
        <th scope="col">Date</th>
          <th scope="col">Action</th>
    </tr>
  </thead>




@foreach($data as $items)
<a href="#" hidden>
{{$initial3 = $initial3 + $items->amount }}
</a>
<tr>

  <th scope="row" align="center">{{$count++}}</th>
 <td align="center">{{$items->cname }}</td>
  <td align="center">{{$items->propertyName }}</td>
  <td align="center">{{$items->lot}}</td>
  <td align="center">{{$items->block}}</td>
  <td align="center">{{number_format(round($items->amount,2),2,'.',',')}}</td>
  <td align="center">{{date("F j, Y", strtotime($items->created_at))}}</td>
  <td align="center">
@if($items->isSign == '0')
<a href="#" class="form-control">Not Signed</a>
<a href="#" hidden>
{{$initial1 = $initial1 + $items->amount }}
@else
</a>
<a class="btn btn-sm btn-outline-info" href="{{'/Release/' . $items->cmid. '/'. $items->cp_id . '/' . $items->id }}" role="button">Release</a>
<a href="#" hidden>
{{$initial = $initial + $items->amount }}
</a>
  </td>
@endif


</tr>

@endforeach



<tr>

  <td scope="row" align="center"></td>
  <td align="center">Total Available Commission</td>
  <td align="center">---></td>
  <td align="center"></td>
  <td align="center">{{number_format(round($initial,2),2,'.',',')}}</td>
  <td align="center"></td>
</tr>
<tr>

  <td scope="row" align="center"></td>
  <td align="center">Total Unavailable</td>
  <td align="center">---></td>
  <td align="center"></td>
  <td align="center">{{number_format(round($initial1,2),2,'.',',')}}</td>
  <td align="center"></td>
</tr>
<tr>

  <td scope="row" align="center"></td>
  <td align="center">Total</td>
  <td align="center">---></td>
  <td align="center"></td>
  <td align="center">{{number_format(round($initial3,2),2,'.',',')}}</td>
  <td align="center"></td>
</tr>
</table>
<nav aria-label="Page navigation example">

</nav>



                    </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
</div>

@endsection
