@extends('layouts.app')

@section('content')


<div class="container">

    <div class="row justify-content-center">
      
        <div class="col-md-12">
          
            <div class="card">
              @if($ClientProperty->client4=="" || $ClientProperty->client3=="" || $ClientProperty->client2=="" || $ClientProperty->client4=="")

                <div class="card-header"><h2> {{$ClientProperty->firstName . " " . $ClientProperty->middleName . " " .$ClientProperty->lastName }}'s Properties</h2>

              @else
                  <div class="card-header"><h3> {{$ClientProperty->firstName . " " . $ClientProperty->middleName . " " .$ClientProperty->lastName . " ," . $ClientProperty->client4 . " ," . $ClientProperty->client3. " , and " . $ClientProperty->client2 }}'s Properties</h3>
              @endif
                  <h4>{{$ClientProperty->address}}</h4>
                  <h4>{{$ClientProperty->mobileNumber}}</h4>
                  <h4>{{$ClientProperty->emailAddress}}</h4>
                </div>

                <div class="card-body">
                  @if (Route::has('login'))
                    <div class="container-fluid">
<button type="button" class="btn btn-sm btn-outline-primary form-group" data-toggle="modal" data-target="#addProperty">Add Client Property</button>
@if(session()->has('message'))
<div class="alert alert-success">{{session()->get('message')}} </div>
@endif
<x-alert />
@if($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>

</div>

@endif



                      <table class="table" id="clientpropertylistTable">
  <thead class="thead-primary">
    <tr align="center">
      <th scope="col">#</th>
      <th scope="col">Property Name</th>
      <th scope="col">block</th>
      <th scope="col">lot</th>
        <th scope="col">TCP</th>
        <th scope="col">Monthly Amortization</th>
        <th scope="col">Total Paid</th>
        <th scope="col">Balance</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>



@foreach($clientProperties as $items)

<tr>

  <th scope="row" align="center">{{$count++}}</th>
  <td align="center">{{$items->propertyName }}</td>
  <td align="center">{{$items->block}}</td>

  <td align="center">{{$items->lot}}</td>
  <td align="center">{{number_format(round($items->contractPrice,2),2,'.',',')}}</td>
  <td align="center">{{number_format(round($items->monthlyAmortization,2),2,'.',',')}}  </td>
  <td align="center">{{number_format(round($items->totalPaid ,2),2,'.',',') }}</td>
  <td align="center">{{number_format(round((($items->contractPrice - $items->totalPaid)) ,2),2,'.',',') }}</td>


  <td align="center">
    @if($items->isSign == '0')
<a class="btn btn-sm btn-outline-primary" href="{{'/Signed/'.$items->cp_id}}" role="button">Contract Signed</a>
@endif

    <a class="btn btn-sm btn-outline-secondary" id="getRequest" href="#" data-cp_id="{{$items->cp_id}}" data-propid="{{$items->propertylistid}}" data-toggle="modal" data-target="#forfeit" role="button">Forfeit</a>
      <a class="btn btn-sm btn-outline-primary" href="#" data-cp_id="{{$items->cp_id}}" data-tcp="{{$items->contractPrice}}" data-comnum="{{$items->comnum}}" data-propid="{{$items->propertylistid}}" data-propnym="{{$items->propertyName}} Block {{$items->block}} Lot {{$items->lot}} " role="button" data-toggle="modal" data-target="#assume">Assume</a>
    <a class="btn btn-sm btn-outline-success" href="{{'/paymenthistory/'.$items->cp_id}}" role="button">Payment History</a>
    <a class="btn btn-sm btn-outline-primary" href="#" data-tcp="{{$items->contractPrice}}" data-comnum="{{$items->comnum}}" data-dcom="{{$items->directCommission}}" data-umcom="{{$items->unitManagerCommission}}" data-mcom="{{$items->managerCommission}}" data-mname="{{$items->DirectFname}}  {{$items->DirectLname}}" data-mnname="{{$items->ManagerFname}}  {{$items->ManagerLname}}" data-umname="{{$items->UnitManagerFname}}  {{$items->UnitManagerLname}}" data-propid="{{$items->propertylistid}}" role="button" data-toggle="modal" data-target="#getagents">AGENTS</a>
    @if($items->isFullyPaid == '0')
  
    <a class="btn btn-sm btn-outline-primary" href="#" role="button" data-toggle="modal" data-target="#addTitilingFee">Title Fee</a>
    @endif
</td>
</tr>

@endforeach
</tbody>
</table>
<nav aria-label="Page navigation example">

</nav>

<!-- Modal -->
<div class="modal fade" id="forfeit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Super Admin Password</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="/propertyList/tryForfeit">
            @csrf
        <input type="password" class="form-control" id="password" name="password">
        <input type="hidden" class="form-control" id="cpFormid" name="cpFormid">
        <input type="hidden" class="form-control" id="propp" name="propp">
   

      </div>
      <div class="modal-footer">

        <button type="submit" class="btn btn-primary">Save changes</button>
          </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="addTitilingFee" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Add Titling Cost</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="/propertyList/tryForfeit">
            @csrf
        <input type="text" class="form-control" id="titleCost" name="titleCost" placeholder="Title Fee!!">
        <input type="hidden" class="form-control" id="cpFormid" name="cpFormid">
        <input type="hidden" class="form-control" id="propp" name="propp">
   

      </div>
      <div class="modal-footer">

        <button type="submit" class="btn btn-primary">Save changes</button>
          </form>
      </div>
    </div>
  </div>
</div>
<!-- AGENT -->
<div class="modal fade" id="getagents" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel"> <b> Agents: </b></h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      <div class="form-row">
                          <div class="form-group col-md-4">

                           <h3><b> Direct Agent </b></h3> 
                            <div class="dagent "> <p id="dagent"></p></div>

                          </div>
                          <div class="form-group col-md-4 mx-auto">
                          <h3><b>Agent Manager </b></h3> 
                           <h4><b> <div class="magent"><p id="magent"></p></div> </b></h4>
                          </div>
                          <div class="form-group col-md-4 mx-auto">
                          <h3><b>Agent Unit Manager </b></h3> 
                          <h4><b> <div class="umagent"><p id="umagent"></p></div></b></h4>
                          </div>
                        </div>
      

      </div>
      <div class="modal-footer">

       
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="addProperty" tabindex="-1" aria-labelledby="exampleModalLabelssss" aria-hidden="true">
  <div class="modal-dialog modal-lg ">
    <div class="modal-content ">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Add Client Property</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="addClientProp">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <div class="container">

          <div class="form-row">
             <div class="form-group col-md-4">
               <label for="product_name">Select Property</label>
               <select class="form-control" name="property" id="property">
                  <option value="">Select Property</option>
                  @foreach($propertyNameList as $plist)
                 <option value="{{$plist->propId}}">{{$plist->propertyName}}</option>
                @endforeach
               </select>

             </div>
             <div class="form-group col-md-4 mx-auto">
               <label for="Unit">Specific Block</label>
               <select class="form-control" name="pList" id="pList">
                 <option value="">Select Block</option>

               </select>
             </div>
             <div class="form-group col-md-4 mx-auto">
               <label for="Unit">Specific Lot</label>
               <select class="form-control" name="lotList" id="lotList">
                 <option value="">Select Lot</option>

               </select>
             </div>
           </div>

          
             <div class="form-row">

               <div class="form-group col-md-4 mx-auto">
                   <label for="Agent_name">Direct Agent</label>
                   <select style="width:240px;" id="directAgent">
                      <option value="">Select Agent</option>

                   </select>
                 </div>
               <div class="form-group col-md-4 mx-auto">
                 <label for="product_name">Unit Manager</label>
                 <select style="width:240px;" id="unitmanager" name="unitmanager">
                    <option value="">Select Agent</option>

                 </select>
                 <small id="asNameError" class="form-text text-muted"></small>
               </div>
               <div class="form-group col-md-4 mx-auto">
                 <label for="Unit">Manager</label>
                 <select class="form-control" style="width:240px;" name="manager" id="manager">
                   <option value="">Select Agent</option>

                 </select>
               </div>

               </div>
               <div class="form-row">

                   <div class="form-group col-md-2 mx-auto">
                     <label for="product_name">Percent</label>
                       <input type="text" class="form-control" id="dcper" name="dcper" value="0">
                     <small id="asNameError" class="form-text text-muted"></small>
                   </div>
                   <div class="form-group col-md-2 mx-auto">
                     <label for="product_name">Commission</label>
                       <input type="text" class="form-control" id="dcom" name="dcom" readonly>
                     <small id="asNameError" class="form-text text-muted"></small>
                   </div>
                   <div class="form-group col-md-2 mx-auto">
                     <label for="product_name">Percent</label>
                       <input type="text" class="form-control" id="umper" name="umper" value="0">
                     <small id="asNameError" class="form-text text-muted"></small>
                   </div>
                   <div class="form-group col-md-2 mx-auto">
                     <label for="product_name">Commission</label>
                       <input type="text" class="form-control" id="umcom" name="umcom" readonly>
                     <small id="asNameError" class="form-text text-muted"></small>
                   </div>
                   <div class="form-group col-md-2 mx-auto">
                     <label for="product_name">Percent</label>
                       <input type="text" class="form-control" id="mper" name="mper" value="0">
                     <small id="asNameError" class="form-text text-muted"></small>
                   </div>
                   <div class="form-group col-md-2 mx-auto">
                     <label for="product_name">Commission</label>
                       <input type="text" class="form-control" id="mcom" name="mcom" readonly>
                     <small id="asNameError" class="form-text text-muted"></small>
                   </div>
</div>
<div class="form-row">

<div class="form-group col-md-4">
  <label for="product_name">Total Contract Price</label>
  <input type="text" class="form-control" id="contractPrice" name="contractPrice" readonly>
  <small id="asNameError" class="form-text text-muted"></small>
  <input type="hidden" id="grant1" name="grant1">
  <input type="hidden" id="grant2" name="grant2">
  <input type="hidden" id="grant3" name="grant3">
  <input type="hidden" id="grant4" name="grant4">
  <input type="hidden" id="grant5" name="grant5">
  <input type="hidden" id="grant1Per" name="grant1Per">
  <input type="hidden" id="grant2Per" name="grant2Per">
  <input type="hidden" id="grant3Per" name="grant3Per">
  <input type="hidden" id="grant4Per" name="grant4Per">
  <input type="hidden" id="grant5Per" name="grant5Per">
  <input type="hidden" id="grant1Com" name="grant1Com">
  <input type="hidden" id="grant2Com" name="grant2Com">
  <input type="hidden" id="grant3Com" name="grant3Com">
  <input type="hidden" id="grant4Com" name="grant4Com">
  <input type="hidden" id="grant5Com" name="grant5Com">
</div>
<div class="form-group col-md-4 mx-auto">
  <label for="Unit">PlanTerms</label>
<input type="text" class="form-control" id="terms" name="terms" >
  <small id="asTpypeError" class="form-text text-muted"></small>
</div>
<div class="form-group col-md-4 mx-auto">
  <label for="product_name">Monthly Amortization</label>
    <input type="text" class="form-control" id="Monthly" name="Monthly">
      <input type="hidden" id="cid" name="cid" value="{{$ClientProperty->client_id }}">
  <small id="asNameError" class="form-text text-muted"></small>
</div>
</div>
<div class="form-row">
                   <div class="form-group col-md-2 mx-auto">
                     <label for="product_name">Due Date</label>
                       <input type="text" class="form-control" id="due" name="due">
                     <small id="asNameError" class="form-text text-muted"></small>
                   </div>
                   <div class="form-group col-md-2 mx-auto">
                     <label for="product_name">Com Release</label>
                       <input type="text" class="form-control" id="comRel" name="comRel">
                     <small id="asNameError" class="form-text text-muted"></small>
                   </div>
                   <div class="form-group col-md-4 mx-auto">
                     <label for="product_name">Reservation</label>
                       <input type="text" class="form-control" id="res" name="res" value="0">
                     <small id="asNameError" class="form-text text-muted"></small>
                   </div>
                   <div class="form-group col-md-4 mx-auto">
                     <label for="product_name">Or Number</label>
                       <input type="text" class="form-control" id="OR" name="OR" value="000-0000-000">
                     <small id="asNameError" class="form-text text-muted"></small>
                   </div>

                 </div>
                 <div class="form-row">

<div class="form-group col-md-4 mx-auto">
<label for="product_name">Custom Date</label>
<input type="date" class="form-control" id="date" name="date">
</div>
</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>

                    </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal for Assume -->
<div class="modal fade" id="assume" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="prop">'+dialog_title+'</h3> 
        <!-- <span id="prop">'+dialog_title+'</span> -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form id="assume">
           @csrf
        <div class="container">
        <div class="form-row">
        <div class="form-group col-md-12 mx-auto">
        <label for="Agent_name">Client to Assume</label>
                   <select style="width:240px;" id="client" name="client">
                      <option value="">Select Client</option>

                   </select>
                   <small id="ClientError" class="form-text text-muted"></small>
        </div>
        </div>
             <div class="form-row">

               <div class="form-group col-md-4 mx-auto">
                   <label for="Agent_name">Direct Agent</label>
                   <select style="width:240px;" id="directAgent1" name="directAgent1">
                      <option value="">Select Agent</option>

                   </select>
                 </div>
               <div class="form-group col-md-4 mx-auto">
                 <label for="product_name">Unit Manager</label>
                 <select style="width:240px;" id="unitmanager1" name="unitmanager1">
                    <option value="">Select Agent</option>

                 </select>
                 <small id="asNameError" class="form-text text-muted"></small>
               </div>
               <div class="form-group col-md-4 mx-auto">
                 <label for="Unit">Manager</label>
                 <select class="form-control" style="width:240px;" name="manager1" id="manager1">
                   <option value="">Select Agent</option>

                 </select>
               </div>
               <small id="AgentError" class="form-text text-muted"></small>
               </div>
               <div class="form-row">
               
                   <div class="form-group col-md-2 mx-auto">
                     <label for="product_name">Percent</label>
                       <input type="text" class="form-control" id="dcper1" name="dcper1" value="0.00" >
                     
                   </div>
                   <div class="form-group col-md-2 mx-auto">
                     <label for="product_name">Commission</label>
                       <input type="text" class="form-control" id="dcom1" name="dcom1" readonly>
                     <small id="asNameError" class="form-text text-muted"></small>
                   </div>
                   <div class="form-group col-md-2 mx-auto">
                     <label for="product_name">Percent</label>
                       <input type="text" class="form-control" id="umper1" name="umper1"  value="0.00" >
                     <small id="asNameError" class="form-text text-muted"></small>
                   </div>
                   <div class="form-group col-md-2 mx-auto">
                     <label for="product_name">Commission</label>
                       <input type="text" class="form-control" id="umcom1" name="umcom1" readonly>
                     <small id="asNameError" class="form-text text-muted"></small>
                   </div>
                   <div class="form-group col-md-2 mx-auto">
                     <label for="product_name">Percent</label>
                       <input type="text" class="form-control" id="mper1" name="mper1"  value="0.00" >
                     <small id="asNameError" class="form-text text-muted"></small>
                   </div>
                   <div class="form-group col-md-2 mx-auto">
                     <label for="product_name">Commission</label>
                       <input type="text" class="form-control" id="mcom1" name="mcom1" readonly>
                     <small id="asNameError" class="form-text text-muted"></small>
                   </div>
                   <small id="persError" class="form-text text-muted"></small>
                   <input type="hidden" class="form-control" id="tcp" name="tcp">
                   <input type="hidden" class="form-control" id="cpid" name="cpid">
                   <input type="hidden" class="form-control" id="propid" name="propid">
        <input type="hidden" class="form-control" id="comnum" name="comnum">
                   

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
</form>
      </div>
      <div class="successni"><h2><p class="text-success"id="successni"></p></h2></div>
    </div>
  </div>
</div>
<!-- Modal for Assume -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script type="text/javascript">
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function(){
  $('#property').on('change', function () {
                 let prop_id = $(this).val();
                 $('#pList').empty();
                 $('#pList').append(`<option value="0" disabled selected>Processing...</option>`);
                 $.ajax({
                 type: 'GET',
                 url: '/propertyLists/' + prop_id,
                 success: function (response) {
                 var response = JSON.parse(response);
                 // console.log(response);
                 $('#pList').empty();
                 $('#pList').append(`<option value="0" disabled selected>Select Block*</option>`);
                 response.forEach(element => {
                     $('#pList').append(`<option value="${element['block']}">Block ${element['block']}</option>`);
                     });
                 }
             });
         });
////////////////////////////////////SAVING////////////////////////////////////////////////////////

$('#addClientProp').submit(function(e){
  e.preventDefault();
  var property = $('#property').val();
  var pList = $('#pList').val();
  var lotList = $('#lotList').val();
  var directAgent = $('#directAgent').val();
  var unitmanager = $('#unitmanager').val();
  var manager = $('#manager').val();
  var dcper = parseFloat($('#dcper').val());
  var dcom = $('#dcom').val();
  var umper = parseFloat($('#umper').val());
  var umcom = $('#umcom').val();
  var mper = parseFloat($('#mper').val());
  var mcom = $('#mcom').val();
  var dcom = $('#dcom').val();
  var umcom = $('#umcom').val();
  var mcom = $('#mcom').val();
  var grant1 = $('#grant1').val();
  var grant2 = $('#grant2').val();
  var grant3 = $('#grant3').val();
  var grant4 = $('#grant4').val();
  var grant5 = $('#grant5').val();
  var grant1Per = $('#grant1Per').val();
  var grant2Per = $('#grant2Per').val();
  var grant3Per = $('#grant3Per').val();
  var grant4Per = $('#grant4Per').val();
  var grant5Per = $('#grant5Per').val();
  var grant1Com = $('#grant1Com').val();
  var grant2Com = $('#grant2Com').val();
  var grant3Com = $('#grant3Com').val();
  var grant4Com = $('#grant4Com').val();
  var grant5Com = $('#grant5Com').val();
  var terms = $('#terms').val();
  var Monthly = $('#Monthly').val();
  var cid = $('#cid').val();
  var due = $('#due').val();
  var comRel = $('#comRel').val();
  var res = $('#res').val();
  var OR = $('#OR').val();
  var date = $('#date').val();
  var _token = $("input[name=_token]").val();
  var total = dcper+umper+mper;
  if(total !=10){
    
 
    Swal.fire({
  type: "error",
  title: 'Check Agent Commission',
  text: 'Something Missing!',
  footer: '<a href="">Agent Commission Percentage should be equal to 10.</a>'
});
  }
  else if(lotList=="" || lotList==0)
  {
    Swal.fire({
  type: "error",
  title: 'No Property Selected',
  text: 'Something Missing!',
  footer: '<a href="">Please select property to add.</a>'
});
  }
  else if(due =="" )
  {
    Swal.fire({
  type: "error",
  title: 'Check Due Date',
  text: 'Something Missing!',
  footer: '<a href="">Client due date should be set.</a>'
});
  }

  else{
    $.ajax({
        url:"{{route('clientProperties.store')}}",
        type:"post",
        data:{
property:property,
pList:pList,
lotList:lotList,
directAgent:directAgent,
unitmanager:unitmanager,
manager:manager,
dcper:dcper,
dcom:dcom,
umper:umper,
umcom:umcom,
mper:mper,
mcom:mcom,
dcom:dcom,
umcom:umcom,
mcom:mcom,
grant1:grant1,
grant2:grant2,
grant3:grant3,
grant4:grant4,
grant5:grant5,
grant1Per:grant1Per,
grant2Per:grant2Per,
grant3Per:grant3Per,
grant4Per:grant4Per,
grant1Com:grant1Com,
grant2Com:grant2Com,
grant3Com:grant3Com,
grant4Com:grant4Com,
grant5Com:grant5Com,
terms:terms,
Monthly:Monthly,
cid:cid,
due:due,
comRel:comRel,
res:res,
OR:OR,
date:date,
_token:_token
        },
        success:function(response){
console.log(response);

Swal.fire({
  position: 'center',
  type: 'success',
  title: 'Added Successfully',
  showConfirmButton: true,
  timer: 6000000
});
$("#addClientProp").trigger("reset");
$("#addProperty .close").click();
  

}
    });
  }

    

})


////////////////////////////////////SAVING////////////////////////////////////////////////////////

$('#assume').on('change', function () {
  var client = $('#client').val();
  var tcp = $('#tcp').val();
  var comnum = $('#comnum').val();
  var directAgent1 = $('#directAgent1').val();
  var unitmanager1 = $('#unitmanager1').val();
  var manager1 = $('#manager1').val();
  var dcper1 = parseFloat($('#dcper1').val());
  var umper1 = parseFloat($('#umper1').val());
  var mper1 = parseFloat($('#mper1').val());
  var unitmanager = $('#unitmanager').val();
  var manager = $('#manager').val();
  var dcper = parseFloat($('#dcper').val());
  var dcom = $('#dcom').val();
  var dcom1 = $('#dcom1').val();
  var umcom1 = $('#umcom1').val();
  var mcom1 = $('#mcom1').val();

  var totatper = dcper1 + umper1 + mper1;
  if(client=="") {
    $("#ClientError").html("<span class='text-danger'>NO CLIENT SELECTED!</span>");
  }
  else
  {
    $("#ClientError").html("");
  }
  if(directAgent1 =="" && unitmanager1 =="" && manager1=="")
  {
    $("#AgentError").html("<span class='text-danger'>NO AGENT SELECTED!</span>");
  }
  else
  {
    $("#AgentError").html("");
  }
  if(dcper1=="0.00")
  {
    $('#dcom1').val("0.00");
  }
  else{
    var com1 = (tcp * (dcper1/100))/comnum;
    $('#dcom1').val(parseFloat(com1));
  }
  if(umper1=="0.00")
  {
    $('#umcom1').val("0.00");
  }
  else{
    var com2 = (tcp * (umper1/100))/comnum;
    $('#umcom1').val(parseFloat(com2));
  }
  if(mper1=="0.00")
  {
    $('#mcom1').val("0.00");
  }
  else{
    var com3 = (tcp * (mper1/100))/comnum;
    $('#mcom1').val(parseFloat(com3));
  }
  if(totatper!=10)
  {
    $("#dcper1").addClass("border-danger");
    $("#umper1").addClass("border-danger");
    $("#mper1").addClass("border-danger");
    $("#persError").html("<span class='text-danger'>Total Percentage should be 10!</span>"); 
  }
  else{
    $("#dcper1").removeClass("border-danger");
    $("#umper1").removeClass("border-danger");
    $("#mper1").removeClass("border-danger");
    $("#persError").html(""); 
  }
 
 
 
                });
//////////////////////////////  A   S   S   U   M   E   //////////////////////////////////////////

$('#assume').submit(function(e){
  e.preventDefault();
  var client = $('#client').val();
  var cpid = $('#cpid').val();
  var directAgent1 = $('#directAgent1').val();
  var unitmanager1 = $('#unitmanager1').val();
  var manager1 = $('#manager1').val();
  var dcom1 = parseFloat($('#dcom1').val());
  var umcom1 = parseFloat($('#umcom1').val());
  var mcom1 = parseFloat($('#mcom1').val());
  var dcper1 = parseFloat($('#dcper1').val());
  var umper1 = parseFloat($('#umper1').val());
  var mper1 = parseFloat($('#mper1').val());
  var propid = $('#propid').val();
  
  var _token = $("input[name=_token]").val();
  var total = dcper1+umper1+mper1;
  if(client=="")
  {
    Swal.fire({
  type: "error",
  title: 'Check Client',
  text: 'Something Missing!',
  footer: '<a href="">Please select a client.</a>'
});
  }
  else if(directAgent1 =="" && unitmanager1 =="" && manager1=="")
  {
    Swal.fire({
  type: "error",
  title: 'Check Agent',
  text: 'Something Missing!',
  footer: '<a href="">Please select an Agent.</a>'
});
  }
  else if(total !=10){
    
 
    Swal.fire({
  type: "error",
  title: 'Check Agent Commission',
  text: 'Something Missing!',
  footer: '<a href="">Agent Commission Percentage should be equal to 10.</a>'
});
  }

 

  else{
    $.ajax({
        url:"{{route('clientProperties.assume')}}",
        type:"post",
        data:{
cpid:cpid,
propid:propid,
client:client,
directAgent1:directAgent1,
unitmanager1:unitmanager1,
manager1:manager1,
dcom1:dcom1,
umcom1:umcom1,
mcom1:mcom1,
_token:_token
        },
        success:function(response){
console.log(response);

Swal.fire({
  position: 'center',
  type: 'success',
  title: 'Assumed Successfully',
  showConfirmButton: true,
  timer: 6000000
});

  

}
    });
  }

    

})
/////////////////////////////   A   S   S   U   M   E   //////////////////////////////////////////

         $('#forfeit').on('show.bs.modal', function (event) {
             var button = $(event.relatedTarget)
             var cp_id = button.data('cp_id')
             var propid = button.data('propid')
             $('#cpFormid').val(cp_id);
             $('#propp').val(propid);

     propp
         });
         $('#getagents').on('show.bs.modal', function (event) {
             var button = $(event.relatedTarget)
             var mname = button.data('mname')
             var mnname = button.data('mnname')
             var umname = button.data('umname')
             var tcp = button.data('tcp')
             var comnum = button.data('comnum')
             var dcom = button.data('dcom')
             var umcom = button.data('umcom')
             var mcom = button.data('mcom')
            var com1 = (((comnum*dcom)/tcp)*100);
            var com2 = (((comnum*umcom)/tcp)*100);
            var com3 = (((comnum*mcom)/tcp)*100);
              if(mnname == '  ')
              {
                manager = 'NO MANAGER';
              }
              else{
                manager = mnname;
              }
              if(umname == '  ')
              {
                umanager = 'NO UNIT MANAGER';
              }
              else{
                umanager = umname;
              }
             $('#dagent').html('');
             $('#magent').html('');
             $('#umagent').html('');
             $('#dagent').html('<h4><b>' +  mname + '<br> ' + (dcom-(dcom * 0.05)).toFixed(2) + ' (' +  Math.round(com1) + '%)' + '</h4></b>');
             $('#magent').html('<h4><b>' + manager + '<br> ' + (mcom-(mcom * 0.05)).toFixed(2) + ' (' +  Math.round(com3) + '%)' + '</h4></b>');
             $('#umagent').html('<h4><b>' + umanager + '<br> ' + (umcom-(umcom * 0.05)).toFixed(2) + ' (' +  Math.round(com2) + '%)' + '</h4></b>');

     
         });
         $('#assume').on('show.bs.modal', function (event) {
             var button = $(event.relatedTarget)
             var cp_id = button.data('cp_id')
             var propid = button.data('propid')
             var propnym = button.data('propnym')
             var tcp = button.data('tcp')
             var comnum = button.data('comnum')
        
                
             $('#cpid').val(cp_id);
             $('#tcp').val(tcp);
             $('#comnum').val(comnum);
             $('#propid').val(propid);
             $('#prop').text('Property Name: ' + propnym);
             

     
         });
         $('#pList').on('change', function () {
                        let block = $(this).val();
                        var prop_id = $('#property').val();
                        $('#lotList').empty();
                        $('#lotList').append(`<option value="0" disabled selected>Processing...</option>`);
                        $.ajax({
                        type: 'GET',
                        url: '/propertyListsActive/' + block +'/'+ prop_id ,
                        success: function (response) {
                        var response = JSON.parse(response);
                        // console.log(response);
                        $('#lotList').empty();
                        $('#lotList').append(`<option value="0" disabled selected>Select Lot*</option>`);
                        response.forEach(element => {
                            $('#lotList').append(`<option value="${element['propertylistid']}">Lot ${element['lot']}</option>`);
                            });
                        }
                    });
                });

                $('#lotList').on('change', function () {
                               let propertylist = $(this).val();
                               $.ajax({
                               type: 'GET',
                               dataType: 'json',
                               url: '/propertyListsView/' + propertylist ,
                               success: function (data) {
                               // var data = JSON.parse(data);
                               console.log(data[0]);
                                $('#contractPrice').val(data[0].contractPrice);
                                $('#grant1').val(data[0].gc1);
                                $('#grant2').val(data[0].gc2);
                                $('#grant3').val(data[0].gc3);
                                $('#grant4').val(data[0].gc4);
                                $('#grant5').val(data[0].gc5);
                                $('#grant1Per').val(data[0].gc1Per);
                                $('#grant2Per').val(data[0].gc2Per);
                                $('#grant3Per').val(data[0].gc3Per);
                                $('#grant4Per').val(data[0].gc4Per);
                                $('#grant5Per').val(data[0].gc5Per);

                               //alert(data[0].contractPrice);


                               }
                           });
                           $.ajax({
                           type: 'GET',
                           dataType: 'json',
                           url: '/propertyListsView/' + propertylist ,
                           success: function (data) {
                           // var data = JSON.parse(data);
                           console.log(data[0]);
                            $('#contractPrice').val(data[0].contractPrice);
                           //alert(data[0].contractPrice);


                           }
                       });
                       });
                       //function for fetching manager and unit manager
                       $('#directAgent').on('change', function () {
    let agentid = $(this).val();
    
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/agentFind/' + agentid,
        success: function (data) {
            console.log(data[0]);

            var managerid = data[0].manager;
            var unitmanagerid = data[0].unitmanager;

            // AJAX for Manager
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: '/agentFind/' + managerid,
                success: function (data1) {
                    console.log(data1[0]);

                    // Clear previous options and set new selected option for #manager
                    $('#manager').empty(); // Clear options
                    var name = data1[0].AgentFname + ' ' + data1[0].AgentLname;
                    var $option = $("<option selected></option>").val(managerid).text(name);
                    $('#manager').append($option).trigger('change');
                }
            });

            // AJAX for Unit Manager
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: '/agentFind/' + unitmanagerid,
                success: function (data2) {
                    console.log(data2[0]);

                    // Clear previous options and set new selected option for #unitmanager
                    $('#unitmanager').empty(); // Clear options
                    var name1 = data2[0].AgentFname + ' ' + data2[0].AgentLname;
                    var $option1 = $("<option selected></option>").val(unitmanagerid).text(name1);
                    $('#unitmanager').append($option1).trigger('change');
                }
            });
        }
    });
});

                      //function for fetching manager and unit manager
                       $('#comRel').on('change', function () {
                                      let release = $(this).val();

                              let price = $('#contractPrice').val();
                              gc1 =   (price*($('#grant1Per').val()/100))/release;
                              gc2 =   (price*($('#grant2Per').val()/100))/release;
                              gc3 =   (price*($('#grant3Per').val()/100))/release;
                              gc4 =   (price*($('#grant4Per').val()/100))/release;
                              gc5 =   (price*($('#grant5Per').val()/100))/release;
                              dcom = (price*($('#dcper').val()/100))/release;
                              umcom = (price*($('#umper').val()/100))/release;
                              mcom = (price*($('#mper').val()/100))/release;
                              $('#dcom').val(dcom);
                              $('#umcom').val(umcom);
                              $('#mcom').val(mcom);

                              $('#grant1Com').val(gc1);
                              $('#grant2Com').val(gc2);
                              $('#grant3Com').val(gc3);
                              $('#grant4Com').val(gc4);
                              $('#grant5Com').val(gc5);
                              });

                       $('#res').on('change', function () {

                              $('#Monthly').val((($('#contractPrice').val()-$('#res').val())/$('#terms').val()).toFixed(2));
                              });
                       $('#terms').on('change', function () {
                        let terms = $(this).val();
                              $('#Monthly').val((($('#contractPrice').val()-$('#res').val())/$('#terms').val()).toFixed(2));
                              if($('#terms').val()=='72')
                              {
                                $('#comRel').val(16);
                                var release = 16;
                              }
                              else if($('#terms').val()=='60')
                              {
                                $('#comRel').val(12);
                                var release = 12;
                              }
                              else
                              {
                                $('#comRel').val(terms);
                                var release = terms; 
                              }
                              var price = $('#contractPrice').val();
                              gc1 =   (price*($('#grant1Per').val()/100))/release;
                              gc2 =   (price*($('#grant2Per').val()/100))/release;
                              gc3 =   (price*($('#grant3Per').val()/100))/release;
                              gc4 =   (price*($('#grant4Per').val()/100))/release;
                              gc5 =   (price*($('#grant5Per').val()/100))/release;
                              dcom = (price*($('#dcper').val()/100))/release;
                              umcom = (price*($('#umper').val()/100))/release;
                              mcom = (price*($('#mper').val()/100))/release;
                              $('#dcom').val(dcom);
                              $('#umcom').val(umcom);
                              $('#mcom').val(mcom);

                              $('#grant1Com').val(gc1);
                              $('#grant2Com').val(gc2);
                              $('#grant3Com').val(gc3);
                              $('#grant4Com').val(gc4);
                              $('#grant5Com').val(gc5);
                     
                              });
                      $('#directAgent').select2({
                        dropdownParent: $('#addProperty'),
                        ajax:{
                          url:"/agent/getList",
                          type:"post",
                          dataType:"json",
                          data: function(params){
                            return{
                            _token:CSRF_TOKEN,
                            search: params.term
                          };
                        },
                        processResults: function(response){
                          return{
                            results: response
                          };
                        },
                        cache :true
                      }
                    });
                    $('#directAgent1').select2({
                      dropdownParent: $('#assume'),
                        ajax:{
                          url:"/agent/getList",
                          type:"post",
                          dataType:"json",
                          data: function(params){
                            return{
                            _token:CSRF_TOKEN,
                            search: params.term
                          };
                        },
                        processResults: function(response){
                          return{
                            results: response
                          };
                        },
                        cache :true
                      }
                    });
                    $('#unitmanager').select2({
    dropdownParent: $('#addProperty'), // Specify the modal as the dropdown parent
    ajax: {
        url: "/agent/getList",
        type: "post",
        dataType: "json",
        data: function(params) {
            return {
                _token: CSRF_TOKEN, // Ensure CSRF token is set
                search: params.term // Pass search term to server
            };
        },
        processResults: function(response) {
            console.log("Response:", response); // Check if response format is correct
            return {
                results: response // Ensure the response is an array of objects with `id` and `text`
            };
        },
        cache: true
    }
});
                  $('#unitmanager1').select2({
    dropdownParent: $('#assume'), // Specify the modal as the dropdown parent
    ajax: {
        url: "/agent/getList",
        type: "post",
        dataType: "json",
        data: function(params) {
            return {
                _token: CSRF_TOKEN, // Ensure CSRF token is set
                search: params.term // Pass search term to server
            };
        },
        processResults: function(response) {
            console.log("Response:", response); // Check if response format is correct
            return {
                results: response // Ensure the response is an array of objects with `id` and `text`
            };
        },
        cache: true
    }
});

                  $('#manager').select2({
                    dropdownParent: $('#addProperty'),
    ajax: {
        url: "/agent/getList",
        type: "post",
        dataType: "json",
        data: function(params) {
            return {
                _token: CSRF_TOKEN, // Ensure CSRF token is set
                search: params.term // Pass search term to server
            };
        },
        processResults: function(response) {
            console.log("Response:", response); // Check if response format is correct
            return {
                results: response // Ensure the response is an array of objects with `id` and `text`
            };
        },
        cache: true,
        error: function(jqXHR, textStatus, errorThrown) {
            console.error("Error:", textStatus, errorThrown); // Log any errors in the AJAX request
        }
    }
});

                  $('#manager1').select2({
                    dropdownParent: $('#assume'),
                      ajax:{
                        url:"/agent/getList",
                        type:"post",
                        dataType:"json",
                        data: function(params){
                          return{
                          _token:CSRF_TOKEN,
                          search: params.term
                        };
                      },
                      processResults: function(response){
                        return{
                          results: response
                        };
                      },
                      cache :true
                    }
                  });
                  $('#client').select2({
                      ajax:{
                        url:"/client/getList",
                        type:"post",
                        dataType:"json",
                        data: function(params){
                          return{
                          _token:CSRF_TOKEN,
                          search: params.term
                        };
                      },
                      processResults: function(response){
                        return{
                          results: response
                        };
                      },
                      cache :true
                    }
                  });



});

</script>
@endsection
