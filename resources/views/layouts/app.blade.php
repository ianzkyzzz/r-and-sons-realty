<!--
 =========================================================
* Black Dashboard PRO - v1.1.1
=========================================================

* Product Page: https://themes.getbootstrap.com/product/black-dashboard-pro-premium-bootstrap-4-admin/
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="apple-touch-icon" sizes="80x80" href="{{asset('img/dias.png')}}">
  <link rel="icon" type="image/png" href="{{asset('img/logo.jpg')}}">


<title>{{ config('app.name', 'Laravel') }}</title>
<!-- <script src="{{ asset('js/app.js') }}" defer></script> -->
  <!--     Fonts and icons     -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.2.0/sweetalert2.min.css">
  <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,600,700,800" rel="stylesheet" />
  <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
  <!-- Nucleo Icons -->
  <link href="{{asset('assets/css/nucleo-icons.css')}}" rel="stylesheet" />
  <!-- CSS Files -->
  <link href="{{asset('assets/css/black-dashboard.css?v=1.1.1')}}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="{{asset('assets/demo/demo.css')}}" rel="stylesheet" />
  <link rel="stylesheet" type="text/css" href="{{asset('select2/dist/css/select2.min.css')}}">
  
<!-- Select2 js -->
    <script src="{{ asset('select2/dist/js/select2.min.js') }}" defer></script>
</head>

<body class="sidebar-mini white-content">
  <div class="wrapper">
    <div class="navbar-minimize-fixed">
      <button class="minimize-sidebar btn btn-link btn-just-icon">
        <i class="tim-icons icon-align-center visible-on-sidebar-regular text-muted"></i>
        <i class="tim-icons icon-bullet-list-67 visible-on-sidebar-mini text-muted"></i>
      </button>
    </div>
    <div class="sidebar" data="blue">
      <div class="sidebar-wrapper">
        <div class="logo">
          <a href="javascript:void(0)" class="simple-text logo-mini">
            RNS
          </a>
          <a href="javascript:void(0)" class="simple-text logo-normal">
            R and Sons Properties Co.
          </a>
        </div>
        <ul class="nav">
          @guest
                  <li class="nav-item">
                      <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                  </li>

              @else
              @if (Auth::user()->role == 'Admin')
              <li class="nav-item dropdown">
              <a href="{{ url('/home') }}" class="nav-link text-sm text-gray-700 underline"><i class="tim-icons icon-atom"></i>
                <p>Home</p></a>
            </li>
                <li class="nav-item dropdown">
              <a href="{{ url('/client') }}" class="nav-link text-sm text-gray-700 underline"><i class="tim-icons icon-badge"></i>
                <p>Client</p></a>
            </li>
            @endif
            @if (Auth::user()->role == 'Partner' || Auth::user()->role == 'Admin')
            <li class="nav-item dropdown">
          <a href="{{ url('/partner') }}" class="nav-link text-sm text-gray-700 underline"><i class="tim-icons icon-chat-33"></i>
            <p>Partners</p></a>
        </li>
        @endif
        @if (Auth::user()->role == 'Admin')
            <li>
              <a data-toggle="collapse" href="#pagesExamples">
                <i class="tim-icons icon-coins"></i>
                <p>
                  Payment
                  <b class="caret"></b>
                </p>
              </a>
              <div class="collapse" id="pagesExamples">
                <ul class="nav">
                  <li>
                    <a href="{{ url('/Payment') }}">
                      <span class="sidebar-mini-icon">MP</span>
                      <span class="sidebar-normal"> Monthly Payment </span>
                    </a>
                  </li>
                  <li>
                    <a href="{{ url('/paymenthistory') }}">
                      <span class="sidebar-mini-icon">PH</span>
                      <span class="sidebar-normal">Payments</span>
                    </a>
                  </li>
                  <li>
                    <a href="{{ url('/OtherPayment') }}">
                      <span class="sidebar-mini-icon">TP</span>
                      <span class="sidebar-normal">Title Payments</span>
                    </a>
                  </li>
                    </div>
                    @endif
                    @if (Auth::user()->role == 'Admin')
                <li class="nav-item dropdown">
              <a href="{{ url('/property') }}" class="nav-link text-sm text-gray-700 underline"><i class="tim-icons icon-molecule-40"></i>
                <p>Properties</p></a>
            </li>
        @endif
        @if (Auth::user()->role == 'Admin')
                <li class="nav-item dropdown">
              <a href="{{ url('/agent') }}" class="nav-link text-sm text-gray-700 underline"><i class="tim-icons icon-single-02"></i>
                <p>Agents</p></a>
            </li>
          </li>
          @endif
          @if (Auth::user()->role == 'Admin')
              <li class="nav-item dropdown">
            <a href="{{ url('/userlist') }}" class="nav-link text-sm text-gray-700 underline"><i class="tim-icons icon-user-run"></i>
              <p>Users</p></a>
          </li>
          <li class="nav-item dropdown">
        <a href="{{ url('/expenses') }}" class="nav-link text-sm text-gray-700 underline"><i class="tim-icons icon-money-coins"></i>
          <p>Expenses</p></a>
      </li>
                <li class="nav-item dropdown">
              <a href="{{ url('/reports') }}" class="nav-link text-sm text-gray-700 underline"><i class="tim-icons icon-chart-bar-32"></i>
                <p>Reports</p></a>
            </li>
@endif

        </ul>
      </div>
    </div>
    <div class="main-panel" data="blue">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-absolute navbar-transparent">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-minimize d-inline">
              <button class="minimize-sidebar btn btn-link btn-just-icon" rel="tooltip" data-original-title="Sidebar toggle" data-placement="right">
                <i class="tim-icons icon-align-center visible-on-sidebar-regular"></i>
                <i class="tim-icons icon-bullet-list-67 visible-on-sidebar-mini"></i>
              </button>
            </div>
            <div class="navbar-toggle d-inline">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
            <a class="navbar-brand" href="{{ url('/') }}">

                  R and Sons Properties Co.

              </a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
          <div class="collapse navbar-collapse" id="navigation">
            <ul class="navbar-nav ml-auto">
              <li class="search-bar input-group">
                <button class="btn btn-link" id="search-button" data-toggle="modal" data-target="#searchModal"><i class="tim-icons icon-zoom-split"></i>
                  <span class="d-lg-none d-md-block">Search</span>
                </button>
              </li>
              <li class="dropdown nav-item">

                  <div class="notification d-none d-lg-block d-xl-block"></div>

                                      <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                          @csrf
                                      </form>
                </a>

              </li>

              <li class="dropdown nav-item">
                <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
                  <div class="photo">
                    <img src="{{asset('/storage/images/'.Auth::user()->avatar)}}" alt="{{asset('/storage/images/avatar.png')}}"/>

                  </div>

                  <b class="caret d-none d-lg-block d-xl-block"></b>


                </a>
                <ul class="dropdown-menu dropdown-navbar">
                  <li class="nav-link">
                    <a href="javascript:void(0)" class="nav-item dropdown-item">  {{ Auth::user()->name }}</a>
                  </li>
                  <li class="nav-link">
                    <a href="{{ url('/userprofile') }}" class="nav-item dropdown-item">Settings</a>
                  </li>
                  <li class="dropdown-divider"></li>
                  <li class="nav-link">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                        Log out
                    </a>
                  </li>
                </ul>
              </li>
              <li class="separator d-lg-none"></li>
            </ul>
                @endguest
          </div>
        </div>
      </nav>
      <div class="modal modal-search fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="SEARCH">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i class="tim-icons icon-simple-remove"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
      <!-- End Navbar -->
      <div class="content">
        <main class="py-4">
            @yield('content')
        </main>
      </div>
      <footer class="footer">
        <div class="container-fluid">

          <div class="copyright">
            ©
            <script>
              document.write(new Date().getFullYear())
            </script> made with <i class="tim-icons icon-heart-2"></i> by
            <a href="javascript:void(0)" target="_blank">R and Sons IT</a>.
          </div>
        </div>
      </footer>
    </div>
  </div>
  <div class="fixed-plugin">
   <div class="dropdown show-dropdown">
     <a href="#" data-toggle="dropdown">
       <i class="fa fa-cog fa-2x"> </i>
     </a>
     <ul class="dropdown-menu">
       <li class="header-title"> Sidebar Background</li>
       <li class="adjustments-line">
         <a href="javascript:void(0)" class="switch-trigger background-color">
           <div class="badge-colors text-center">
             <span class="badge filter badge-primary active" data-color="blue"></span>
             <span class="badge filter badge-info" data-color="blue"></span>
             <span class="badge filter badge-success" data-color="green"></span>
             <span class="badge filter badge-warning" data-color="orange"></span>
             <span class="badge filter badge-danger" data-color="red"></span>
           </div>
           <div class="clearfix"></div>
         </a>
       </li>
       <li class="header-title">
         Sidebar Mini
       </li>
       <li class="adjustments-line">
         <div class="togglebutton switch-sidebar-mini">
           <span class="label-switch">OFF</span>
           <input type="checkbox" name="checkbox" checked class="bootstrap-switch" data-on-label="" data-off-label="" />
           <span class="label-switch label-right">ON</span>
         </div>

       </li>
       <li class="button-container mt-4">

       </li>
       <li class="header-title"></li>
       <li class="button-container text-center">


       </li>
     </ul>
   </div>
 </div>
  <!--   Core JS Files   -->
  <script src="{{asset('assets/js/core/jquery.min.js')}}"></script>
  <script src="{{asset('assets/js/core/popper.min.js')}}"></script>
  <script src="{{asset('assets/js/core/bootstrap.min.js')}}"></script>
  <script src="{{asset('assets/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>
  <script src="{{asset('assets/js/plugins/moment.min.js')}}"></script>
  <!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
  <script src="{{asset('assets/js/plugins/bootstrap-switch.js')}}"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="{{asset('assets/js/plugins/sweetalert2.min.js')}}"></script>
  <!--  Plugin for Sorting Tables -->
  <script src="{{asset('assets/js/plugins/jquery.tablesorter.js')}}"></script>
  <!-- Forms Validations Plugin -->
  <script src="{{asset('assets/js/plugins/jquery.validate.min.js')}}"></script>
  <!--  Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="{{asset('assets/js/plugins/jquery.bootstrap-wizard.js')}}"></script>
  <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="{{asset('assets/js/plugins/bootstrap-selectpicker.js')}}"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="{{asset('assets/js/plugins/bootstrap-datetimepicker.js')}}"></script>
  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/    -->
  <script src="{{asset('assets/js/plugins/jquery.dataTables.min.js')}}"></script>
  <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="{{asset('assets/js/plugins/bootstrap-tagsinput.js')}}"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="{{asset('assets/js/plugins/jasny-bootstrap.min.js')}}"></script>
  <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="{{asset('assets/js/plugins/fullcalendar/fullcalendar.min.js')}}"></script>
  <script src="{{asset('assets/js/plugins/fullcalendar/daygrid.min.js')}}"></script>
  <script src="{{asset('assets/js/plugins/fullcalendar/timegrid.min.js')}}"></script>
  <script src="{{asset('assets/js/plugins/fullcalendar/interaction.min.js')}}"></script>
  <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
  <script src="{{asset('assets/js/plugins/jquery-jvectormap.js')}}"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="{{asset('assets/js/plugins/nouislider.min.js')}}"></script>
  <!--  Google Maps Plugin    -->
  <!-- Place this tag in your head or just before your close body tag. -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chart JS -->
  <script src="{{asset('assets/js/plugins/chartjs.min.js')}}"></script>
  <!--  Notifications Plugin    -->
  <script src="{{asset('assets/js/plugins/bootstrap-notify.js')}}"></script>
  <!-- Control Center for Black Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{asset('assets/js/black-dashboard.min.js?v=1.1.1')}}"></script>
  <!-- Black Dashboard DEMO methods, don't include it in your project! -->
  <script src="{{asset('assets/demo/demo.js')}}"></script>
  <script>
   $(document).ready(function() {

     $('#agetTable').DataTable({
             "pagingType": "full_numbers",
             "lengthMenu": [
               [10, 25, 50, -1],
               [10, 25, 50, "All"]
             ],
             responsive: true,
             language: {
               search: "_INPUT_",
               searchPlaceholder: "Search records",
             }

           });

$('#clientList').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          search: "_INPUT_",
          searchPlaceholder: "Search records",
        }

      });


  $('#clientpropertylistTable').DataTable({
          "pagingType": "full_numbers",
          "lengthMenu": [
            [10, 25, 50, -1],
            [10, 25, 50, "All"]
          ],
          responsive: true,
          language: {
            search: "_INPUT_",
            searchPlaceholder: "Search records",
          }

        });
        $('#propertylistTable').DataTable({
                "pagingType": "full_numbers",
                "lengthMenu": [
                  [10, 25, 50, "All"]
                ],
                responsive: true,
                language: {
                  search: "_INPUT_",
                  searchPlaceholder: "Search records",
                }

              });
              $('#propertyTable').DataTable({
                      "pagingType": "full_numbers",
                      "lengthMenu": [
                        [10, 25, 50, -1],
                        [10, 25, 50, "All"]
                      ],
                      responsive: true,
                      language: {
                        search: "_INPUT_",
                        searchPlaceholder: "Search records",
                      }

                    });
                    $sidebar = $('.sidebar');
       $navbar = $('.navbar');
       $main_panel = $('.main-panel');

       $full_page = $('.full-page');

       $sidebar_responsive = $('body > .navbar-collapse');
       sidebar_mini_active = true;
       white_color = false;

       window_width = $(window).width();

       fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();



       $('.fixed-plugin a').click(function(event) {
         if ($(this).hasClass('switch-trigger')) {
           if (event.stopPropagation) {
             event.stopPropagation();
           } else if (window.event) {
             window.event.cancelBubble = true;
           }
         }
       });

       $('.fixed-plugin .background-color span').click(function() {
         $(this).siblings().removeClass('active');
         $(this).addClass('active');

         var new_color = $(this).data('color');

         if ($sidebar.length != 0) {
           $sidebar.attr('data', new_color);
         }

         if ($main_panel.length != 0) {
           $main_panel.attr('data', new_color);
         }

         if ($full_page.length != 0) {
           $full_page.attr('filter-color', new_color);
         }

         if ($sidebar_responsive.length != 0) {
           $sidebar_responsive.attr('data', new_color);
         }
       });

       $('.switch-sidebar-mini input').on("switchChange.bootstrapSwitch", function() {
         var $btn = $(this);

         if (sidebar_mini_active == true) {
           $('body').removeClass('sidebar-mini');
           sidebar_mini_active = false;
           blackDashboard.showSidebarMessage('Sidebar mini deactivated...');
         } else {
           $('body').addClass('sidebar-mini');
           sidebar_mini_active = true;
           blackDashboard.showSidebarMessage('Sidebar mini activated...');
         }

         // we simulate the window Resize so the charts will get updated in realtime.
         var simulateWindowResize = setInterval(function() {
           window.dispatchEvent(new Event('resize'));
         }, 180);

         // we stop the simulation of Window Resize after the animations are completed
         setTimeout(function() {
           clearInterval(simulateWindowResize);
         }, 1000);
       });

       $('.switch-change-color input').on("switchChange.bootstrapSwitch", function() {
         var $btn = $(this);

         if (white_color == true) {

           $('body').addClass('change-background');
           setTimeout(function() {
             $('body').removeClass('change-background');
             $('body').removeClass('white-content');
           }, 900);
           white_color = false;
         } else {

           $('body').addClass('change-background');
           setTimeout(function() {
             $('body').removeClass('change-background');
             $('body').addClass('white-content');
           }, 900);

           white_color = true;
         }


       });

       $('.light-badge').click(function() {
         $('body').addClass('white-content');
       });

       $('.dark-badge').click(function() {
         $('body').removeClass('white-content');
       });


    });

  </script>
</body>

</html>
