@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                <div class="card-header"><div class="card-header"><h2> {{$PartnerName}}'s {{$propertyName}} Share</h2></div>

                <div class="card-body">
                  @if (Route::has('login'))
                    <div class="container-fluid">
                      <form method="post" action="/partnerShareGenerate">
                          @csrf
                          <div class="form-group">
                            <label for="exampleInputPassword1">From</label>
                              <input type="date" class="form-control" id="from" name="from">
                          </div>
                          <div class="form-group">
                            <label for="exampleInputEmail1">To</label>
                          <input type="date" class="form-control" id="To" name="To" >
                          <input type="hidden" id="propId" name="propId" value="{{$propId}}" >
                          <input type="hidden" id="partnerId" name="partnerId" value="{{$partnerId}}" >
                            <input type="hidden" id="percentage" name="percentage" value="{{$percentage}}" >
                            <input type="hidden" id="partnerName" name="partnerName" value="{{$PartnerName}}" >
                            <input type="hidden" id="propertyName" name="propertyName" value="{{$propertyName}}" >


                          </div>
                          <button class="btn-sm btn-outline-primary" type="submit">Generate</button>
                      </form>

@if(session()->has('message'))
<div class="alert alert-success">{{session()->get('message')}} </div>
@endif
<x-alert />
@if($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif



              </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editProperty" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Property</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="/partner/edit">
            @csrf
        <div class="form-group">
          <label for="exampleInputPassword1">Properties 1</label>
            <input type="textbox" class="form-control" id="PartnerNameEdit" name="PartnerNameEdit">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Address</label>
        <input type="textarea" class="form-control" id="PartnerAddressEdit" name="PartnerAddressEdit" >
        <input type="hidden" id="id" name="id" >
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Partner Contact</label>
            <input type="textbox" class="form-control" id="PartnerContactEdit" name="PartnerContactEdit">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" >Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
          </form>
      </div>
    </div>
  </div>
</div>
@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function(){

});
</script>
