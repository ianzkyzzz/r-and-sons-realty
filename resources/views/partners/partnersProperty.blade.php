@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('PartnersPropertyPercentage') }}</div>

                <div class="card-body">
                  @if (Route::has('login'))
                    <div class="container-fluid">

@if(session()->has('message'))
<div class="alert alert-success">{{session()->get('message')}} </div>
@endif
<x-alert />
@if($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
                      <table class="table" id="propertyTable">
  <thead class="thead-light">
    <tr align="center">
      <th scope="col">#</th>
      <th scope="col">Partners Name</th>
      <th scope="col">Address</th>
      <th scope="col">Contact</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>

    @foreach($data as $item)

    <tr>

      <th scope="row" align="center">{{$count++}}</th>
      <td align="center">{{$item->PartnerName}}</td>
      <td align="center">{{$item->PartnerAddress}}</td>
      <td align="center">{{$item->PartnerContact}}</td>
      <td align="center">
        <a class="btn btn-sm btn-outline-info" href="{{'/propertyList/'.$item->partnerId.'/propertylists'}}" role="button">View Invested Property</a>
      <a class="btn btn-sm btn-outline-success" href="#" data-name="{{$item->PartnerName}}" data-contact="{{$item->PartnerContact}}" data-address="{{$item->PartnerAddress}}" data-Parentpropid="{{$item->partnerId}}" data-toggle="modal" data-target="#editProperty" role="button">Edit</a></td>


    </tr>

@endforeach
  </tbody>
</table>
<nav aria-label="Page navigation example">
{{$data->links()}}
</nav>

<!-- Modal -->
<div class="modal fade" id="addProperty" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Partner</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="/partner/create">
           @csrf
           <div class="container">

             <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Partner's Name</label>
                  <input type="text" class="form-control" id="PartnerName" name="PartnerName" aria-describedby="emailHelp">


                </div>

  <div class="form-group col-md-6">
    <label for="exampleInputEmail1">Partner Contact</label>
    <input type="text" class="form-control" id="PartnerContact" name="PartnerContact" aria-describedby="emailHelp">

  </div>



      </div>
      <div class="form-row">
<div class="form-group col-md-12">
<label for="exampleInputEmail1">Partner Address</label>
<input type="text" class="form-control" id="PartnerAddress" name="PartnerAddress" aria-describedby="emailHelp">
    <small id="emailHelp" class="form-text text-muted">New Partner will be added.</small>
</div>



</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>
                    </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editProperty" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Property</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="/partner/edit">
            @csrf
        <div class="form-group">
          <label for="exampleInputPassword1">Partner Name</label>
            <input type="textbox" class="form-control" id="PartnerNameEdit" name="PartnerNameEdit">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Address</label>
        <input type="textarea" class="form-control" id="PartnerAddressEdit" name="PartnerAddressEdit" >
        <input type="hidden" id="id" name="id" >
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Partner Contact</label>
            <input type="textbox" class="form-control" id="PartnerContactEdit" name="PartnerContactEdit">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" >Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
          </form>
      </div>
    </div>
  </div>
</div>
@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function(){
  $('#changeBlock').on('change', function () {
                 let block = $(this).val();
                var prop=  $('#propId').val()

window.location.href ="/getpropListwithblock/"+prop+"/"+block;


  });

  $('#editProperty').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget)
      var name = button.data('name')
      var id = button.data('parentpropid')
      var address = button.data('address')
      var contact = button.data('contact')



      // // alert(title)
      $('#PartnerNameEdit').val(name);
      $('#id').val(id);
      $('#PartnerAddressEdit').val(address);
      $('#PartnerContactEdit').val(contact);

      // var description = button.data('mydescription')
      // var cat_id = button.data('catid')
      // var modal = $(this)
      // modal.find('.modal-body #title').val(title);
      // modal.find('.modal-body #des').val(description);
      // modal.find('.modal-body #cat_id').val(cat_id);
  });

  // $('.unlist').on('click', function () {
  //   var button = $(event.relatedTarget)
  //   var propid = button.data('propid2')
  //   alert(propid);
  // });

  function myFunction() {
    var txt;
    var r = confirm("Press a button!");
    if (r == true) {
      txt = "You pressed OK!";
    } else {
      txt = "You pressed Cancel!";
    }
    document.getElementById("demo").innerHTML = txt;
  }
});
</script>
