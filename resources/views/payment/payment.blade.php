@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h2>{{ __('Payment') }}</h2></div>
                @if(session()->has('message'))
                <div class="alert alert-success">{{session()->get('message')}} </div>
                @endif
                <x-alert />
                @if($errors->any())
                <div class="alert alert-danger">
                  <ul>
                    @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
                @endif

                <div class="card-body">
                  @if (Route::has('login'))

                    <div class="container-fluid">
                      <div class="container-fluid">
                        <form id="addpayment">
                           @csrf

                      <div class="container">

                       <div class="form-row">
                         <div class="card-body" id="map" style="height: 450px;display: none">
                         <img src="{{asset('/img/full.png')}}" alt="{{asset('/storage/images/avatar.png')}}" style="vertical-align:middle"/>
                         </div>
                          <div class="form-group col-md-4">

                            <label for="Agent_name">Client Name</label>
                            <select name="Client" class="custom-select"  id="Client" >
                               <option  value="" >Select Client</option>

                            </select>

                          </div>
                          <div class="form-group col-md-4 mx-auto">
                            <label for="Unit">Property</label>
                            <select class="form-control" name="propList" id="propList">


                            </select>
                          </div>
                          <div class="form-group col-md-2 mx-auto">
                            <label for="Unit">Monthly Amortization</label>
                     <input type="text" class="form-control" id="monthlyAmortization" name="monthlyAmortization" readonly>
                          </div>
                          <div class="form-group col-md-2 mx-auto">
                            <label for="Unit">PlanTerms</label>
                          <input type="text" class="form-control" id="terms" name="terms" readonly >
                            <small id="asTpypeError" class="form-text text-muted"></small>
                          </div>
                        </div>

                        <div class="form-row">

                           <div class="form-group col-md-3">

                             <label for="product_name">Total Contract Price</label>
                             <input type="text" class="form-control" id="contractPrice" name="contractPrice" readonly>
                             <small id="asNameError" class="form-text text-muted"></small>

                           </div>   <div class="form-group col-md-2">
                                <label for="product_name">Mode of Payment</label>
                                <select name="PaymentMethod" class="form-control"  id="PaymentMethod">

                                    <option value="Cash">Cash</option>
                                    <option value="Bank">Bank</option>
                                </select>
                                <small id="asNameError" class="form-text text-muted"></small>
                              </div>
                              <div class="form-group col-md-2 mx-auto">
                               <label for="product_name">Bank</label>
                               <select name="bank" class="form-control"  id="bank">
                                  <option value="">Select Bank</option>
                                   <option value="BDO">BDO</option>
                                   <option value="METROBANK">METROBANK</option>
                               </select>

                                <small id="asNameError" class="form-text text-muted"></small>

                              </div>
                           <div class="form-group col-md-2 mx-auto">
                            <label for="product_name">Due Date</label>
                               <input type="text" class="form-control" id="due" name="due" readonly>
                             <small id="asNameError" class="form-text text-muted"></small>

                           </div>
                           <div class="form-group col-md-3 mx-auto">
                             <label for="product_name">Penalty</label>
                       

                         
                         <input type="text" class="form-control" id="Penalty" name="Penalty" placeholder="Penalty" value="0">  

                         </div>

                                 </div>
                         </div>
                          <div class="form-row">
                            <input type="text" class="form-control"  id="PenaltyDescription" name="PenaltyDescription" placeholder="DESCRIPTION FOR PENALTY">
                          </div>
                         <div class="form-row">
                           <div class="form-group col-md-2 mx-auto">
                             <label for="product_name">Total Paid</label>
                               <input type="text" class="form-control" id="Paid" name="Paid" value="0.00">
                             <small id="asNameError" class="form-text text-muted"></small>

                                 </div>
                                 <div class="form-group col-md-2 mx-auto">
                                   <label for="product_name">OR NUMBER</label>
                                     <input type="text" class="form-control" id="OR" name="OR" value="0000000">

                                   <small id="asNameError" class="form-text text-muted"></small>

                                       </div>
                            <div class="form-group col-md-2 mx-auto">
                              <label for="product_name">Amount</label>
                                <input type="text" class="form-control" id="Monthly" name="Monthly">
                                   <input type="hidden" id="dcom" name="dcom">
                                   <input type="hidden" id="umcom" name="umcom">
                                   <input type="hidden" id="mcom" name="mcom">
                                  <input type="hidden" id="comRelease" name="comRelease" value="">
                                  <input type="hidden" id="numberRelease" name="numberRelease" value="">
                                  <input type="hidden" id="counter" name="counter">
                                  <input type="hidden" id="gcom1" name="gcom1">
                                  <input type="hidden" id="gcom2" name="gcom2">
                                  <input type="hidden" id="gcom3" name="gcom3">
                                  <input type="hidden" id="gcom4" name="gcom4">
                                  <input type="hidden" id="gcom5" name="gcom5">
                                  <input type="hidden" id="grant1" name="grant1">
                                  <input type="hidden" id="grant2" name="grant2">
                                  <input type="hidden" id="grant3" name="grant3">
                                  <input type="hidden" id="grant4" name="grant4">
                                  <input type="hidden" id="grant5" name="grant5">
                                  <input type="hidden" id="directAgent" name="directAgent">
                                  <input type="hidden" id="unitManager" name="unitManager">
                                  <input type="hidden" id="manager" name="manager">
                              <small id="asNameError" class="form-text text-muted"></small>

                                  </div>
                                  <div class="form-group col-md-2 mx-auto">
                                    <label for="product_name">Total</label>
                              <div class="totaltopay"><p class="form-control" id="totaltopay">0.00</p></div>
                                    <small id="asNameError" class="form-text text-muted"></small>

                                        </div>




                            <div class="form-group col-md-4 mx-auto">
                              <label for="product_name">Details</label>
                                <textarea class="form-control" id="details" name="details"> </textarea>
                                  <input type="hidden" id="cid" name="cid" value="">
                              <small id="asNameError" class="form-text text-muted"></small>

                            </div>

                            </div>
                            <div class="form-row">
                            <div class="form-group col-md-2 mx-auto">
                            <label for="product_name">Custom Date</label>
                            <input type="date" class="form-control"  id="date" name="date">
                          </div>
                          </div>
                                  <input type="hidden"  id="cusCommission" name="cusCommission">

                                 
                                  <div class="successni"><h2><p class="text-success"id="successni"></p></h2></div>
                                    <small id="asNameError" class="form-text text-muted"></small>

                                        </div>
                        <button type="submit"  class="btn btn-primary">Save Payment</button>
                      </form>
                      </div>
                      </div>
                   

                    </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function(){

                      $('#Client').select2({
                        ajax:{
                          url:"{{route('client.getClientList')}}",
                          type:"post",
                          dataType:"json",
                          data: function(params){
                            return{
                           _token:CSRF_TOKEN,
                            search: params.term,
                              col: 'vdn'
                          };
                        },
                        processResults: function(response){
                          return{
                            results: response
                          };
                        },
                        cache :true
                      }
                    });
$('#addpayment').submit(function(e){
  e.preventDefault();
  var Client = $('#Client').val();
  var propList = $('#propList').val();
  var monthlyAmortization = $('#monthlyAmortization').val();
  var contractPrice = $('#contractPrice').val();
  var PaymentMethod = $('#PaymentMethod').val();
  var bank = $('#bank').val();
  var due = $('#due').val();
  var Penalty = $('#Penalty').val();
  var PenaltyDescription = $('#PenaltyDescription').val();
  var Paid = $('#Paid').val();
  var OR = $('#OR').val();
  var Monthly = $('#Monthly').val();
  var dcom = $('#dcom').val();
  var umcom = $('#umcom').val();
  var mcom = $('#mcom').val();
  var comRelease = $('#comRelease').val();
  var numberRelease = $('#numberRelease').val();
  var counter = $('#counter').val();
  var gcom1 = $('#gcom1').val();
  var gcom2 = $('#gcom2').val();
  var gcom3 = $('#gcom3').val();
  var gcom4 = $('#gcom4').val();
  var gcom5 = $('#gcom5').val();
  var grant1 = $('#grant1').val();
  var grant2 = $('#grant2').val();
  var grant3 = $('#grant3').val();
  var grant4 = $('#grant4').val();
  var grant5 = $('#grant5').val();
  var directAgent = $('#directAgent').val();
  var unitManager = $('#unitManager').val();
  var manager = $('#manager').val();
  var details = $('#details').val();
  var date = $('#date').val();
  var _token = $("input[name=_token]").val();

    $.ajax({
        url:"{{route('payment.Save')}}",
        type:"post",
        data:{
Client:Client,
propList:propList,
monthlyAmortization:monthlyAmortization,
contractPrice:contractPrice,
PaymentMethod:PaymentMethod,
bank:bank,
due:due,
Penalty:Penalty,
PenaltyDescription:PenaltyDescription,
Paid:Paid,
OR:OR,
Monthly:Monthly,
dcom:dcom,
umcom:umcom,
mcom:mcom,
comRelease:comRelease,
numberRelease:numberRelease,
counter:counter,
gcom1:gcom1,
gcom2:gcom2,
gcom3:gcom3,
gcom4:gcom4,
gcom5:gcom5,
grant1:grant1,
grant2:grant2,
grant3:grant3,
grant4:grant4,
grant5:grant5,
directAgent:directAgent,
unitManager:unitManager,
manager:manager,
details:details,
date:date,
_token:_token
        },
        success:function(response){
console.log(response);
  $('#Client').val("");
  $('#propList').val("");
  $('#monthlyAmortization').val("");
  $('#contractPrice').val("");

  $('#bank').val("");
  $('#due').val("");

  $('#PenaltyDescription').val("");
  $('#Paid').val("");
  $('#OR').val("");
  $('#Monthly').val("");
  $('#dcom').val("");
  $('#umcom').val("");
  $('#mcom').val("");
  $('#comRelease').val("");
  $('#numberRelease').val("");
  $('#counter').val("");
  $('#gcom1').val("");
  $('#gcom2').val("");
  $('#gcom3').val("");
  $('#gcom4').val("");
  $('#gcom5').val("");
  $('#grant1').val("");
  $('#grant2').val("");
  $('#grant3').val("");
  $('#grant4').val("");
  $('#grant5').val("");
  $('#directAgent').val("");
  $('#unitManager').val("");
  $('#manager').val("");
  $('#details').val("");
  $('#date').val("");
  $('#terms').val("");
  $('#successni').html('<b>'+ 'ADDED SUCCESFULLY' +'</b>');
  $('#totaltopay').html('<b>'+ '' +'</b>');
  
  

}
    });

})
$('#propList').on('change', function () {
   let cp_id = $(this).val();
   $.ajax({
   type: 'GET',
   dataType: 'json',
   url: '/clientPropertyFetch/' + cp_id ,
   success: function (data) {
     console.log(data[0]);
         $('#monthlyAmortization').val(data[0].monthlyAmortization);
         $('#contractPrice').val(data[0].contractPrice);
         $('#terms').val(data[0].PlanTerms);
         $('#due').val(data[0].dueDate);
         $('#dcom').val(data[0].directCommission);
         $('#umcom').val(data[0].unitManagerCommission);
         $('#mcom').val(data[0].managerCommission);
         $('#Monthly').val(data[0].monthlyAmortization);
         $('#Paid').val(data[0].totalPaid);
         $('#comRelease').val(data[0].comRelease);
         $('#directAgent').val(data[0].DC);
         $('#unitManager').val(data[0].UMC);
         $('#manager').val(data[0].MC);
         $('#gcom1').val(data[0].GC1);
         $('#gcom2').val(data[0].GC2);
         $('#gcom3').val(data[0].GC3);
         $('#gcom4').val(data[0].GC4);
         $('#gcom5').val(data[0].GC5);
         $('#grant1').val(data[0].grant1);
         var total = data[0].monthlyAmortization;
         $('#grant2').val(data[0].grant2);
         $('#grant3').val(data[0].grant3);
         $('#grant4').val(data[0].grant4);
         $('#grant5').val(data[0].grant5);
         $('#numberRelease').val(data[0].comnum);
         $('#counter').val(data[0].counter);

         $('#totaltopay').html('<b>'+ (total) +'</b>');
         var x = (data[0].contractPrice)-(data[0].totalPaid);
         $('#Balance').val((data[0].contractPrice)-(data[0].totalPaid));
         $('#successni').html('<b>'+ '' +'</b>');

if (x<=0) {
  $('#Monthly').prop('readonly', true);
  $('#OR').prop('readonly', true);
  $('#details').prop('readonly', true);
  $('#Monthly').val('FULLY PAID');
  $('#OR').val('FULLY PAID');
  $('#details').val('FULLY PAID');
  $("#map").show();
}
else {
  {
    $("#map").hide();
  }
}


   //alert(data[0].contractPrice);


   }
});

});
                    $('#Penalty').on('change', function () {
                      if( $('#Penalty').val()==0)
                      {
                        var pen = parseFloat(($('#monthlyAmortization').val()*0.03) );
                      var mon = parseFloat($('#monthlyAmortization').val());
                  
                      $('#Penalty').val(($('#monthlyAmortization').val()*0.03).toFixed(2));
                      $("#totaltopay").html("<b>"+ (pen+mon).toFixed(2) +"</b>");
                      }
                      else
                      {
                        var pen = parseFloat(($('#Penalty').val()) );
                      var mon = parseFloat($('#monthlyAmortization').val());
                  
                      
                      $("#totaltopay").html("<b>"+ (pen+mon).toFixed(2) +"</b>");
                      }
        
                    });
//

$('#Monthly').on('change', function () {

   let mon = parseFloat($(this).val());
  var pen = parseFloat($('#Penalty').val());
  $("#totaltopay").html("<b>"+ (pen+mon).toFixed(2) +"</b>");

});
                    $('#Client').on('change', function () {
                                   let client_id = $(this).val();

                                   $('#propList').empty();
                                   $('#propList').append(`<option value="0" disabled selected>Processing...</option>`);
                                   $.ajax({
                                   type: 'GET',
                                   url: '/clientPropertiesList/' + client_id ,
                                   success: function (response) {
                                   var response = JSON.parse(response);
                                   // console.log(response);
                                   $('#propList').empty();
                                   $('#propList').append(`<option value="0" disabled selected>Select Lot*</option>`);
                                   response.forEach(element =>  {

                                     // console.log(element.getpropertylist_relation.propertyName);
                                       $('#propList').append(`<option value="${element['cp_id']}"> ${element['propertyName']} Block ${element['block']} Lot ${element['lot']} </option>`);
                                       });
                                   }
                               });
                           });
});
</script>
@endsection
