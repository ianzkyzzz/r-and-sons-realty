@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h2>{{ __('Payment History') }}</h2></div>

                <div class="card-body">
                  @if (Route::has('login'))
                    <div class="container-fluid">
@if(session()->has('message'))
<div class="alert alert-success">{{session()->get('message')}} </div>
@endif
<x-alert />
@if($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif

                      <table class="table" id="clientList">
  <thead class="thead-light">
    <tr align="center">
      <th scope="col">#</th>
      <th scope="col">Client Name</th>
      <th scope="col">Property</th>
      <th scope="col">Payment</th>
      <th scope="col">OR Number</th>
      <th scope="col">Method</th>
      <th scope="col">Date</th>


      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>

    @foreach($data as $item)

    <tr>

      <th scope="row" align="center">{{$count++}}</th>
      <td align="center">{{$item->firstName . " " . $item->lastName}}</td>
      <td align="center">{{$item->propertyName . " Block " . $item->block . " Lot " . $item->lot }}</td>
      <td align="center">{{$item->payment}}</td>
      <td align="center">{{$item->or_num}}</td>
      <td align="center">{{$item->paymentMethod}}</td>
      <td align="center">{{date("F j, Y", strtotime($item->created_at))}}</td>

      <td align="center">
      <a class="btn btn-sm btn-outline-success" href="#" data-ornum="{{$item->or_num}}"  data-desc="{{$item->paymentDesc}}" data-datecreate="{{$item->created_at}}" data-bank="{{$item->bank}}" data-penalty="{{$item->penalty}}" data-penaltydesc="{{$item->PenaltyDescription}}"  data-lot="{{$item->payment}}"  data-id="{{$item->id}}" data-method="{{$item->paymentMethod}}"  data-payment="{{$item->payment}}" data-toggle="modal" data-target="#editPayment" role="button">Edit</a>
        <a class="btn btn-sm btn-outline-info unlist" data-id="{{$item->id}}" data-amount="{{$item->payment}}" href="#" role="button">delete</a></td>


    </tr>

@endforeach
  </tbody>
</table>
<nav aria-label="Page navigation example">

</nav>


<!-- bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb -->
<div class="modal fade" id="editPayment" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" >
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="exampleModalLabel">Edit Payment</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      
        <form form id="editPayments" >
            @csrf
        <div class="form-row">
        <div class="form-group col-md-4">

          <label for="exampleInputEmail1">OR NUMBER</label>
          <input type="text" class="form-control" id="ornum" name="ornum"  aria-describedby="emailHelp">
          <input type="hidden" name="payid" id="payid" />


        </div>
        <div class="form-group col-md-4">
          <label for="exampleInputPassword1">PAYMENT</label>
            <input type="textarea" class="form-control" id="payment" name="payment">
        </div>
        <div class="form-group col-md-4">
          
          <label for="exampleInputPassword1">PAYMENT DESCRIPTION</label>
          <textarea class="form-control" id="paymentDesc" name="paymentDesc"> </textarea>
        </div>
        </div>
        <div class="form-row">
        <div class="form-group col-md-4">
          <label for="exampleInputEmail1">Method</label>
          <select class="form-control" id="Method" name="Method">
                <option value="">Select Method</option>
            <option value="Cash">Cash</option>
          <option value="Bank">Bank</option>


          </select>
        </div>
        <div class="form-group col-md-4">
          <label for="exampleInputEmail1">Bank</label>
          <select class="form-control" id="Bank" name="Bank">
                <option value="">Select Bank</option>
            <option value="BDO">BDO</option>
          <option value="METROBANK">METROBANK</option>


          </select>
        </div>
        <div class="form-group col-md-4">
          <label for="exampleInputPassword1">Penalty</label>
            <input type="text" class="form-control" id="penalty" name="penalty">
        </div>
        </div>
        <div class="form-row">
        <div class="form-group col-md-8">
        <label for="exampleInputPassword1">PENALTY DESCRIPTION</label>
        <textarea class="form-control" id="penaltyDesc" name="penaltyDesc">  </textarea>
        </div>
        <div class="form-group col-md-4">
          <label for="exampleInputPassword1">Date</label>
            <input type="date" class="form-control" id="createdDate" name="createdDate">
        </div>
        </div>
      <div class="modal-footer">
        
        <button type="submit" class="btn btn-primary">Save changes</button>
          </form>
      </div>
    </div>
  </div>
  @endif

</div>
<!-- bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function(){
  $('#changeBlock').on('change', function () {
                 let block = $(this).val();
                var prop=  $('#propId').val()

window.location.href ="/getpropListwithblock/"+prop+"/"+block;


  });

  $('#editPayment').on('show.bs.modal', function (event) {

      var button = $(event.relatedTarget)
      var payment = button.data('payment')
      var ornum = button.data('ornum')
      var payid = button.data('id')
      var pmethod = button.data('method')
      var penalty = button.data('penalty')
      var desc = button.data('desc')
      var bank = button.data('bank')
      var penaltydesc = button.data('penaltydesc')
      var datecreate = button.data('datecreate')
      


      // // alert(title)
      $('#payid').val(payid);
      $('#payment').val(payment);
      $('#ornum').val(ornum);
      $('#Method').val(pmethod);
      $('#parent').val(parent);
      $('#penalty').val(penalty);
      $('#paymentDesc').val(desc);
      $('#Bank').val(bank);
      $('#penaltyDesc').val(penaltydesc);
      $('#createdDate').val(datecreate);
      

      // var description = button.data('mydescription')
      // var cat_id = button.data('catid')
      // var modal = $(this)
      // modal.find('.modal-body #title').val(title);
      // modal.find('.modal-body #des').val(description);
      // modal.find('.modal-body #cat_id').val(cat_id);
  });

  // $('.unlist').on('click', function () {
  //   var button = $(event.relatedTarget)
  //   var propid = button.data('propid2')
  //   alert(propid);
  // });
  $('#editPayments').submit(function(e){
  e.preventDefault();
     var payid = $('#payid').val();
      var payment = $('#payment').val();
      var ornum = $('#ornum').val();
      var Method = $('#Method').val();
      var parent = $('#parent').val();
      var penalty =  $('#penalty').val();
      var paymentDesc = $('#paymentDesc').val();
      var Bank = $('#Bank').val();
      var penaltyDesc = $('#penaltyDesc').val();
      var createdDate = $('#createdDate').val();
      
  var _token = $("input[name=_token]").val();

    $.ajax({
        url:"{{route('payment.Edit')}}",
        type:"post",
        data:{
          payid:payid,
          payment:payment,
          ornum:ornum,
          Method:Method,
          parent:parent,
          penalty:penalty,
          paymentDesc:paymentDesc,
          Bank:Bank,
          penaltyDesc:penaltyDesc,
          createdDate:createdDate,
_token:_token
        },
        success:function(response){
console.log(response);
 //swal area
 Swal.fire({
  position: 'top-end',
  type: 'success',
  title: 'Edited Succesfully..',
  showConfirmButton: true,

})
$("#editPayment .close").click();
location.reload();
  

},
    error: function(data) {
      Swal.fire({
  type: 'error',
  title: 'ERROR!',
  text: 'You do not have permission to edit!'
 
})
$("#editPayment .close").click();
    }


    });

})
  $("body").delegate('.unlist','click',function(){
           var id = $(this).attr("data-id");
           var amount = $(this).attr("data-amount");
           var r = confirm("Are you sure you want to delete payment?");
           if (r == true) {
            window.location.href ="/paymentDelete/"+id+"/"+amount;
            Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Payment Deleted',
            showConfirmButton: false,
            timer: 1500
})
           } else {
             txt = "You pressed Cancel!";
           }

       })
  function myFunction() {
    var txt;
    var r = confirm("Press a button!");
    if (r == true) {
      txt = "You pressed OK!";
    } else {
      txt = "You pressed Cancel!";
    }
    document.getElementById("demo").innerHTML = txt;
  }
});
</script>

@endsection
