@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h2>{{ __('Other Payment') }}</h2></div>
                @if(session()->has('message'))
                <div class="alert alert-success">{{session()->get('message')}} </div>
                @endif
                <x-alert />
                @if($errors->any())
                <div class="alert alert-danger">
                  <ul>
                    @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
                @endif
                <div class="card-body">
                  @if (Route::has('login'))
                    <div class="container-fluid">
                      <div class="container-fluid">
                        <form method="post" action="/otherpayment/store">
                           @csrf

                      <div class="container">

                       <div class="form-row">
                          <div class="form-group col-md-6">

                            <label for="Agent_name">Client Name</label>
                            <select name="Client" class="form-control"  id="Client" >
                               <option  value="" >Select Client</option>

                            </select>

                          </div>
                          <div class="form-group col-md-6 mx-auto">
                            <label for="Unit">Property</label>
                            <select class="form-control" name="propList" id="propList">
                              <option value="">Select Property</option>

                            </select>
                          </div>
                        </div>

                        <div class="form-row">

                           <div class="form-group col-md-4">
                             <label for="product_name">Total Contract Price</label>
                             <input type="text" class="form-control" id="contractPrice" name="contractPrice" readonly>
                             <small id="asNameError" class="form-text text-muted"></small>

                           </div>   <div class="form-group col-md-4">
                                <label for="product_name">Payment For</label>
                                <select name="paymentfor" class="form-control"  id="paymentfor">
                                   <option value="">Select Payment</option>
                                    <option value="Survey">Survey</option>
                                    <option value="Title">Title</option>
                                    <option value="Notarial">Notarial</option>
                                    <option value="Deed of Sale">Deed of Sale</option>
                                </select>
                                <small id="asNameError" class="form-text text-muted"></small>
                              </div>

                           <div class="form-group col-md-4 mx-auto">
                            <label for="product_name">Due Date</label>
                               <input type="text" class="form-control" id="due" name="due" readonly>
                             <small id="asNameError" class="form-text text-muted"></small>

                           </div>



                         <input type="hidden" class="form-control" id="Penalty" name="Penalty" placeholder="Penalty" value="0" readonly>

                         <div class="form-row">
                           <div class="form-group col-md-2 mx-auto">
                             <label for="product_name">Total Paid</label>
                               <input type="text" class="form-control" id="Paid" name="Paid" readonly>
                             <small id="asNameError" class="form-text text-muted"></small>

                                 </div>
                                 <div class="form-group col-md-2 mx-auto">
                                   <label for="product_name">Balance</label>
                                     <input type="text" class="form-control" id="Balance" name="Balance" readonly>
                                   <small id="asNameError" class="form-text text-muted"></small>

                                       </div>
                            <div class="form-group col-md-2 mx-auto">
                              <label for="product_name">Paid Amount</label>
                                <input type="text" class="form-control" id="Monthly" name="Monthly">
                                  <input type="hidden" id="cid" name="cid" value="">
                              <small id="asNameError" class="form-text text-muted"></small>

                                  </div>
                                  <div class="form-group col-md-2 mx-auto">
                                    <label for="product_name">OR NUMBER</label>
                                      <input type="text" class="form-control" id="OR" name="OR">
                                        <input type="hidden" id="cid" name="cid" value="">
                                    <small id="asNameError" class="form-text text-muted"></small>

                                        </div>



                            <div class="form-group col-md-4 mx-auto">
                              <label for="product_name">Details</label>
                                <input type="text" class="form-control" id="details" name="details">
                                  <input type="hidden" id="cid" name="cid" value="">
                              <small id="asNameError" class="form-text text-muted"></small>

                            </div>
                            </div>

                        <button type="submit"  class="btn btn-primary">Save changes</button>
                      </form>
                      </div>
                      </div>


                    </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function(){

                      $('#Client').select2({
                        ajax:{
                          url:"{{route('client.getClientList')}}",
                          type:"post",
                          dataType:"json",
                          data: function(params){
                            return{
                           _token:CSRF_TOKEN,
                            search: params.term,
                              col: 'vdn'
                          };
                        },
                        processResults: function(response){
                          return{
                            results: response
                          };
                        },
                        cache :true
                      }
                    });
                    $('#paymentfor').on('change', function () {
                      if($('#paymentfor').val()=='Deed of Sale')
                      {
                      $('#Monthly').val(($('#contractPrice').val()*0.02).toFixed(2));

                      }



                    });
$('#propList').on('change', function () {
   let cp_id = $(this).val();
   $.ajax({
   type: 'GET',
   dataType: 'json',
   url: '/clientPropertyFetch/' + cp_id ,
   success: function (data) {
   // var data = JSON.parse(data);
   console.log(data[0]);
    $('#monthlyAmortization').val(data[0].get_clientproperty_relation.monthlyAmortization);
    $('#contractPrice').val(data[0].contractPrice);
    $('#terms').val(data[0].get_clientproperty_relation.PlanTerms);
    $('#due').val(data[0].get_clientproperty_relation.dueDate);
    $('#Paid').val(data[0].get_clientproperty_relation.totalPaid);
    $('#Balance').val((data[0].contractPrice)-(data[0].get_clientproperty_relation.totalPaid));



   //alert(data[0].contractPrice);


   }
});

});

                    $('#Client').on('change', function () {
                                   let client_id = $(this).val();

                                   $('#propList').empty();
                                   $('#propList').append(`<option value="0" disabled selected>Processing...</option>`);
                                   $.ajax({
                                   type: 'GET',
                                   url: '/clientPropertiesList/' + client_id ,
                                   success: function (response) {
                                   var response = JSON.parse(response);
                                   // console.log(response);
                                   $('#propList').empty();
                                   $('#propList').append(`<option value="0" disabled selected>Select Lot*</option>`);
                                   response.forEach(element => {

                                     // console.log(element.getpropertylist_relation.propertyName);
                                       $('#propList').append(`<option value="${element.get_clientproperty_relation.cp_id}">' ${element.getpropertylist_relation.propertyName} Block ${element['block']} Lot ${element['lot']} </option>`);
                                       });
                                   }
                               });
                           });
});
</script>
@endsection
