@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('PropertyList') }}</div>

                <div class="card-body">
                  @if (Route::has('login'))
                    <div class="container-fluid">
<button type="button" class="btn btn-sm btn-outline-primary form-group" data-toggle="modal" data-target="#addProperty">Add Property</button>
@if(session()->has('message'))
<div class="alert alert-success">{{session()->get('message')}} </div>
@endif
<x-alert />
@if($errors->any())
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{ $error }}</li>
    @endforeach
  </ul>
</div>
@endif
                      <table class="table" id="propertyTable">
  <thead class="thead-light">
    <tr align="center">
      <th scope="col">#</th>
      <th scope="col">Property Name</th>
      <th scope="col">Address</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>

    @foreach($data as $item)

    <tr>

      <th scope="row" align="center">{{$count++}}</th>
      <td align="center">{{$item->propertyName}}</td>
      <td align="center">{{$item->address}}</td>
      <td align="center">
        <a class="btn btn-sm btn-outline-info" href="{{'/propertyList/'.$item->propId.'/propertylists'}}" role="button">View PropertyList</a>
      <a class="btn btn-sm btn-outline-success" href="#" data-name="{{$item->propertyName}}" data-address="{{$item->address}}" data-Parentpropid="{{$item->propId}}" data-toggle="modal" data-target="#editProperty" role="button">Edit</a></td>


    </tr>

@endforeach
  </tbody>
</table>
<nav aria-label="Page navigation example">
{{$data->links()}}
</nav>

<!-- Modal -->
<div class="modal fade" id="addProperty"  aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h3>Property Details</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post"  action="/property/create" id="form">
           @csrf
           <div class="container">

             <div class="form-row">
                <div class="form-group col-md-6">
                  <label for="exampleInputEmail1">Property Name</label>
                  <input type="text" class="form-control" id="propertyName" name="propertyName" aria-describedby="emailHelp">


                </div>

  <div class="form-group col-md-6">
    <label for="exampleInputEmail1">Property Address</label>
    <input type="text" class="form-control" id="address" name="address" aria-describedby="emailHelp">

  </div>



</div>
    <h3>Partners</h3>
      <div class="form-row">
         <div class="form-group col-md-3">
           <label for="exampleInputEmail1">Partner 1</label>

           <select style="width:240px, height:50px" id="partner1" name="partner1">
              <option value="">Select Partner</option>

           </select>
   <small id="perError" class="form-text text-muted"></small>
   <small id="msgSuccess" class="form-text text-muted"></small>

         </div>
         <div class="form-group col-md-1">
           <label for="exampleInputEmail1">Percentage</label>
           <input type="text" class="form-control" id="pper1" name="pper1" value="0" aria-describedby="emailHelp">


         </div>
         <div class="form-group col-md-3">
           <label for="exampleInputEmail1">Partner 2</label>
           <select style="width:240px;" id="partner2" name="partner2">
              <option value="">Select Partner</option>

           </select>


         </div>
         <div class="form-group col-md-1">
           <label for="exampleInputEmail1">Percentage</label>
           <input type="text" class="form-control" id="pper2" name="pper2" value="0" aria-describedby="emailHelp">


         </div>
         <div class="form-group col-md-3">
           <label for="exampleInputEmail1">Partner 3</label>
           <select style="width:240px;" id="partner3" name="partner3">
              <option value="">Select Partner</option>

           </select>


         </div>
         <div class="form-group col-md-1">
           <label for="exampleInputEmail1">Percentage</label>
           <input type="text" class="form-control" id="pper3" name="pper3" value="0" aria-describedby="emailHelp">


         </div>
          </div>
          <div class="form-row">
             <div class="form-group col-md-3 mx-auto">
               <label for="exampleInputEmail1">Partner 4</label>
               <select style="width:240px;" id="partner4" name="partner4">
                  <option value="">Select Partner</option>

               </select>
   </div>
   <div class="form-group col-md-1">
     <label for="exampleInputEmail1">Percentage</label>
     <input type="text" class="form-control" id="pper4" name="pper4" value="0" aria-describedby="emailHelp">
   </div>
   <div class="form-group col-md-3 mx-auto">
     <label for="exampleInputEmail1">Partner 5</label>
     <select style="width:240px;" id="partner5" name="partner5">
        <option value="">Select Partner</option>

     </select>
</div>
<div class="form-group col-md-1">
<label for="exampleInputEmail1">Percentage</label>
<input type="text" class="form-control" id="pper5" name="pper5" value="0" aria-describedby="emailHelp">
</div>

    </div>
        <h3>Grant Commission</h3>
        <div class="form-row">
           <div class="form-group col-md-3">
             <label for="exampleInputEmail1">Agent 1</label>
             <select style="width:240px;" id="agent1" name="agent1">
                <option value="">Select Sales Director</option>

             </select>



             <small id="agError" class="form-text text-muted"></small>
             <small id="agmsgSuccess" class="form-text text-muted"></small>

           </div>
           <div class="form-group col-md-1">
             <label for="exampleInputEmail1">Percentage</label>
             <input type="text" class="form-control" id="aper1" name="aper1" aria-describedby="emailHelp">


           </div>
           <div class="form-group col-md-3">
             <label for="exampleInputEmail1">Agent 2</label>
             <select style="width:240px;" id="agent2" name="agent2">
                <option value="">Select Sales Director</option>

             </select>




           </div>
           <div class="form-group col-md-1">
             <label for="exampleInputEmail1">Percentage</label>
             <input type="text" class="form-control" id="aper2" name="aper2" aria-describedby="emailHelp">


           </div>
           <div class="form-group col-md-3">
             <label for="exampleInputEmail1">Agent 3</label>
             <select style="width:240px;" id="agent3" name="agent3">
                <option value="">Select Sales Director</option>

             </select>


           </div>
           <div class="form-group col-md-1">
             <label for="exampleInputEmail1">Percentage</label>
             <input type="text" class="form-control" id="aper3" name="aper3" aria-describedby="emailHelp">


           </div>
            </div>
            <div class="form-row">
               <div class="form-group col-md-3 mx-auto">
                 <label for="exampleInputEmail1">Agent 4</label>
                 <select style="width:240px;" id="agent4" name="agent4">
                    <option value="">Select Sales Director</option>

                 </select>



     </div>
     <div class="form-group col-md-1">
       <label for="exampleInputEmail1">Percentage</label>
       <input type="text" class="form-control" id="aper4" name="aper4" aria-describedby="emailHelp">
     </div>
     <div class="form-group col-md-3 mx-auto">
       <label for="exampleInputEmail1">Agent 5</label>
       <select style="width:240px;" id="agent5" name="agent5">
          <option value="">Select Sales Director</option>

       </select>



  </div>
  <div class="form-group col-md-1">
  <label for="exampleInputEmail1">Percentage</label>
  <input type="text" class="form-control" id="aper5" name="aper5" aria-describedby="emailHelp">
  </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        </form>
      </div>
    </div>
  </div>
</div>
                    </div>
                    @endif


                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editProperty" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Property</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="/propertyedit">
            @csrf
        <div class="form-group">
          <label for="exampleInputPassword1">Property Name</label>
            <input type="textbox" class="form-control" id="propNamez" name="propNamez">
        </div>
        <div class="form-group">
          <label for="exampleInputEmail1">Address</label>
        <input type="textarea" class="form-control" id="addressz" name="addressz" >
        <input type="hidden" id="id" name="id" >
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" >Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
          </form>
      </div>
    </div>
  </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script type="text/javascript">
var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
$(document).ready(function(){
  $('#form').validate({ // initialize the plugin
        rules: {
            pper1: {
                required: true
            },
        }
    });
$('#form').on('change', function () {
var total =  (parseFloat($('#pper1').val()) + parseFloat($('#pper2').val()) +  parseFloat($('#pper3').val()) +  parseFloat($('#pper4').val()) +  parseFloat($('#pper5').val()));
var totalp =  (parseFloat($('#aper1').val()) + parseFloat($('#aper2').val()) +  parseFloat($('#aper3').val()) +  parseFloat($('#aper4').val()) +  parseFloat($('#aper5').val()));

if(totalp > 5)
{
  $("#agError").html("<span class='text-danger'>Up to 5% Only!</span>");
   $("#aper1").addClass("border-danger");
   $("#aper2").addClass("border-danger");
   $("#aper3").addClass("border-danger");
   $("#aper4").addClass("border-danger");
   $("#aper5").addClass("border-danger");
}
else{
  $("#aper1").removeClass("border-danger");
  $("#aper2").removeClass("border-danger");
  $("#aper3").removeClass("border-danger");
  $("#aper4").removeClass("border-danger");
  $("#aper5").removeClass("border-danger");
   $("#agError").html("");
}


        if(total > 100)
        {
          // alert("over");
           $("#perError").html("<span class='text-danger'>Up to 100% Only!</span>");
            $("#pper1").addClass("border-danger");
            $("#pper2").addClass("border-danger");
            $("#pper3").addClass("border-danger");
            $("#pper4").addClass("border-danger");
            $("#pper5").addClass("border-danger");
        }
        else
        {
          $("#pper1").removeClass("border-danger");
          $("#pper2").removeClass("border-danger");
          $("#pper3").removeClass("border-danger");
          $("#pper4").removeClass("border-danger");
          $("#pper5").removeClass("border-danger");
           $("#perError").html("");
        }
        if(total==100)
        {
        $("#msgSuccess").html("<span class='text-success'>100% done</span>");
        }
        else if(total<100)
      {
        $("#msgSuccess").html("<span class='text-danger'>Not 100% yet!!</span>");
      }
      else if(total>100)
      {
            $("#msgSuccess").html("");
      }

            });
  $('#changeBlock').on('change', function () {
                 let block = $(this).val();
                var prop=  $('#propId').val()

window.location.href ="/getpropListwithblock/"+prop+"/"+block;


  });
  $('#agent5').select2({
    ajax:{
      url:"/agent/getList",
      type:"post",
      dataType:"json",
      data: function(params){
        return{
        _token:CSRF_TOKEN,
        search: params.term
      };
    },
    processResults: function(response){
      return{
        results: response
      };
    },
    cache :true
  }
});
  $('#agent4').select2({
    ajax:{
      url:"/agent/getList",
      type:"post",
      dataType:"json",
      data: function(params){
        return{
        _token:CSRF_TOKEN,
        search: params.term
      };
    },
    processResults: function(response){
      return{
        results: response
      };
    },
    cache :true
  }
});
  $('#agent3').select2({
    ajax:{
      url:"/agent/getList",
      type:"post",
      dataType:"json",
      data: function(params){
        return{
        _token:CSRF_TOKEN,
        search: params.term
      };
    },
    processResults: function(response){
      return{
        results: response
      };
    },
    cache :true
  }
});
/////
$('#partner1').select2({
  ajax:{
    url:"/partner/getList",
    type:"post",
    dataType:"json",
    data: function(params){
      return{
      _token:CSRF_TOKEN,
      search: params.term
    };
  },
  processResults: function(response){
    return{
      results: response
    };
  },
  cache :true
}
});
$('#partner2').select2({
  ajax:{
    url:"/partner/getList",
    type:"post",
    dataType:"json",
    data: function(params){
      return{
      _token:CSRF_TOKEN,
      search: params.term
    };
  },
  processResults: function(response){
    return{
      results: response
    };
  },
  cache :true
}
});
$('#partner3').select2({
  ajax:{
    url:"/partner/getList",
    type:"post",
    dataType:"json",
    data: function(params){
      return{
      _token:CSRF_TOKEN,
      search: params.term
    };
  },
  processResults: function(response){
    return{
      results: response
    };
  },
  cache :true
}
});
$('#partner4').select2({
  ajax:{
    url:"/partner/getList",
    type:"post",
    dataType:"json",
    data: function(params){
      return{
      _token:CSRF_TOKEN,
      search: params.term
    };
  },
  processResults: function(response){
    return{
      results: response
    };
  },
  cache :true
}
});
$('#partner5').select2({
  ajax:{
    url:"/partner/getList",
    type:"post",
    dataType:"json",
    data: function(params){
      return{
      _token:CSRF_TOKEN,
      search: params.term
    };
  },
  processResults: function(response){
    return{
      results: response
    };
  },
  cache :true
}
});
$('#agent2').select2({
  ajax:{
    url:"/agent/getList",
    type:"post",
    dataType:"json",
    data: function(params){
      return{
      _token:CSRF_TOKEN,
      search: params.term
    };
  },
  processResults: function(response){
    return{
      results: response
    };
  },
  cache :true
}
});
$('#agent1').select2({
  ajax:{
    url:"/agent/getList",
    type:"post",
    dataType:"json",
    data: function(params){
      return{
      _token:CSRF_TOKEN,
      search: params.term
    };
  },
  processResults: function(response){
    return{
      results: response
    };
  },
  cache :true
}
});
  $('#editProperty').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget)
      var name = button.data('name')
      var id = button.data('parentpropid')
      var address = button.data('address')



      // // alert(title)
      $('#propNamez').val(name);
      $('#id').val(id);
      $('#addressz').val(address);

      // var description = button.data('mydescription')
      // var cat_id = button.data('catid')
      // var modal = $(this)
      // modal.find('.modal-body #title').val(title);
      // modal.find('.modal-body #des').val(description);
      // modal.find('.modal-body #cat_id').val(cat_id);
  });

  // $('.unlist').on('click', function () {
  //   var button = $(event.relatedTarget)
  //   var propid = button.data('propid2')
  //   alert(propid);
  // });

  function myFunction() {
    var txt;
    var r = confirm("Press a button!");
    if (r == true) {
      txt = "You pressed OK!";
    } else {
      txt = "You pressed Cancel!";
    }
    document.getElementById("demo").innerHTML = txt;
  }
});
</script>
@endsection
