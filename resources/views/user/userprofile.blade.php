@extends('layouts.app')

@section('content')
<div class="container">
  <h1> User Profile </h1>
  @if(session()->has('message'))
  <div class="alert alert-success">{{session()->get('message')}} </div>
  @endif
  <x-alert />
    <div class="row profile">
		<div class="col-md-3">
			<div class="profile-sidebar">
				<!-- SIDEBAR USERPIC -->
				<div class="profile-userpic">
				  <img src="{{asset('/storage/images/'.Auth::user()->avatar)}}" alt="{{asset('/storage/images/avatar.png')}}"/>
				</div>
				<!-- END SIDEBAR USERPIC -->
				<!-- SIDEBAR USER TITLE -->

				<!-- END SIDEBAR USER TITLE -->
				<!-- SIDEBAR BUTTONS -->
				<div class="profile-userbuttons">

                            <form action="/upload" method="post" enctype="multipart/form-data">
                              @csrf
                              <input type="file" class="form-control" name="image"/>
                              <input type="submit" class="btn btn-outline-primary" name="Upload" />

                            </form>

				</div>
				<!-- END SIDEBAR BUTTONS -->
				<!-- SIDEBAR MENU -->
				<div class="profile-usermenu">
					<ul class="nav">
						<li class="active">
							<a href="#">
							<i class="glyphicon glyphicon-home"></i>
							Overview </a>
						</li>
						<li>
							<a href="#">
							<i class="glyphicon glyphicon-user"></i>
							Account Settings </a>
						</li>
						<li>
							<a href="#" target="_blank">
							<i class="glyphicon glyphicon-ok"></i>
							Tasks </a>
						</li>
						<li>
							<a href="#">
							<i class="glyphicon glyphicon-flag"></i>
							Help </a>
						</li>
					</ul>
				</div>
				<!-- END MENU -->
			</div>
		</div>
		<div class="col-md-9">
            <div class="profile-content">
              <form method="post" action="/user/update">
                 @csrf
              <div class="form-row">
                 <div class="form-group col-md-6">
                   <label for="product_name">Name</label>
                   <input type="text" class="form-control" id="name" name="name" value="{{Auth::user()->name}}">
                   <small id="asNameError" class="form-text text-muted"></small>

                 </div>
                 <div class="form-group col-md-6 mx-auto">
                   <label for="Unit">Branch</label>
                <select class="form-control" name="branch" id="branch">
                  <option value="{{Auth::user()->branch}}"> {{Auth::user()->branch}} </option>
                  <option value="Panabo"> Panabo</option>
                  <option value="Sto.Thomas"> Sto.Thomas</option>
                </select>
                   <small id="asTpypeError" class="form-text text-muted"></small>
                 </div>

               </div>


          </div>
          <div class="modal-footer">

            <button type="submit" class="btn btn-primary">Save changes</button>
            </form>
          </div>
        </div>
      </div>
    </div>
              			<!-- END profile -->
            </div>
		</div>
	</div>
</div>
@endsection
