<?php

use Illuminate\Support\Facades\Route;
// use Illuminate\Http\Request;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PropertyController;
use App\Http\Controllers\PropertylistController;
use App\Http\Controllers\FormController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('forms', 'App\Http\Controllers\FormController');
Route::get('/', [App\Http\Controllers\PropertyController::class, 'proplist'])->name('home');
Route::get('/home', function () {
    return view('dashboard');
});
Route::get('/users', 'App\Http\Controllers\UserController@index');
// Route::get('/hjhj', function () {
//     return 'Hello world';

// Route::get('/aboutus', [PropertyController::class, 'index']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\PropertyController::class, 'proplist'])->name('home');

Auth::routes();
// Route::get('/home', [App\Http\Controllers\HomeController::class, 'uploadAvatar']);
Route::post('/upload', [UserController::class,'uploadAvatar']);

Route::get('/home', [App\Http\Controllers\PropertyController::class, 'proplist'])->name('home')->middleware('auth');
Route::get('/property', [App\Http\Controllers\PropertyController::class, 'proplist'])->middleware('auth');
Route::post('/property/create', [App\Http\Controllers\PropertyController::class, 'store'])->middleware('auth');
Route::get('/propertylist', [App\Http\Controllers\PropertyController::class, 'propertylist'])->middleware('auth');
Route::get('/propertyList/{propId}/propertylists', [App\Http\Controllers\PropertylistController::class, 'propertyListAll'])->middleware('auth');
Route::post('/propertyList/create', [App\Http\Controllers\PropertylistController::class, 'storepropertylist'])->middleware('auth');
Route::get('/client', [App\Http\Controllers\ClientController::class, 'clientlists'])->name('client')->middleware('auth');
Route::post('/client/create', [App\Http\Controllers\ClientController::class, 'store'])->name('client')->middleware('auth');
Route::post('/client/Edit', [App\Http\Controllers\ClientController::class, 'edit'])->middleware('auth');
Route::get('/agent', [App\Http\Controllers\AgentController::class, 'agentList'])->name('Agent')->middleware('auth');
Route::post('/agent/create', [App\Http\Controllers\AgentController::class, 'store'])->name('Agent')->middleware('auth');
Route::post('/agent/Edit', [App\Http\Controllers\AgentController::class, 'edit'])->middleware('auth');
Route::get('/clientProperties/{propId}/list', [App\Http\Controllers\ClientPropertyController::class, 'clientProperties'])->name('clientProperties')->middleware('auth');
Route::get('/clientProperties/getList', [App\Http\Controllers\ClientPropertyController::class, 'getpropList'])->name('client')->middleware('auth');
Route::get('/propertyLists/{prop_id}', [App\Http\Controllers\PropertylistController::class, 'getBlockList'])->middleware('auth');
Route::get('/propertyListsActive/{block}/{prop_id}', [App\Http\Controllers\PropertylistController::class, 'getActiveList'])->middleware('auth');
Route::get('/propertyListsView/{propertylist}', [App\Http\Controllers\PropertylistController::class, 'getView'])->middleware('auth');
Route::post('/agent/getList', [App\Http\Controllers\AgentController::class, 'getAgents'])->name('Agent.getAgents')->middleware('auth');
Route::get('/agentFind/{agentid}', [App\Http\Controllers\AgentController::class, 'fetchAgents'])->middleware('auth');

Route::post('/clientProperties/create', [App\Http\Controllers\ClientPropertyController::class, 'store'])->name('clientProperties.store')->middleware('auth');
Route::post('/clientProperties/assume', [App\Http\Controllers\ClientPropertyController::class, 'assume'])->name('clientProperties.assume')->middleware('auth');
Route::get('/Payment', [App\Http\Controllers\PaymentController::class, 'index'])->name('Payment.index')->middleware('auth');
Route::get('/OtherPayment', [App\Http\Controllers\PaymentController::class, 'otherindex'])->name('Payment.otherindex')->middleware('auth');
Route::post('/client/getList', [App\Http\Controllers\ClientController::class, 'getClientList'])->name('client.getClientList')->middleware('auth');
Route::get('/clientPropertiesList/{client_id}', [App\Http\Controllers\ClientPropertyController::class, 'getActiveListPay'])->middleware('auth');
Route::get('/clientPropertyFetch/{cp_id}', [App\Http\Controllers\ClientPropertyController::class, 'Fetch'])->name('clientProperty.Fetch')->middleware('auth');
Route::post('payment/Save', [App\Http\Controllers\PaymentController::class, 'Save'])->name('payment.Save')->middleware('auth');
Route::post('/propertyList/Blockcreate', [App\Http\Controllers\PropertylistController::class, 'storepropertylistmany'])->middleware('auth');
Route::post('/propertyList/edit', [App\Http\Controllers\PropertylistController::class, 'Edit'])->middleware('auth');
Route::get('/test', [App\Http\Controllers\PropertylistController::class, 'index'])->middleware('auth');
Route::get('/Commission/{agent_id}', [App\Http\Controllers\PaymentController::class, 'getUnclaimed'])->middleware('auth');
Route::get('/CommissionBank/{agent_id}', [App\Http\Controllers\PaymentController::class, 'getUnclaimedBank'])->middleware('auth');
Route::get('/CommissionHistory/{agent_id}', [App\Http\Controllers\PaymentController::class, 'getRelease'])->middleware('auth');
Route::get('/CommissionHistoryBank/{agent_id}', [App\Http\Controllers\PaymentController::class, 'getReleaseBank'])->middleware('auth');
Route::get('/CommissionHistoryDate/{agent_id}/{relDate}/{method}', [App\Http\Controllers\PaymentController::class, 'getReleaseDate'])->middleware('auth');
Route::get('/CommissionHistoryClient/{agent_id}/{client}', [App\Http\Controllers\PaymentController::class, 'getReleaseClient'])->middleware('auth');

Route::get('/paymenthistory/{cp_id}', [App\Http\Controllers\PaymentController::class, 'listPaid'])->middleware('auth');
Route::get('/userlist', [App\Http\Controllers\UserController::class, 'userlists'])->middleware('auth');
Route::post('/propertyList/tryForfeit', [App\Http\Controllers\UserController::class, 'checkpassword'])->middleware('auth');
Route::get('/newUser', [App\Http\Controllers\UserController::class, 'loaduserform'])->middleware('auth');
Route::post('/newUseradd', [App\Http\Controllers\UserController::class, 'adduser'])->middleware('auth');
Route::post('/otherpayment/store', [App\Http\Controllers\PaymentController::class, 'SaveOtherPayment'])->name('payment.SaveOtherPayment')->middleware('auth');
Route::get('/expenses', [App\Http\Controllers\ExpenseController::class, 'index'])->middleware('auth');
Route::post('/expense/add', [App\Http\Controllers\ExpenseController::class, 'store'])->middleware('auth');
Route::get('/userprofile', [App\Http\Controllers\UserController::class, 'profile'])->middleware('auth');
Route::post('/user/update', [App\Http\Controllers\UserController::class, 'editUser'])->middleware('auth');
Route::get('/reports', [App\Http\Controllers\ReportController::class, 'index'])->middleware('auth');
Route::get('/getpropListwithblock/{prop}/{block}', [App\Http\Controllers\PropertylistController::class, 'propertylistwithBlock'])->middleware('auth');
Route::get('/Release/{id}/{cp_id}/{ornum}/{method}', [App\Http\Controllers\PaymentController::class, 'releaseComm'])->middleware('auth');
Route::get('/ReleaseDate/{agent_id}/{date}/{method}', [App\Http\Controllers\PaymentController::class, 'releaseCommDate'])->middleware('auth');
Route::get('/forfeit/{cp_id}/{propertylistid}', [App\Http\Controllers\ClientPropertyController::class, 'forfeit'])->middleware('auth');
Route::get('/paymenthistory', [App\Http\Controllers\PaymentController::class, 'paymenthistory'])->middleware('auth');
Route::post('/paymentEdit', [App\Http\Controllers\PaymentController::class, 'Edit'])->name('payment.Edit')->middleware('auth');
Route::post('/propertyedit', [App\Http\Controllers\PropertyController::class, 'EditProp'])->middleware('auth');
Route::get('/paymentDelete/{id}/{amount}', [App\Http\Controllers\PaymentController::class, 'unlist'])->middleware('auth');
Route::get('/SubAgentList/{agent_id}', [App\Http\Controllers\SubagentController::class, 'fetch'])->middleware('auth');
Route::post('/subagent/create', [App\Http\Controllers\SubagentController::class, 'store'])->name('SubAgent')->middleware('auth');
Route::get('/test', [App\Http\Controllers\PaymentController::class, 'scheduleSMS'])->middleware('auth');
Route::get('/getsubagentlist/{agent_id}', [App\Http\Controllers\SubagentController::class, 'getSubagent'])->middleware('auth');
Route::get('/partner', [App\Http\Controllers\PartnerController::class, 'partnerslist'])->middleware('auth');
Route::get('/partners/properties/{partnerId}', [App\Http\Controllers\PartnerController::class, 'partnersproperties'])->middleware('auth');
Route::post('/partners/properties/store', [App\Http\Controllers\PartnerController::class, 'storeproperty'])->middleware('auth');
Route::post('/partner/getList', [App\Http\Controllers\PartnerController::class, 'getPartners'])->middleware('auth');
Route::post('/partner/create', [App\Http\Controllers\PartnerController::class, 'store'])->middleware('auth');
Route::get('/Signed/{cp_id}', [App\Http\Controllers\ClientPropertyController::class, 'nowSigned'])->middleware('auth');
Route::post('/dailyBankReport', [App\Http\Controllers\ReportController::class, 'BankDaily'])->middleware('auth');
Route::get('/partnerPayment/{propId}/{partnerId}/{percentage}', [App\Http\Controllers\PartnerController::class, 'partnerPaymentsShare'])->middleware('auth');
Route::post('/partnerShareGenerate', [App\Http\Controllers\PartnerController::class, 'GeneratePartnerReport'])->middleware('auth');
Route::get('/noticeSent', [App\Http\Controllers\PaymentController::class, 'sendNoticeSMS'])->name('payment.sendNoticeSMS')->middleware('auth');
Route::post('/bulkrelease', [App\Http\Controllers\PaymentController::class, 'bulkrelease'])->name('bulkrelease')->middleware('auth');









Route::group([
    'namespace' => 'App\Http\Controllers',
], function () {
    Route::get('SOA/{cp_id}', 'ReportController@statement');
});


Route::group([
    'namespace' => 'App\Http\Controllers',
], function () {
    Route::get('reports/agents', 'ReportController@agents');
});


// Route::post('/registerNow', [App\Http\Controllers\UserController::class, 'adduser']);



//Route::resource('property', 'PropertyController');
//Route::resource('client', 'App\Http\Controllers\ClientController');
